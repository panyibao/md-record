# java

jdk,jre,jvm关系

![](./img/jdk,jre,jvm关系.jpg)

## 1 内存模型

#### 1.1 JVM内存模型

JVM内存模型包括：堆(Heap)、方法区(元空间,Method Area)、虚拟机栈(vm Stack)、本地方法栈(Native Method Stack)、程序计数器(PC Register)；虚拟机栈、本地方法栈、程序计数器是每个线程所独有的。
方法区（元空间）：
主要包括：常量、静态变量、类信息、运行时常量池，操作的是直接内存。
堆：
堆是运行时数据区，所有类的实例和数据都是在堆上分配内存。它在 JVM 启动的时候被创建。对象所占的堆内存是由自动内存管理系统也就是垃圾收集器回收。默认堆中年轻代(Young)占1/3，老年代(Old)占2/3，年轻代中包含Eden区和Survivor区，Survivor区包含From(S0)区和To(区)，默认新生代中Eden区、From区、To区的比例为8:1:1，当Eden区内存不足时会触发Minor gc，没有被回收的对象进入到Survivor区，同时分代年龄+1，当再次触发Minor gc时，From区中的对象会移动到To区，Minor gc会回收Eden区和From区中的垃圾对象，对象的分代年龄会一次次的增加，当分代年龄增加到15以后，对象会进入到老年代。
当老年代内存不足时，会触发Full gc，如果Full gc无法释放足够的空间，会触发OOM内存溢出，在进行Minor gc或Full gc时，会触发STW（Stop The World），即停止用户线程。

jvm模型简图：

![](./img/jvm类加载及执行模型图.jpg)

### 1.2 多线程内存模型

​        在多线程中，多个线程访问主存中的临界资源（共享变量）时，需要首先从主存中拷贝一份共享变量的值到自己的工作内存中，然后在线程中每次访问该变量时都是访问的线程工作内存(高速缓存)中的共享的变量副本，而不是每次都去主存中读取共享变量的值（因为CPU的读写速率和主存读写速率相差很大，如果CPU每次都访问主存的话那么效率会非常低）。java线程变量加载的大致流程是，将主内存的变量加载到工作内存进行处理，**处理完毕后写会主内存**。

![img](./img/2020072610582125.png)

针对以下程序：

```java
public class MainThread {
    public static void main(String[] args) {
        C_Thread ct = new C_Thread();
        ct.start();
        while (true){
            if(ct.isFlag()){
                System.out.println("flag:" + ct.isFlag());
                break;
            }
        }
    }
}

class C_Thread extends Thread{
    private boolean flag = false;

    public boolean isFlag(){
        return this.flag;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.flag = true;
        System.out.println("flag:" + this.isFlag());
    }
}
```

运行会发现主线程一直无法退出：

分析原因：主线程启动将变量C_Thread ct加载到了自己的工作内存，此时flag为false, 然后子线程将flag修改为了true并且在运行完成后写回了主内存。但是因为主线程一直没有刷新自己的工作内存导致自己工作内存的变量一直是false。

解决方法：给flag加上volatile 关键字，保证变量的可见性。当一个变量被volatile关键字修饰时，对于共享资源的读操作会直接在主内存中进行(当然也会缓存到工作内存中，当其他线程对该共享资源进行了修改，则会导致当前线程在工作内存中的共享资源失效，所以必须从主内存中再次获取)。

除上述方法外，以下操作也会刷新工作内存：

只有工作内存失效的时候，工作内存才会重新加载主内存的变量，所以需要使工作内存失效即可，具体方法有：

**线程中释放锁时**：在方法中调用同步方法 比如System.out.println();

**线程切换时**：线程休眠sleep(1000);

**CPU有空闲时间时**（比如线程休眠，IO操作）：IO操作 File file = new File(“D://temp.txt”);

备注：

当子线程非常消耗CPU的时候，子线程的工作内存不会主动去和主内存中的共享变量同步，这个就造成了刚刚出现的问题。但是当CPU消耗不是太厉害的时候，JVM会自动的把主内存中的共享变量同步到线程的工作内存中。
JVM有两种启动启动模式：一种是client启动模式，还有之中是server启动模式。server模式启动比较慢，但是启动了之后程序的运行速度比较快，这个是因为Server模式在内存方面做优化就是上面的cache。client模式启动的时候比较快，内存使用比较少，但是程序运行的速度就比较慢

## 2、Integer问题

### 2.1、Integer缓存

看下面代码

```java
public static void main(String[] args) {
        Integer i0 = 1;
        Integer i1 = new Integer(1);
        Integer i2 = new Integer(1);

        Integer i3 = Integer.valueOf(1);
        Integer i4 = Integer.valueOf(1);

        System.out.println(i0 == i1);
        System.out.println(i0 == i3);
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);;
}
```

代码的运行结果为：

```shell
false
true
false
true
```

解释：

查看源代码，new Integer和Integer.valueOf的代码如下：

![](./img/2022-08-08-13-52-35-image.png)

![](./img/2022-08-08-13-53-05-image.png)

从源代码可知：

Integer中有一个内部类IntegerCache缓存了[-128, 127]的整形数据

![](./img/2022-08-08-13-55-35-image.png)

当使用new Integer创建对象时并没有从IntegerCache内取数据，所以两个对象不是同一个

当使用Integer.valueOf创建对象时是从IntegerCache内取数据，所以两个对象是同一个

Integer i0 = 1;通过反编译可以知道实际使用的是Integer.valueOf方法。

**注意**

通过代码搜索可以知道只有在通过Integer.valueOf获取对象时才用到IntegerCache，其余方法获取的是不同的对象

## 3、String问题

### 3.1、String对象地址

看下面代码：

```java
        String a1 = "123";
        String a2="12" + "3"; //编译期会进行优化
        String a3 = new String("123");
        String a4 = String.valueOf("123");

        System.out.println("a1 == a2: " + (a1 == a2));
        System.out.println("a1 == a3: " + (a1 == a3));
        System.out.println("a1 == a4: " + (a1 == a4));
        System.out.println("a2 == a3: " + (a2 == a3));
        System.out.println("a3 == a4: " + (a3 == a4));
```

jdk8运行结果：

```shell
a1 == a2: true
a1 == a3: false
a1 == a4: true
a2 == a3: false
a3 == a4: false
```

**字面量**
字面量就是指由字母、数字等构成的字符串或者数值常量
字面量只可以右值出现，所谓右值是指等号右边的值，如：int a=1 这里的a为左值，1为右值。在这个例子中1就是字面量。

**符号引用**

符号引用是编译原理中的概念，是相对于直接引用来说的。主要包括了以下三类常量：
类和接口的全限定名
字段的名称和描述符
方法的名称和描述符

**字符串常量池**
JDK8字符串常量池放到堆空间,其引用指向元空间(方法区)的常量池。常量池设计就是一种缓存池，为了提高程序性能。
为字符串开辟一个字符串常量池，类似于缓存区
创建字符串常量时，首先查询字符串常量池是否存在该字符串
存在该字符串，返回引用实例，不存在，实例化该字符串并放入池中

"123" 双引号String 对象会自动放入常量池
调用String的intern 方法也会将对象放入到常量池中

**问题分析：**

先看字面量常量池中是否有"123"字面量，如果没有先在字符串常量池中创建字符串对象“123"，再去内存堆中创建字符串对象 ，把堆中的引用返回；

a2="12" + "3"中"12" 和 "3"都是常量，a2在编译时就已经确定，会在编译期会进行优化；

String.valueOf("123")内部实际调用的是"123".toString()，"123"字符串常量，与a1相同；

String a3 = new String("123");从两方面拆分来看:
String name ---->name指向内存中的对象引用
new String(“tom”)—->new 在堆内存中创建对象
它保证字符串常量池和堆中都有这个对象，没有就创建，最后返回堆内存中的对象引用。

在看下面的分析：

**全局字符串常量池中存放的内容是在类加载完成后存到String Pool中的，在每个VM中只有一份，存放的是字符串常量的引用值(在堆中生成字符串对象实例)。**

![](./img/字符串常量池.png)

在看以下代码：

```java
String ab = "a" + "b";
String a = "a";
String ab1 = a + "b";
System.out.println(ab == ab1); // false
```

查看反编译后的代码：

![](./img/2022-08-08-15-47-55-image.png)可以看到String ab = "

String ab1 = a + "b";使用了StringBuilder进行操作，最终使用toString方法放回字符串。

## 4、数据精度丢失问题

看下面代码

```java
public static void main(String[] args) {
        Double num1 = 0.02;
        Double num2 = 0.03;
        System.out.println(num2 - num1);

        BigDecimal num3 = new BigDecimal(0.02);
        BigDecimal num4 = new BigDecimal(0.03);
        System.out.println(num4.subtract(num3));

        BigDecimal num5 = new BigDecimal("0.02");
        BigDecimal num6 = new BigDecimal("0.03");
        System.out.println(num6.subtract(num5));

        BigDecimal num7 = BigDecimal.valueOf(0.02);
        BigDecimal num8 = BigDecimal.valueOf(0.03);
        System.out.println(num8.subtract(num7));
}
```

运行结果：

```shell
0.009999999999999998
0.0099999999999999984734433411404097569175064563751220703125
0.01
0.01
```

**分析：**

Double类型的两个参数相减会转换成二进制，因为Double有效位数为16位这就会出现存储小数位数不够的情况，这种情况下就会出现误差。

使用BigDecimal时需要注意,直接使用new BigDecimal(0.02)仍然可能丢失精度，这个在BigDecimal的构造方法上有说明：

```java
/**
     * Translates a {@code double} into a {@code BigDecimal} which
     * is the exact decimal representation of the {@code double}'s
     * binary floating-point value.  The scale of the returned
     * {@code BigDecimal} is the smallest value such that
     * <tt>(10<sup>scale</sup> × val)</tt> is an integer.
     * <p>
     * <b>Notes:</b>
     * <ol>
     * <li>
     * The results of this constructor can be somewhat unpredictable.
     * One might assume that writing {@code new BigDecimal(0.1)} in
     * Java creates a {@code BigDecimal} which is exactly equal to
     * 0.1 (an unscaled value of 1, with a scale of 1), but it is
     * actually equal to
     * 0.1000000000000000055511151231257827021181583404541015625.
     * This is because 0.1 cannot be represented exactly as a
     * {@code double} (or, for that matter, as a binary fraction of
     * any finite length).  Thus, the value that is being passed
     * <i>in</i> to the constructor is not exactly equal to 0.1,
     * appearances notwithstanding.
     *
     * <li>
     * The {@code String} constructor, on the other hand, is
     * perfectly predictable: writing {@code new BigDecimal("0.1")}
     * creates a {@code BigDecimal} which is <i>exactly</i> equal to
     * 0.1, as one would expect.  Therefore, it is generally
     * recommended that the {@linkplain #BigDecimal(String)
     * <tt>String</tt> constructor} be used in preference to this one.
     *
     * <li>
     * When a {@code double} must be used as a source for a
     * {@code BigDecimal}, note that this constructor provides an
     * exact conversion; it does not give the same result as
     * converting the {@code double} to a {@code String} using the
     * {@link Double#toString(double)} method and then using the
     * {@link #BigDecimal(String)} constructor.  To get that result,
     * use the {@code static} {@link #valueOf(double)} method.
     * </ol>
     *
     * @param val {@code double} value to be converted to
     *        {@code BigDecimal}.
     * @throws NumberFormatException if {@code val} is infinite or NaN.
     */
    public BigDecimal(double val) {
        this(val,MathContext.UNLIMITED);
    }
```

对于注释中的这段话：

```
     * The results of this constructor can be somewhat unpredictable.
     * One might assume that writing {@code new BigDecimal(0.1)} in
     * Java creates a {@code BigDecimal} which is exactly equal to
     * 0.1 (an unscaled value of 1, with a scale of 1), but it is
     * actually equal to
     * 0.1000000000000000055511151231257827021181583404541015625.
     * This is because 0.1 cannot be represented exactly as a
     * {@code double} (or, for that matter, as a binary fraction of
     * any finite length).  Thus, the value that is being passed
     * <i>in</i> to the constructor is not exactly equal to 0.1,
     * appearances notwithstanding.
```

从这段话可以知道：

此构造函数的结果可能有些不可预测。假设有人可能会用 Java 编写 {@code new BigDecimal(0.1)} 会创建一个 {@code BigDecimal} ，它完全等于0.1（未缩放的值为 1，比例为 1），但实际上等于 0.1000000000000000055511151231257827021181583404541015625。 这是因为 0.1 不能完全表示为 {@code double} （或者，就此而言，不能表示为任何有限长度的二进制分数）。 因此，传递给构造函数的值并不完全等于 0.1。

随后的注释中给出了解决方法：

         * The {@code String} constructor, on the other hand, is
         * perfectly predictable: writing {@code new BigDecimal("0.1")}
         * creates a {@code BigDecimal} which is <i>exactly</i> equal to
         * 0.1, as one would expect.  Therefore, it is generally
         * recommended that the {@linkplain #BigDecimal(String)
         * <tt>String</tt> constructor} be used in preference to this one.

这段注释说明了：

{@code String} String参数的构造函数是完全可预测的：编写 {@code new BigDecimal("0.1")} 会创建一个 {@code BigDecimal}，它<i>完全</i>等于 0.1， 正如人们所期望的那样。 因此，一般建议优先使用 {@linkplain #BigDecimal(String) <tt>String</tt> 构造函数}。

同时还给出了另一种解决方法

```
     * When a {@code double} must be used as a source for a
     * {@code BigDecimal}, note that this constructor provides an
     * exact conversion; it does not give the same result as
     * converting the {@code double} to a {@code String} using the
     * {@link Double#toString(double)} method and then using the
     * {@link #BigDecimal(String)} constructor.  To get that result,
     * use the {@code static} {@link #valueOf(double)} method.
```

这段注释说明：

当必须将 {@code double} 用作 {@code BigDecimal} 的源时，请注意此构造函数提供了精确的转换； 它与使用 {@link Double#toString(double)} 方法然后使用 {@link #BigDecimal(String)} 构造函数将 {@code double} 转换为 {@code String} 的结果不同。 要获得该结果，请使用 {@code static} {@link #valueOf(double)} 方法。

即：使用BigDecimal.valueOf(0.02)进行创建

查看代码：

```java
public static BigDecimal valueOf(double val) {
        // Reminder: a zero double returns '0.0', so we cannot fastpath
        // to use the constant ZERO.  This might be important enough to
        // justify a factory approach, a cache, or a few private
        // constants, later.
        return new BigDecimal(Double.toString(val));
}
```

可以看到BigDecimal.valueOf中使用Double.toString对传入的double参数转换成了字符串，最终调用的仍然是BigDecimal(String) 构造函数。

## 5、位运算

1、>>>

【>>>】是按位右移补零操作符，移动得到的空位用0填充。

比如 4 >>>1 得到的就是2，是指向右移动 1 位，对十进制数值的影响是 / 2^1，即...0100 ---> ...0010，将非0的最高位向右移动1位，右移是缩小值的大小，为“/”，左移为增大值的大小，为“ * ”。

2、>>>

【>>】 是按位右移运算符。

比如 4 >> 1 得到的就是 2，是指向右移动 1 位，对十进制数值影响是 / 2^1，即 100 ---> 10 ,移动得到的空位未用0填充。

3、<<

【<<】 是按位左移运算符。

比如 4 << 2 得到的就是 16,是指向左移动 2 位，对十进制数值影响是 * 2^2，即 100（4） ---> 10 00（16）。

4 、& 运算符 与
& 两个位都为1时，结果才为1

5 、| 运算符 或
| 两个位都为0时，结果才为0

6、 ^ 运算符  异或    
^运算符，两个位相同为0，相异为1

## 6、时间格式化

时间使用旧的Date对象时，用SimpleDateFormat进行格式化显示。
使用新的LocalDateTime或ZonedLocalDateTime时，要进行格式化显示，就要使用DateTimeFormatter。
和SimpleDateFormat不同的是，DateTimeFormatter不但是不变对象，它还是线程安全的。
因为SimpleDateFormat不是线程安全的，使用的时候，只能在方法内部创建新的局部变量。而DateTimeFormatter可以只创建一个实例，到处引用。

**注意：**  JDK8中SimpleDateFormat的F和DateTimeFormatter中的F并不是同一个字段，需要使用DateTimeFormatterBuilder构造DateTimeFormatter

**SimpleDateFormat：F指Day of week in month 某月中第几周,按这个月1号算,1号起就是第1周,8号起就是第2周。**

**DateTimeFormatter：F指week-of-month  日历中的第几周**

普通的yyyyMMddHHmmssSSS格式不包含F时可以使用如下初始化：

```java
DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
```

当包含F时如yyyyMMddHHmmssFFF,建议使用如下初始化：

使用DateTimeFormatterBuilder的appendValue方法指定F对应位置的字段。

```java
DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern("yyyyMMddHHmmss")
                .appendValue(ChronoField.ALIGNED_WEEK_OF_MONTH, 3)
                .toFormatter();
```

## 7、内存屏障

内存屏障其实就是一个CPU指令，在硬件层面上来说可以扥为两种：Load Barrier 和 Store Barrier即读屏障和写屏障。主要有两个作用：
**（1）阻止屏障两侧的指令重排序；**
**（2）强制把写缓冲区/高速缓存中的脏数据等写回主内存，让缓存中相应的数据失效。**
在JVM层面上来说作用与上面的一样，但是种类可以分为四种：

| 屏障         | 指令示例                     | 说明                                     |
| ---------- | ------------------------ | -------------------------------------- |
| LoadLoad   | Load1；LoadLoad；Load2     | 保证Load1的读取在Load2之前执行。                  |
| StoreStore | Store1；StoreStore；Store2 | 保证在Store2写操作之前，Store1的写操作已经刷新到主内存。     |
| LoadStore  | Load1；LoadStore；Store1   | 保证在Store1写操作之前，Load1的读操作已经完成。          |
| StoreLoad  | Store1；StoreLoad；Load1   | 保证在Load1读操作之前，Store1以及之前的写操作已经刷新回主内存中。 |

内存屏障有三种类型和一种伪类型：
（1）lfence：即读屏障(Load Barrier)，在读指令前插入读屏障，可以让高速缓存中的数据失效，重新从主内存加载数据，以保证读取的是最新的数据。
（2）sfence：即写屏障(Store Barrier)，在写指令之后插入写屏障，能让写入缓存的最新数据写回到主内存，以保证写入的数据立刻对其他线程可见。
（3）mfence，即全能屏障，具备ifence和sfence的能力。
（4）Lock前缀：Lock不是一种内存屏障，但是它能完成类似全能型内存屏障的功能。
为什么说Lock是一种伪类型的内存屏障，是因为内存屏障具有happen-before的效果，而Lock在一定程度上保证了先后执行的顺序，因此也叫做伪类型。比如，IO操作的指令，当指令不执行时，就具有了mfence的功能。

## 8、volatile关键字

### 8.1 volatile的特性

可见性、有序性（禁止重排 ）。
volatile不具备原子性。在一个线程（t1）操作volatile变量时，另一个线程（t2）更新了该volatile变量的值，则t1线程需要去主内存中获取最新的volatile变量值，t1线程之前的操作则全部作废，出现操作丢失问题。由此可见volatile解决的是可见性的问题，而无法保证其原子性。所以在多线程修改主内存变量的情况下必须加锁。

### 8.2 volatile变量的读写过程

在对一个volatile变量进行写操作时，JMM会将该线程工作内存中的该volatile变量立即刷新回主内存中，并通知其它线程该变量已更新。
在对一个volatile变量进行读操作时，JMM会将该线程工作内存中的该volatile变量置为无效，重新回到主内存中读取该变量。

### 8.3 8个原子性操作

**read（读取）**：作用于主内存，将变量从主内存读取到工作内存中。
**load（加载）**：作用于工作内存，将读取到工作内存中的变量放入工作内存的变量副本中（工作线程修改的都是自己副本中的变量）。
**use（使用）**：作用于工作内存，将工作内存中的变量副本中的值传递给执行引擎进行操作。
**assign（赋值）**：作用于工作内存，将执行引擎中返回的值赋值给工作内存中的变量副本。
**store（存储）**：作用于工作内存，将赋值完毕的变量副本写回给主内存。
**write（写入）**：作用于主内存，将工作内存写回的值赋值给主内存中的变量。
**lock（锁定）**：作用于主内存，在工作内存写回变量的过程中，为防止多个线程同时向主内存写入，因此需要加锁，确保同一时刻只能有一个线程写入。
**unlock（解锁）**：作用于主内存，对应lock，线程写入后解锁，其他线程才能继续写入该变量。

### 8.4 volatile的适用场景

状态标志：将volatile修饰的状态变量改变时，会立刻通知其他线程。
开销较低的读，写锁策略：当读远多于写的情况下，可以将变量设置为volatile，则只需要在写操作上添加sync同步即可，每次写操作都会通知读操作去读取最新的值。

**单例模式中为变量添加volatile**，保证多线程情况下new对象时禁止指令重排的问题（先获取一块内存地址，再通过这块内存初始化对象，再返回对象的地址）；

### 8.5 volatile的有序性

volatile的底层指令为Lock前缀，实现了内存屏障的效果，保证volatile的有序性，即禁止指令重排序（在多线程环境下，指令会根据编译器等重排序）的一种屏障指令。主要用于禁止屏障两侧的指令重排。     当一个变量被定义为volatile时，在其字节码层面会为这个字段的flags里添加一个ACC_VOLATILE，jvm在生成对应的字节码指令时，发现操作的是volatile变量时，会在相应位置插入内存屏障。

### 8.6 volatile规则之四种屏障的位置

当第一个操作为volatile读操作时，无论第二个是什么操作，都不能重排； 当第二个操作为volatile写操作时，无论第一个操作是什么，都不能重排； 当第一个操作为volatile写操作，第二个为volatile读操作时，不能重排；

# 9、对象初始化

## 9.1类的生命周期

![](./img/类的生命周期.jpg)

**加载：**
1.通过一个类的全限定名来获取其定义的二进制字节流；
2.将这个字节流所代表的的静态存储结构转化为方法区的运行时数据结构。
3.在java堆生成一个代表这个类的java.lang.class对象，作为方法区中这些数据的访问入口。
可以使用系统提供的类加载器来完成加载，也可以自定义自己的类加载器来完成加载。加载完成后，虚拟机外部的二进制字节流就按虚拟机要求的格式存在方法区中，同时在堆区创建一个java.lang.class对象用来访问方法区中的数据。
注：链接就是将java类的二进制代码合并到jvm的运行状态之中的过程。
**验证：** 确保加载的类信息符合jvm规范，没有安全方面的问题。
**准备：** 为类变量（static变量）分配内存并设置类变量初始值的阶段，这些内存都将在方法区中进行分配。如public static int a = 8080,a在准备的值为0，但如果是final修饰的则是8080。
**解析：** 虚拟机常量池内的符号引用替换为直接引用的过程。（比如String s=“a”,转化为s的地址指向“aaa”的地址）。符号引用就是class文件中的CONSTANT_Class_info、CONSTANT_field_info、CONSTANT_Method_info等类型的常量。
初始化：为类的静态变量赋予正确的初始值，只有对类的主动使用的时候才会导致类的初始化。
**初始化：**
a.假如这个类还有没被加载和连接，则程序先加载并连接该类
b.假如该类的直接父类还没有被初始化，则先初始化其直接父类
c.假如类中有初始化语句，则系统依次执行这些初始化语句。

## 9.2创建对象

![](./img/对象创建过程.jpg)

**类加载检查**
虚拟机执行new指令时，首先去检查这个指令的参数是否能在常量池中定位到这个类的符号引用，并且检查这个符号引用代表的类是否被加载、解析和初始化过，如果没有，则必须先执行相应的类加载过程。

**分配内存**
类加载检查通过后，虚拟机将为新生对象分配内存，对象所需的内存大小在类加载完成后便可确定，为对象分配空间的任务等同于把一块确定大小的内存从java堆中划分出来。分配内存的方式有“指针碰撞”和“空闲列表”两种，选择哪种方式由java堆是否规整决定，java堆是否规整又由所采用的的垃圾收集器是否带有压缩整理功能决定。
        **指针碰撞：** 适用场合：堆内存规整（即没有内存碎片）的情况。原理：用过的内存全部整合到一边，没用过得内存放在另一边，中间有一个分解值指针，只需要向着没用过的内存方向将该指针移动对象内存大小位置即可。GC垃圾收集器：Serial，ParNew
        **空闲列表：** 使用场合：堆内存不规整的情况下。原理：虚拟机会维护一个列表，该列表中会记录那些内存块是可用的，在分配的时候，找一块儿足够大的内存块儿来划分给对象实例，最后更新列表记录。GC收集器：CMS。

**初始化零值**
内存分配完成后，虚拟机需要将分配到的内存空间都初始化为零值（不包括对象头），这一操作保证了对象的实例字段在java代码中可以不赋予初始值就直接使用，程序能访问到这些字段的数据类型所对应的零值。

**设置对象头**
初始化零值完成之后，虚拟机要对对象进行必要的设置，例如这个对象是哪个类的实例，如何才能找到类的元数据信息，对象的哈希码，对象的GC分代年龄等信息，这些信息存放在对象头，此外，是否开启偏向锁，锁的标志位都在对象头中设置。

**执行init方法**
到此，从虚拟机的角度，一个新的对象已经产生了，但从java程序的视角来看，对象创建才刚开始，<init>方法还没有执行，所有字段都还为零，所以一般来说，执行完new指令会执行init方法，把对象按照程序员的意愿进行初始化，这样一个真正可用的对象才算完全产生出来。

## 9.3指令重排导致对象半初始化问题

程序：

```java
public class Singleton {    
    private static Singleton singleton;
    private Singleton(){}

    public static Singleton getInstance(){
        if(singleton==null){
            synchronized(Singleton.class){
                if(singleton==null){
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
```

语句“singleton = new Singleton()”在程序执行时可能会发生指令重排，这样一个语句，实际上被分成了以下三个步骤：

1、分配对象的内存空间
2、初始化内存空间
3、将对象指向该内存空间
而当指令重排的时候，三个步骤的顺序可能会变成这样：

1、分配对象的内存空间
2、将对象指向该内存空间
3、初始化内存空间
那么问题就来了，假设我们现在有两个线程，A和B，当A执行到上述步骤中的第二步的时候，B执行到一个校验语句“if(singleton==null)”，此时对象已经指向了分配的内存空间，所以singleton不为空，那么B线程就会获得一个未初始化完成的对象singleton，从而造成程序错误。

**因此需要将singleton声明为volatile类型，以此来禁止指令重排。**

# 10、NIO、BIO

**BIO**

BIO全称是Blocking IO，同步阻塞式IO，是JDK1.4之前的传统IO模型。
Java BIO：服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器端就需要启动一个线程进行处理，

![](./img/BIO示意图.jpg)

虽然此时服务器具备了高并发能力，即能够同时处理多个客户端请求了，但是却带来了一个问题，随着开启的线程数目增多，将会消耗过多的内存资源，导致服务器变慢甚至崩溃，NIO可以一定程度解决这个问题。

**NIO**

NIO 中的 N 可以理解为 **Non-blocking**(非阻塞)，不单纯是 New，是解决高并发、I/O高性能的有效方式。
Java NIO是Java1.4之后推出来的一套IO接口，NIO提供了一种完全不同的操作方式， NIO支持面向缓冲区的、基于通道的IO操作。
新增了许多用于处理输入输出的类，这些类都被放在java.nio包及子包下，并且对原java.io包中的很多类进行改写，新增了满足NIO的功能。

![](./img/NIO示意图.jpg)

针对图中的就**绪事件列表rdlist**

![](./img/NIO示意图1.jpg)

这个列表是由操作系统的**中断程序**感知到之后回调NIO底层的事件函数建事件添加到列表中的。

对于linux系统，java NIO的实现主要依赖系统的epoll对应的三个系统函数epoll_create，epoll_ctl，epoll_wait。

（1）轮询机制：在JDK1.4版本是用linux的内核函数select()或poll()来实现，跟上面的NioServer代码类似，selector每次都会轮询所有的sockchannel看下哪个channel有读写事件，有的话就处理，没有就继续遍历。

（2）事件通知机制：JDK1.5开始引入了epoll基于事件响应机制来优化NIO。

**select,poll,epoll对比**

![](./img/epoll内核函数对比.jpg)

select的上限为1024

**NIO 有三大核心组件：**
**Channel(通道)， Buffer(缓冲区)，Selector(多路复用器)**

1、channel 类似于流，每个 channel 对应一个 buffer缓冲区，buffer 底层就是个数组

2、channel 会注册到 selector 上，由 selector 根据 channel 读写事件的发生将其交由某个空闲的线程处理

3、NIO 的 Buffer 和 channel 都是既可以读也可以写

NIO代码示例：

路径：md/md-code/myjava/src/main/java/com/pan/nio/NioTmp.java

```java
public class NioTmp {
    public static void main(String[] args) {
        try{
            // 打开通道（）
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            // 绑定地址和端口
            serverSocketChannel.socket().bind(new InetSocketAddress(9000));
            // 设置为非阻塞
            serverSocketChannel.configureBlocking(false);
            // 打开Selector用于处理Channel事件，对服务器来说是创建epoll
            Selector selector = Selector.open();
            // 将Channel注册到Selector，指定事件为OP_ACCEPT事件
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("启动服务成功");

            while (true) {
                // 阻塞等待需要处理的事件发生，事件发生后跳出阻塞，执行下面的代码
                selector.select();

                // 获取selector中注册的全部事件的SelectionKey
                // 这里为上面注册的SelectionKey.OP_ACCEPT，和下面注册的
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();

                // 遍历selectionKeys处理
                while (iterator.hasNext()){
                    SelectionKey selectionKey = iterator.next();

                    // 如果是OP_ACCEPT事件，进行连接获取和事件注册
                    if (selectionKey.isAcceptable()){
                        // 获取注册的渠道
                        ServerSocketChannel channel = (ServerSocketChannel) selectionKey.channel();
                        // 进行建立连接
                        SocketChannel socketChannel = channel.accept();
                        // 设置成非阻塞
                        socketChannel.configureBlocking(false);
                        // 将链接后的Channel注册到Selector，处理读事件
                        socketChannel.register(selector, SelectionKey.OP_READ);

                        System.out.println("客户端链接成功");
                    } else if (selectionKey.isReadable()){
                        // 获取注册的渠道
                        SocketChannel socketChannel = (SocketChannel) selectionKey.channel();

                        // 使用java.nio.ByteBuffer类中的方法allocate()分配新的ByteBuffer。
                        // 该方法需要一个参数，即缓冲区的容量。它返回分配的新的ByteBuffer。
                        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
                        int len = socketChannel.read(byteBuffer);
                        if (len > 0) {
                            System.out.println("read data: " + new String(byteBuffer.array()));
                        }else if (len == -1){
                            System.out.println("客户端断开连接");
                            socketChannel.close();
                        }
                    }
                    // 从集合里删除本次处理的key，防止重复处理
                    iterator.remove();
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }

    }
}
```

# 11、双亲委派机制

**类加载器分类**
JVM支持两种加载器，分别为引导类加载器（BootstrapClassLoader）和自定义类加载器。从概念上来说自定义加载器一般是程序中由开发人员定义的一类加载器，然而java虚拟机规范中并没有这样定义，而是将所有派生于抽象类ClassLoader的类加载器都划分为自定义加载器。

**在java8以及以前的版本都会用到如下三种加载器：**

启动类加载器（Bootstrap Class Loader）
扩展类加载器（Extension Class Loader）
应用类加载器（Application Class Loader)

**启动类加载器**
该加载器使用C++实现（不会继承ClassLoader），是虚拟机自身的一部分。该类加载器主要是负责加载存放在JAVA_HOME\lib目录，或者被-Xbootclasspath参数指定路径存放的，并且是java虚拟机能识别的类库加载到虚拟机内存中。（eg:主要是加载java的核心类库，即加载lib目录下的所有class）

**扩展类加载器**
 这个类加载器主要是负责加载JAVA_HOME\lib\ext目录中，或者被java.ext.dirs系统变量所指定的路径中所有类库

**应用类加载器**
这个类的加载器是由sun.misc.Launcher$AppClassLoader来实现，因为该加载器是ClassLoader类中的getSystemClassLoader()方法的返回值，所以一般也称为该加载器为系统类加载器。该加载器主要是加载用户类路径上所有的类库，如果应用程序中没有定义过自己的类加载器，一般情况下这个就是程序的默认加载器。

**用户自定义类加载器**

User ClassLoader（用户自定义类加载器） ： 用户自定义的类加载器,可加载指定路径的class文件

![](./img/JDK8及以前双亲委派模型.jpg)

**双亲委派机制优点：**

**1.避免了类的重复加载**

**2.保护了程序的安全性，防止核**心的API被修改

**模块下的类加载器（1.9以及之后版本）**

在java9以及以后的版本中，为了模块化系统的顺利实施，模块下的类加载器主要有几个变动：

**1.扩展类加载器（Extension Class Loader）** 被平台类加载器（Platform ClassLoader）取代（java9中整个JDK都是基于了模块化的构建，原来的rt.jar和tools.jar都被拆分了数十个JMOD文件）。因为java类库可以满足扩展的需求并且能随时组合构建出程序运行的jre,所以取消了JAVA_HOME\lib\ext和JAVA_HOME\jre目录

**2.平台类加载器和应用程序类加载器都不在派生自java.net.URLClassLoader。** 现在启动类加载器、平台类加载器、应用程序类加载器全都继承于jdk.internal.loader.BuiltinClassLoader,在BuiltinClassLoader中实现了新的模块化架构下类

平台以及应用程序类加载器收到类的加载请求的时候，在委派给父类加载器家在之前，要先判断该类是否归属于摸一个系统模块，如果可以找到这样的归属关系，就要优先委派给负责那个模块的加载器完成加载。

![](./img/JDK1.9以及之后版本双亲委派模型.jpg)

**如何破坏双亲委派机制**
1、如果想打破双亲委派模型需要重写loadClass()方法。如果不想打破双亲委派模型，就重写ClassLoader类中的findClass()方法即可，无法被父类加载器加载的类最终会通过这个方法被加载。
2、使用线程上下文加载器，可以通过java.lang.Thread类的setContextClassLoader()方法来设置当前类使用的类加载器类型。

实际例子:
比如java.jdbc.Driver接口，它只是一个数据库驱动接口，这个接口是由启动类加载器加载的。
但是java.jdbc.Driver接口的实现是由各大数据库厂商来完成的，既然是自己实现的代码，就应该由应用类加载器来加载。于是就出现了启动类加载器加载的类要调用应用类加载器加载的实现。

# 12、序列化和反序列化

序列化目的是为了解决网络通信之间的对象传输问题。
就是如何把当前 JVM 进程里面的一个对象，跨网络传输到另外一个 JVM进程里面
**序列化，就是把内存里面的对象转化为字节流，以便用来实现存储或者传输。**
**反序列化，就是根据从文件或者网络上获取到的对象的字节流，根据字节流里面保存的对象描述信息和状态。重新构建一个新的对象。**

序列化的前提是保证通信双方对于对象的可识别性，多数情况下会把对象先转化为通用的解析格式，比如 json、xml 等。然后再把他们转化为数据流进行网络传输，从而实现跨平台和跨语言的可识别性。

开源的序列化技术非常多，比如 Json、Xml、Protobuf、Kyro、hessian 等等。
选择具体技术的几个关键因素
序列化之后的数据大小，因为数据大小会影响传输性能
序列化的性能，序列化耗时较长会影响业务的性能
是否支持跨平台和跨语言
技术的成熟度，越成熟的方案使用的公司越多，也就越稳定。

**序列化的实现**
1、在java中，只要一个类实现了**java.io.Serializable**接口，就可以被序列化
2、使用ObjectOutputStream和ObjectInputStream来进行对象的序列化和反序列化
3、在类中增加writeObject和readObject可以自定义序列化策略

**序列化版本 UID**
实现java.io.Serializable接口后Java 为每个类生成唯一的 serialVersionUID。它是一个长整型数字，可以保证在不同机器上反序列化时对象内容的兼容性和正确性。
Java序列化机制为每个可序列化的类都分配了一个序列化ID。 序列化ID是一个唯一标识，它是通过hashCode算法根据类的名字、接口列表以及成员（除了transient修饰）等构成的。 通过对象的序列化ID进行版本控制，在反序列化对比时，如果序列化ID不一致则会抛出InvalidClassException异常。因此可以认为序列化ID是一个秘钥。

**transient 关键字**
transient关键字表示瞬态，当我们将一个属性标记为transient后，那么这个属性就不会参与序列化和反序列化操作。因此，它在序列化的时候也不会被保存下来，减小了序列化对象的大小。

Java 序列化的局限性

不同版本之间的**兼容性问题**：如果类的结构发生了变化，则可能导致反序列化失败。

**安全性问题**：如果一个类包含一定的敏感信息，那么序列化这个对象时，数据也会被序列化进去。如果没有进行加密处理，则数据可能会被窃取。

**性能问题**：Java 的序列化和反序列化是比较慢的过程。

为了解决这些问题，可以使用其他替代方案，如 JSON、Protobuf 和 Thrift等。

# 13、SPI机制

SPI 全称：Service Provider Interface

字面意思：服务提供者的接口

**为什们要使用spi:**

在面向对象编程中，基于开闭原则和解耦的需要，一般建议用接口进行模块之间通信编程，通常情况下调用方模块是不会感知到被调用方模块的内部具体实现。
为了实现在模块装配的时候不用在程序里面动态指明，这就需要一种服务发现机制。Java SPI 就是提供了这样一个机制：为某个接口寻找服务实现的机制。这有点类似 IoC 的思想，将装配的控制权移交到了程序之外。

**重新理解spi机制：**
SPI是专门提供给服务提供者或者扩展框架功能的开发者去使用的一个接口。
SPI 将服务接口和具体的服务实现分离开来，将服务调用方和服务实现者解耦，能够提升程序的扩展性、可维护性。修改或者替换服务实现并不需要修改调用方。

**SPI整体机制图如下：**

![](./img/SPI整体机制图.png)

**Spring SPI**
在Spring中提供了SPI机制，我们只需要在 META-INF/spring.factories 中配置接口实现类名，即可通过服务发现机制，在运行时加载接口的实现类

spring-core包里定义了**SpringFactoriesLoader**类，这个类实现了检索**META-INF/spring.factories**文件，并获取指定接口的配置的功能。在这个类中定义了两个对外的方法：
loadFactories 根据接口类获取其实现类的实例，这个方法返回的是对象列表。
loadFactoryNames 根据接口获取其接口类的名称，这个方法返回的是类名的列表。
上面的两个方法的关键都是从指定的ClassLoader中获取spring.factories文件，并解析得到类名列表

在 spring-boot-autoconfigure 模块下，SpringBoot默认就配置了很多接口的服务实现；
其实我们在使用三方 spring-boot-starter就是使用这种机制，我们的Application显然不会和三方jar处于同包或者子包。三方Bean Configuration需要加载就需要使用spring 的spi机制。

# 14、一致性Hash算法

## 算法原理

一致性哈希算法在1997年由麻省理工学院提出，是一种特殊的哈希算法，在移除或者添加一个服务器时，能够尽可能小地改变已存在的服务请求与处理请求服务器之间的映射关系；

一致性哈希解决了简单哈希算法在分布式哈希表（Distributed Hash Table，DHT）中存在的动态伸缩等问题。

一致性hash算法本质上也是一种取模算法。不过，不同于上边按服务器数量取模，一致性hash是对固定值2^32取模。

**IPv4的地址是4组8位2进制数组成，所以用2^32可以保证每个IP地址会有唯一的映射。**

**hash环：**
我们可以将这2^32个值抽象成一个圆环，圆环的正上方的点代表0，顺时针排列，以此类推：1、2、3…直到2^32-1，而这个由2的32次方个点组成的圆环统称为hash环。

## 对象key映射到服务器

在对对应的Key映射到具体的服务器时，需要首先计算Key的Hash值：hash（key）% 2^32。

注：此处的Hash函数可以和之前计算服务器映射至Hash环的函数不同，只要保证取值范围和Hash环的范围相同即可（即：2^32）

将Key映射至服务器遵循下面的逻辑：

**从缓存对象key的位置开始，沿顺时针方向遇到的第一个服务器，便是当前对象将要缓存到的服务器。**

## 数据偏斜&服务器性能平衡问题

在实际场景中很难选取到一个Hash函数这么完美的将各个服务器散列到Hash环上。此时，在服务器节点数量太少的情况下，很容易因为节点分布不均匀而造成数据倾斜问题。

**虚拟节点**
针对数据偏斜&服务器性能平衡问题，可以通过：引入虚拟节点来解决负载不均衡的问题：即将每台物理服务器虚拟为一组虚拟服务器，将虚拟服务器放置到哈希环上，如果要确定对象的服务器，需先确定对象的虚拟服务器，再由虚拟服务器确定物理服务器。

虚拟节点的hash计算通常可以采用：对应节点的IP地址加数字编号后缀 hash（10.24.23.227#1) 的方式；

举个例子，node-1节点IP为10.24.23.227，正常计算node-1的hash值：
hash（10.24.23.227#1）% 2^32
假设我们给node-1设置三个虚拟节点，node-1#1、node-1#2、node-1#3，对它们进行hash后取模：
hash（10.24.23.227#1）% 2^32
hash（10.24.23.227#2）% 2^32
hash（10.24.23.227#3）% 2^32

注意：
分配的虚拟节点个数越多，映射在hash环上才会越趋于均匀，节点太少的话很难看出效果。
引入虚拟节点的同时也增加了新的问题，要做虚拟节点和真实节点间的映射，对象key->虚拟节点->实际节点之间的转换。

# 布隆过滤器

布隆过滤器（Bloom Filter）是一种空间效率极高的概率型数据结构，由 Burton Howard Bloom 在1970年提出。它主要用于判断一个元素是否可能属于某个集合，而不支持直接获取集合中的所有元素。
布隆过滤器的基本结构是一个固定长度的位数组和一组哈希函数。

## 工作原理

1、初始化时，位数组的所有位都被设置为0。
2、当要插入一个元素时，使用**预先设定好的多个独立、均匀分布的哈希函数对元素进行哈希运算**，每个哈希函数都会计算出一个位数组的索引位置。
3、将通过哈希运算得到的每个索引位置的位设置为1。
4、查询一个元素是否存在时，同样用相同的哈希函数对该元素进行运算，并检查对应位数组的位置是否都是1。如果所有位都为1，则认为该元素可能存在于集合中；如果有任何一个位为0，则可以确定该元素肯定不在集合中。

由于哈希碰撞的存在，当多位同时为1时，可能出现误报（False Positive），即报告元素可能在集合中，但实际上并未被插入过。但布隆过滤器不会出现漏报（False Negative），即如果布隆过滤器说元素不在集合中，则这个结论是绝对正确的。

**布隆过滤器的优点**
空间效率高：相比于精确存储所有元素的数据结构，布隆过滤器所需的内存空间小得多。
查询速度快：只需要执行几个哈希函数并检查位数组即可完成查询。

**布隆过滤器的缺点**
不可删除：标准布隆过滤器不支持元素的删除操作，因为无法得知哪些位仅是因为当前查询的元素而置1的。
误报率：随着元素数量增加，误报率也会逐渐升高，但是可以通过调整位数组大小和哈希函数数量来控制误报率。

**布隆过滤器的使用场景**
缓存穿透防护
重复数据检测
垃圾邮件过滤
网络安全：网络防火墙和入侵检测系统中，用于过滤已知恶意IP或攻击特征。

# 加密后的敏感字段如何进行模糊查询

## 方法一：

在数据库中实现与程序一致的加解密算法，修改模糊查询条件，使用数据库加解密函数先解密再模糊查找，这样做的优点是实现成本低，开发使用成本低，只需要将以往的模糊查找稍微修改一下就可以实现，但是缺点也很明显，这样做无法利用数据库的索引来优化查询，甚至有一些数据库可能无法保证与程序实现一致的加解密算法，但是对于常规的加解密算法都可以保证与应用程序一致
如果对查询性能要求不是特别高、对数据安全性要求一般，可以使用常见的加解密算法比如说AES、DES之类的也是一个不错的选择

## 方法二：

对密文数据进行分词组合，将分词组合的结果集分别进行加密，然后存储到扩展列，查询时通过key like '%partial%'，这是一个比较划算的实现方法，我们先来分析一下它的实现思路
先对字符进行固定长度的分组，将一个字段拆分为多个，比如说根据4位英文字符（半角），2个中文字符（全角）为一个检索条件，举个例子：
ningyu1使用4个字符为一组的加密方式，第一组ning ，第二组ingy ，第三组ngyu ，第四组gyu1 … 依次类推
如果需要检索所有包含检索条件4个字符的数据比如：ingy ，加密字符后通过 key like “%partial%” 查库