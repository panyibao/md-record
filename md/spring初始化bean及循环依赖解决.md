# spring初始化bean

## 一，初始化执行顺序：

beanFactory里有bean的定义信息beanDefinition，下面先说beanFactory 和 

AbstractApplicationContext.refresh()获取beanFactory 的过程：

## 1、通过SpringApplication.run()启动

直接点进源码看内部调用：

![image-20220402171156989](./img/image-20220402171156989.png)

//使用ConfigurableApplicationContext上下文

ConfigurableApplicationContext context = null;

context = createApplicationContext();

createApplicationContext返回AnnotationConfigServletWebServerApplicationContext类型的对象

AnnotationConfigServletWebServerApplicationContext创建时会创建两个bean扫描器

![image-20220318152448705](./img/springBean扫描器.png)

这里创建时AnnotationConfigServletWebServerApplicationContext对象时，父类GenericApplicationContext 会创建beanFactory 类型为DefaultListableBeanFactory

![image-20220318152554526](./img/创建beanFactory.png)

// 进入refreshContext初始化

refreshContext(context);

## 2、refreshContext

调用refresh(context);

## 3、refresh方法

refresh 调用传入context对象的refresh方法

**applicationContext.refresh();**
这里applicationContext为AnnotationConfigServletWebServerApplicationContext类型

AnnotationConfigServletWebServerApplicationContext继承ServletWebServerApplicationContext

所以会调用ServletWebServerApplicationContext的refresh()方法，

再调用super.refresh();

最终调用的方法为

**AbstractApplicationContext.refresh()**

## 4、bean的初始化，和生命周期

AbstractApplicationContext.refresh()包含了bean的所有生命周期

第二个步骤，获取一个beanFactory ，

ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

内部调用refreshBeanFactory()

这里context为ServletWebServerApplicationContext类型

ServletWebServerApplicationContext继承自GenericWebApplicationContext继承自GenericApplicationContext

所以最终调用的是

GenericApplicationContext.refreshBeanFactory()

GenericApplicationContext 是AnnotationConfigServletWebServerApplicationContext的父类，在创建context时就已经初始化了beanFactory 类型为DefaultListableBeanFactory

通过getBeanFactory();返回到AbstractApplicationContext.refresh()方法中。

```java
public void refresh() throws BeansException, IllegalStateException {
   synchronized (this.startupShutdownMonitor) {
      StartupStep contextRefresh = this.applicationStartup.start("spring.context.refresh");

      // 真正做refresh操作之前需要准备做的事情如：
      // 设置Spring容器的启动时间，
      // 开启活跃状态，撤销关闭状态，。
      // 初始化context environment（上下文环境）中的占位符属性来源。
      // 验证环境信息里一些必须存在的属性
      prepareRefresh();

      // 让这个类（AbstractApplicationContext）的子类(实际使用的实现类)刷新内部bean工厂。
      // AbstractRefreshableApplicationContext容器：实际上就是重新创建一个bean工厂，并设置工厂的一些属性。
      // GenericApplicationContext容器：获取创建容器的就创建的bean工厂，并且设置工厂的ID.
      ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

      // 上一步已经把工厂建好了，但是还不能投入使用，因为工厂里什么都没有，还需要配置一些东西。看看这个方法的注释
      // 这里设置工厂的属性如：
      // 设置BeanFactory的类加载器,设置环境变量...
      prepareBeanFactory(beanFactory);

      try {
         // prepareBeanFactory设置了一些公共的BeanFactory信息，具体实现类可能还需要做一些额外的操作
         // 这里在这个类（AbstractApplicationContext）中是个空方法，具体由实现类实现如：
         // AnnotationConfigServletWebServerApplicationContext.postProcessBeanFactory中执行了
         // 包扫描（ClassPathBeanDefinitionScanner，basePackages属性），和注解扫描（AnnotatedBeanDefinitionReader bean注解）。
         postProcessBeanFactory(beanFactory);

         StartupStep beanPostProcess = this.applicationStartup.start("spring.context.beans.post-process");

         // 找出实现了BeanFactoryPostProcessor接口的processor并执行
         // 通过该方法可以注入第三方框架的bean到spring容器中，如mybatis框架，
         invokeBeanFactoryPostProcessors(beanFactory);

         // 从Spring容器中找出的BeanPostProcessor接口的bean，并设置到BeanFactory的属性中
         // 之后bean被实例化的时候会调用这个BeanPostProcessor
         registerBeanPostProcessors(beanFactory);
         beanPostProcess.end();

         // 初始化MessageSource组件（做国际化功能；消息绑定，消息解析）,这个接口提供了消息处理功能。主要用于国际化/i18n
         initMessageSource();

         // 在Spring容器中初始化事件广播器，事件广播器用于事件的发布
         initApplicationEventMulticaster();

         // 具体子类实现
         onRefresh();

         // 注册应用的监听器
         registerListeners();

         // 实例化BeanFactory中已经被注册但是未实例化的所有实例(懒加载的不需要实例化)。
         finishBeanFactoryInitialization(beanFactory);

         // refresh做完之后需要做的其他事情，清除上下文级别的资源缓存等
         finishRefresh();
      }

      catch (BeansException ex) {
         if (logger.isWarnEnabled()) {
            logger.warn("Exception encountered during context initialization - " +
                  "cancelling refresh attempt: " + ex);
         }

         // Destroy already created singletons to avoid dangling resources.
         destroyBeans();

         // Reset 'active' flag.
         cancelRefresh(ex);

         // Propagate exception to caller.
         throw ex;
      }

      finally {
         // Reset common introspection caches in Spring's core, since we
         // might not ever need metadata for singleton beans anymore...
         resetCommonCaches();
         contextRefresh.end();
      }
   }
}
```

refresh详细描述推荐查看https://www.cnblogs.com/grasp/p/11942735.html

## 二、spring环境准备

## 1、SpringApplication.run方法

![image-20220322103229600](./img/springRun.png)

上图第1步创建了一个监听器，在第2步准备环境中调用的这个监听器进行准备环境，实际上进入1的方法可以知道监听器仍然是调用的SpringApplication.run方法，所以在Debugger模式下会看到SpringApplication.run被调用了多次。这个关注实际初始化我们项目bean的那次就行。

可以看到创建context时AnnotationConfigServletWebServerApplicationContext创建时会创建两个bean扫描器,

![image-20220321153159539](./img/springBean扫描器.png)

## 2、AnnotatedBeanDefinitionReader

创建时通过AnnotationConfigUtils.registerAnnotationConfigProcessors(this.registry)向容器中添加了一些注解配置处理器：

![image-20220321153530751](./img/spring初始化注解扫描.png)

这里主要关注一下几个处理器：

**负责解析处理所有@Configuration标签：**

org.springframework.context.annotation.internalConfigurationAnnotationProcessor

**负责解析处理所有@Autowired标签：**

org.springframework.context.annotation.internalAutowiredAnnotationProcessor

**负责解析处理所有JSR 250 规范的标签（如@Resource）：**

org.springframework.context.annotation.internalCommonAnnotationProcessor

## 3、ClassPathBeanDefinitionScanner

![image-20220321160022655](./img/ClassPathBeanDefinitionScanner.png)

ClassPathBeanDefinitionScanner通过registerDefaultFilters()注册了一些注解过滤器，这个加了对@Component的扫描，而@Repository，@Controller，@Service，都包含有@Component所以也会被spring扫描到

![image-20220321160521269](./img/注解过滤器.png)

在run方法的prepareContext方法中通过load(context, sources.toArray(new Object[0]));方法将运行的主类BeanDefinition添加到了spring容器中

![image-20220322111922433](./img/prepareContext.png)

在**refresh**方法中**invokeBeanFactoryPostProcessors**会执行一些BeanFactoryPostProcessor（Bean工厂的后置处理器）

进入到PostProcessorRegistrationDelegate.invokeBeanFactoryPostProcessors执行方法，通过debugger可以知道

解析并加载项目中的BeanDefinition到spring容器中的就是这个方法。

**PostProcessorRegistrationDelegate.invokeBeanFactoryPostProcessors**方法中执行了invokeBeanDefinitionRegistryPostProcessors方法，这些PostProcessors中就包含的都上面提到的@Autowired等注解，标签的扫描，将对应的BeanDefinition加载到spring容器中。

其中**ConfigurationClassPostProcessor.processConfigBeanDefinitions**通过parser.parse(candidates);扫描并解析了注解相关的类。

## 4、初始化bean

在**refresh**方法中调用**finishBeanFactoryInitialization(beanFactory);**实例化所有的非懒加载单例。

通过前面的步骤beanFactory中已经有了实例化bean所需的beanDefinition，方法中调用**beanFactory.preInstantiateSingletons();**进行实例化。

实际创建bean的方法：**AbstractAutowireCapableBeanFactory.doCreateBean**

创建实例完成后调用**populateBean(beanName, mbd, instanceWrapper);**填充属性值

调用**InjectionMetadata.inject**进行属性注入,代码顺序如下：

**populateBean(beanName, mbd, instanceWrapper)：**

![image-20220402095021665](./img/image-20220402095021665.png)

**AutowiredAnnotationBeanPostProcessor.postProcessProperties**：

![image-20220402095313390](./img/image-20220402095313390.png)

**InjectionMetadata.inject:**

![image-20220402095754655](./img/image-20220402095754655.png)

最终从beanFactory.getBean(beanName);获取或创建需要依赖的bean。

**5、循环依赖问题**

从初始化bean过程可以知道是先创建bean然后注入依赖，这就会出现循环依赖的问题：即beanA中有属性B,beanB中有属性A。

**AbstractBeanFactory.doGetBean**方法创建A之前会首先调用**getSingleton(String beanName, ObjectFactory<?> singletonFactory)**方法尝试从单例对象缓存池（**DefaultSingletonBeanRegistry.singletonObjects**）获取bean，如果单例对象缓存池不存在则调用**beforeSingletonCreation**方法，该方法检查当前bean是否正在创建，如果不是建beanName添加到正在创建的bean的名称集合中，如果存在说明有循环依赖，抛出异常。DefaultSingletonBeanRegistry.singletonsCurrentlyInCreation为正在创建的bean的名称集合。

![image-20220402144235362](./img/image-20220402144235362.png)

![image-20220402144857844](./img/image-20220402144857844.png)

## 5、循环依赖解决

在创建bean后，注入依赖之前spring的doCreateBean方法为解决循环依赖做了些事，如下图

![image-20220402155029425](./img/image-20220402155029425.png)

这里主要关注解决循环依赖的代码：

![image-20220402155257289](./img/image-20220402155257289.png)

这里一开始做了一个判断：

```java
mbd.isSingleton()//是否是单例对象
isSingletonCurrentlyInCreation(beanName)//是否正在创建中
this.allowCircularReferences// 是否允许循环引用
```

其中**this.allowCircularReferences**是配置项：

```yaml
spring:
    main:
          allow-circular-references: false
```

如果开启了则执行解决循环引用的逻辑提前暴露引用（**addSingletonFactory**方法，将bean封装后传到方法中），可以看到实际传进来的是一个lambda表达式 **() -> getEarlyBeanReference(beanName, mbd, bean)** 这个传进来的时候包含了String beanName bean名称, RootBeanDefinition mbd Bean定义信息, Object bean  目标对象，

传递进来的lambda表达式并不会执行：

![image-20220402155854389](./img/image-20220402155854389.png)

可以看到addSingletonFactory方法中将，将封装后的bean对象添加到了singletonFactories（三级缓存）中。给后面循环引用的bean使用。

之后开始调用populateBean(beanName, mbd, instanceWrapper);注入当前bean的依赖，然后会遇到循环依赖的对象。

为了解决单例循环依赖问题，在AbstractBeanFactory.doGetBean方法中会在开始的地方先调用一遍getSingleton(beanName);这个和上面的getSingleton不是同一个方法（方法重载）getSingleton(beanName)方法首先会从单例对象缓存池获取bean

![image-20220402151544321](./img/image-20220402151544321.png)

![image-20220402151503646](./img/image-20220402151503646.png)

![image-20220402161815292](./img/image-20220402161815292.png)

可以看到从三级缓存获取到singletonFactory然后执行了singletonFactory.getObject();得到Bean对象，这里其实就是获取到之前存在三级缓存的lambda表达式 **() -> getEarlyBeanReference(beanName, mbd, bean)**  读取出来然后执行得到Bean对象。

**getEarlyBeanReference方法：**

![](./img/springbean创建-getEarlyBeanReference方法.jpg)

getEarlyBeanReference方法调用了bp.getEarlyBeanReference(exposedObject, beanName);一路跟踪下去会看到如下代码：AbstractAutoProxyCreator#wrapIfNecessary(),这个方法判断是否需要进行AOP，如果需要生成代理对象返回，否则直接返回普通对象。

![](./img/spring-bean创建-循环依赖创建aop代理对象.jpg)

从三级缓存获取到数据后就不会在执行创建bean的代码重新重新创建bean，这里可以看到从三级缓存拿到数据后就把数据移到了二级缓存earlySingletonObjects中，下一个和这个bean同样有循环依赖的可以直接从earlySingletonObjects读到。

![image-20220402163759059](./img/image-20220402163759059.png)

# 三、备注

## 1、earlySingletonObjects

earlySingletonObjects为二级缓存，再从一级缓存找，再从二级缓存找，再从三级缓存找。 earlySingletonObjects相当于循环对象依赖列表，对象在创建之后，进行注入过程中，发现产生了循环依赖，那么会将对象放入到这个队列，并且从singletonFactories中移除掉。

通过上面可以知道singletonFactories（三级缓存）存储的是lambda表达式 **() -> getEarlyBeanReference(beanName, mbd, bean)** 读取出来之后需要执行完才能获得Bean对象，有如下情况:

```textile
A包含属性B
A包含属性C
B包含属性A
C包含属性A
```

这种情况下A和B,C都出现了循环依赖，在先填充完B属性之后会填充C属性，此时C不能重复执行lambda表达式，否则可能又创建一个新的A的代理对象，为了解决这个问题，看DefaultSingletonBeanRegistry#getSingleton(String beanName, oolean allowEarlyReference)方法执行的时候：

![](./img/image-20220402161815292.png)

可以看到，当创建B执行lambda表达式获取到A的bean对象的时候，会将A放到earlySingletonObjects中缓存，并删除lambda表达式对象，再创建C时可以直接拿到earlySingletonObjects缓存中A的bean对象，而不是重复创建

从这里看出：earlySingletonObjects存的是正在创建中的，已经完成AOP代理过程的对象（如果不需要AOP代理就是普通对象），提供给其他循环依赖的对象使用

## 2、allowCircularReferences配置项

yml:配置：

```yaml
spring:
    main:
          allow-circular-references: false
```

在运行
