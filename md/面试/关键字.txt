java关键字 含义
abstract ：表明类或者成员方法具有抽象属性
synchronized： 表明一段代码需要同步执行
throw ：抛出一个异常
throws ：声明在当前定义的成员方法中所有需要抛出的异常
volatile ：表明两个或者多个变量必须同步地发生变化
continue ：回到一个块的开始处
default ：默认，例如，用在switch语句中，表明一个默认的分支
enum ：枚举
extends ：表明一个类型是另一个类型的子类型，这里常见的类型有类和接口
final ：用来说明最终属性，表明一个类不能派生出子类，或者成员方法不能被覆盖，或者成员域的值不能被改变，用来定义常量
finally ：用于处理异常情况，用来声明一个基本肯定会被执行到的语句块
implements： 表明一个类实现了给定的接口
catch ：用在异常处理中，用来捕捉异常
const ：保留关键字，没有具体含义
assert： 断言，用来进行程序调试
import ：表明要访问指定的类或包
instanceof ：用来测试一个对象是否是指定类型的实例对象
protected： 一种访问控制方式：保护模式
short ：基本数据类型之一,短整数类型
static ：表明具有静态属性
super ：表明当前对象的父类型的引用或者父类型的构造方法
switch ：分支语句结构的引导词
java.io.Serializable 序列化接口
transient 关键字表示瞬态，当我们将一个属性标记为transient后，那么这个属性就不会参与序列化和反序列化操作。
finalize： finalize是java.lang.Object类中的一个方法。在Java中，所有类都隐式地继承自Object类。finalize方法在垃圾回收器清理对象之前被调用，用于执行对象的清理工作。一般情况下，不建议重写finalize方法，因为其执行时间和调用次数是不确定的，而且在Java 9及之后的版本中，finalize方法已经被标记为废弃。为了更好地管理资源，可以使用try-with-resources语句或显式地关闭资源。

线程生命周期及五种状态
New(初始化状态)Runnable(就绪状态)Running(运行状态)Blocked(阻塞状态)Terminated（终止状态）

新建状态（New）运行状态（Running）阻塞状态（Blocked）就绪状态（Ready）等待状态（Waiting）结束状态（Terminated）


mysql关键字 含义
UNION：用于合并两个或多个SELECT语句的结果集。UNION关键字后面需要指定要合并的SELECT语句

MySQL常用函数大全
1、ABS(x) 返回x的绝对值
SELECT ABS(-1);

2、AVG(expression) 返回一个表达式的平均值，expression 是一个字段
SELECT AVG(score) FROM sc;

3、CEIL(x)/CEILING(x) 返回大于或等于 x 的最小整数
SELECT CEIL(1.5);
SELECT CEILING(1.5);

4、FLOOR(x) 返回小于或等于 x 的最大整数
SELECT FLOOR(1.5); 

5、ROUND(x)返回离 x 最近的整数

6、P(x) 返回 e 的 x 次方
SELECT EXP(3);

7、GREATEST(expr1, expr2, expr3, …) 返回列表中的最大值

8、LEAST(expr1, expr2, expr3, …) 返回列表中的最小值 

9、RAND()返回 0 到 1 的随机数

10、TRIM(s)去掉字符串 s 开始和结尾处的空格

11、LTRIM(s)去掉字符串 s 开始处的空格

12、RTRIM(s)去掉字符串 s 结尾处的空格

13、SUBSTR/SUBSTRING(s, start, length)从字符串 s 的 start 位置截取长度为 length 的子字符串

14、concat(str1, str2,...) 多个字符串连接成一个字符串

15、CURDATE()/CURRENT_DATE()返回当前日期

16、CURRENT_TIME()/CURTIME()返回当前时间

17、CURRENT_TIMESTAMP()返回当前日期和时间

18、IFNULL() 函数用于判断第一个表达式是否为 NULL，如果第一个值不为NULL就执行第一个值。第一个值为 NULL 则返回第二个参数的值。

19、to_date函数示例
将字符串“2018-11-20”转换为日期格式的示例：

SELECT to_date('2018-11-20','%Y-%m-%d');

输出结果：2018-11-20。

各类日期格式
在使用to_date函数时，需要使用正确的日期格式。以下是常用的日期格式：
%y：两位数字的年份。
%Y：四位数字的年份。
%m：月份（01-12）。
%d：日期（01-31）。
%H：小时（00-23）。
%h：小时（01-12）。
%i：分钟（00-59）。
%s：秒钟（00-59）。
%p：AM或PM。

20、to_char函数用于将日期或时间类型转化为指定格式的字符串
TO_CHAR(dateExp, format)
SELECT TO_CHAR(NOW(), 'YYYY-MM-DD HH:mm:ss');



sql:
建表
CREATE TABLE NAME (
    id INT(11) NOT NULL auto_increment COMMENT '编号',
    student_name VARCHAR(50) COMMENT '学生姓名',
    introduce TEXT COMMENT '介绍',
    content1 ENUM('篮球', '足球', '乒乓球') COMMENT '爱好1',
    content2 VARCHAR(255) COMMENT '爱好2',
    content3 VARCHAR(500) COMMENT '爱好3',
    content4 VARCHAR(1000) COMMENT '爱好4',
    description1 BINARY COMMENT '描述1',
    description2 VARBINARY(1000) COMMENT '描述2',
    description3 VARBINARY(2000) COMMENT '描述3',
    iq INT COMMENT '0805数据库IQ',
    age INT COMMENT '年龄',
    three_plan INT COMMENT '三年计划',
    code_num INT UNSIGNED COMMENT '代码量',
    price DECIMAL(8, 2) COMMENT '学费',
    book DECIMAL(10, 2) COMMENT '书本费',
    total_amount DECIMAL(12, 2) COMMENT '总金额',
    ruxueriqi DATE COMMENT '入学日期',
    start TIMESTAMP COMMENT '上课时间',
    end_time TIME COMMENT '下课时间',
    ruxuenianfen INT COMMENT '入学年份'
) ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

创建索引：
mysql:
CREATE INDEX indexName ON tableName (columnName(length));
	
ALTER TABLE tableName ADD INDEX indexName(columnName);

SHOW PROCESSLIST 命令可以查看当前正在执行的SQL语句和相关的会话信息。通过该命令，我们可以实时监控MySQL中正在执行的SQL语句，以及其执行状态、执行时间等信息。

oracle:
create index 索引名 on 表名(列名1, 列名2, 列名3, ...);

-- 开启SQL跟踪
ALTER SESSION SET SQL_TRACE = TRUE;

-- 查询当前正在执行的SQL语句
SELECT sql_id, sql_text
FROM   v$sql
WHERE  sql_id IN (SELECT sql_id FROM v$session WHERE status = 'ACTIVE');


limit 0,3


EXPLAIN 查看查询计划
ANALYZE TABLE：用于分析和更新表的统计信息，以帮助优化查询执行计划。
OPTIMIZE TABLE：用于优化表的物理存储结构，以提高查询性能。
SHOW INDEX：显示表的索引信息，包括索引名称、字段、唯一性等。
SHOW CREATE TABLE：显示创建表的SQL语句，包括表的结构、索引等信息。
SHOW TABLE STATUS：显示关于表的详细信息，包括行数、大小、碎片化等。
SHOW VARIABLES：显示MySQL服务器的变量设置，包括缓存大小、连接数等配置参数。
SET optimizer_switch='...'：通过设置优化器开关，可以改变优化器的行为，以调整查询的执行计划。
MySQL Workbench：MySQL官方提供的可视化工具，可以使用查询优化器分析器来检查查询的性能问题和优化建议。








