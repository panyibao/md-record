windows docker-machine 操作

powershell停止虚拟机（名称default）：
docker-machine stop default

链接虚拟机：
docker-machine ssh default

查看虚拟机IP
docker-machine ip default

# docker命令

重启docker

```shell
systemctl restart docker
```

拉取镜像

```shell
docker pull ubuntu
```

查看所有的容器

```shell
# -a 列出当前所有正在运行的容器+历史上运行过得容器
docker ps -a
# -l 显示最近创建的容器
docker ps -l
# -n 显示最近n个创建的容器
docker ps -a
# -q 静默模式，只显示容器的编号
docker ps -a
```

使用 docker start 启动一个已停止的容器

```shell
docker start b750bbbcfd88
```

停止容器的命令

```shell
docker stop <容器 ID>
```

停止的容器可以通过 docker restart 重启

```shell
docker restart <容器 ID或名字>
```

删除容器使用 **docker rm** 命令

```shell
docker rm -f 1e560fca3906
```

### 进入已启动的容器

```shell
docker exec -it 容器id /bin/bash
```

```shell
# 进入springboot应用容器
docker exec -it 243c32535da7 /bin/sh
```

在使用 **-d** 参数时，容器启动后会进入后台。此时想要进入容器，可以通过以下指令进入：

- **docker attach** : 直接进入容器启动命令的终端，不会启动新的进程，用exit退出会导致容器停止

- **docker exec**：在容器中打开新的终端，并且启动新的进程，用exit退出不会导致容器停止。推荐大家使用 docker exec 命令，因为此命令会退出容器终端，但不会导致容器的停止。

run 进入容器后exit命令退出，容器停止。ctrl+p+q退出，容器不停止

## 运行一个 springboot 应用

```shell
docker run -d -p 9076:9076 -p 6379:6379 -p 56120:56120 --privileged=true -v /home/docker/pan/signin/logs:/logs/market-server-signin --name market-server-signin signin-dev:v1.0
```

## 运行一个 web 应用

```shell
 # 载入镜像
docker pull training/webapp
 # 运行 d:让容器在后台运行，-P:将容器内部使用的网络端口随机映射到我们使用的主机上。
docker run -d -P training/webapp python app.py
 # -p 指定映射窗口（主机端口:容器内部端口）
docker run -d -p 5001:5000 training/webapp python app.py
```

- **-P :** 容器内部端口随机映射到主机的端口。

- **-p :** 是容器内部端口绑定到**指定**的主机端口。
  
  注意大小写不同

## 查看 WEB 应用程序日志

```shell
# 查看容器内部的标准输出
docker logs [ID或者名字] 
# -f: 让 docker logs 像使用 tail -f 一样来输出容器内部的标准输出
docker logs -f bf08b7f2cd89
```

## 查看WEB应用程序容器的进程

```shell
docker top name
```

## 检查 WEB 应用程序

```shell
# 它会返回一个 JSON 文件记录着 Docker 容器的配置和状态信息
docker inspect name
```

## 列出镜像列表

```shell
docker images
```

## 重远程仓库查找镜像

```shell
docker search httpd
```

## 删除镜像

镜像删除使用 **docker rmi** 命令，比如我们删除 hello-world 镜像：

```shell
docker rmi hello-world
```

强制删除容器

```shell
docker rm -f <container_id_or_name>
```

## 查看镜像/容器/数据卷所占的空间

```shell
docker system df
```

## docker run参数

```shell
docker run --name=ubuntu -t -i ubuntu:15.10 /bin/bash
```

参数说明：

- **-i**: 交互式操作。
- **-t**: 终端。
- -d:让容器在后台运行
- --name=容器的新名字  为容器指定一个名称
- **ubuntu:15.10**: 这是指用 ubuntu 15.10 版本镜像为基础来启动容器。
- **/bin/bash**：放在镜像名后的是命令，这里我们希望有个交互式 Shell，因此用的是 /bin/bash。
- 退出终端直接使用exit命令

如果要使用版本为 14.04 的 ubuntu 系统镜像来运行容器时，命令如下：

```shell
docker run -t -i ubuntu:14.04 /bin/bash
```

如果你不指定一个镜像的版本标签，例如你只使用 ubuntu，docker 将默认使用 ubuntu:latest 镜像。

## 复制容器内文件到主机

**公式：docker cp 容器id:容器内路径 目的主机路径**

```shell
docker cp a516ea1d55ab:/home/pan/aaa.txt /home/pan/pan/docker/bak
```

### 导出和导入容器

```shell
# 导出本地某个容器，可以使用 docker export 命令
docker export 1e560fca3906 > ubuntu.tar
```

```shell
# 使用 docker import 从容器快照文件中再导入为镜像
# 以下实例将快照文件 ubuntu.tar 导入到镜像 test/ubuntu:v1
cat docker/ubuntu.tar | docker import - test/ubuntu:v1
```

## 创建镜像

当我们从 docker 镜像仓库中下载的镜像不能满足我们的需求时，我们可以通过以下两种方式对镜像进行更改。

- 1、从已经创建的容器中更新镜像，并且提交这个镜像
- 2、使用 Dockerfile 指令来创建一个新的镜像

### 更新镜像

更新镜像之前，我们需要使用镜像来创建一个容器。

```shell
docker run -t -i ubuntu:15.10 /bin/bash
```

在运行的容器内使用 **apt-get update** 命令进行更新。

在完成操作之后，输入 exit 命令来退出这个容器。

此时 ID 为 e218edb10161 的容器，是按我们的需求更改的容器。我们可以通过命令 docker commit 来提交容器副本。

```shell
docker commit -m="has update" -a="runoob" e218edb10161 runoob/ubuntu:v2
```

各个参数说明：

- **-m:** 提交的描述信息

- **-a:** 指定镜像作者

- **e218edb10161** 容器 ID

- **runoob/ubuntu:v2:** 指定要创建的目标镜像名

## 添加容器数据卷

将宿主机文件夹挂载到docker容器内

命令公式：

**docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录 镜像名**

例如：

```shell
docker run -it --privileged=true -v /home/pan/pan/docker/gz:/home/pan/docker ubuntu
```

- 大约在0.6版，privileged被引入docker。
- 使用该参数，container(容器)内的root拥有真正的root权限。
- 否则，container内的root只是外部的一个普通用户权限。

**容器卷作用**

容器的持久化
容器间继承+共享数据
卷就是目录或文件，存在于一个或多个容器中，由docker挂载到容器，但不属于联合文件系统，因此能够绕过Uniin File System提供一些用于持续存储或共享数据的特性:

卷的设计目的就是数据的持久化，完全独立于容器的生存周期，因此Docker不会在容器删除时删除其挂载的数据卷

**特点:**

数据卷可在容器之间共享或重用数据
卷中的更改可以直接生效
数据卷中的更改不会包含在镜像的更新中
数据卷的生命周期一直持续到没有容器使用它位置

## 安装mysql5.7.19

```shell
docker run -d -p 3316:3316 --privileged=true -v /pan/mysql/conf:/etc/mysql/conf.d -v /pan/mysql/logs:/logs -v /pan/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123.abc --name=mysql-5.7.19 mysql:5.7.19
```

命令含义：

**-d** ：后台运行
**-p 3316:3316**：将主机的3306端口映射到docker容器的3306端口
**--privileged=true**: 打开root的权限
**-v /pan/mysql/conf:/etc/mysql/conf.d**：将主机的/pan/mysql/的conf挂载到容器的/etc/mysql/conf.d
**-v /pan/mysql/logs:/logs**：将主机的/pan/mysql的logs目录挂载到容器的/logs
**-v /pan/mysql/data:/var/lib/mysql**：将主机/pan/mysql目录下的data目录挂载到容器的/var/lib/mysql
**-e MYSQL_ROOT_PASSWORD=123.abc**：初始化root用户密码。
**--name=mysql-5.7**：运行容器名字
**mysql:5.7.19**：mysql5.7.19镜像

**注意实际中一定要挂载数据卷，防止误删容器**

安装完成后查看字符集：

```sql
show variables where variable_name LIKE 'character\_set\_%' OR variable_name LIKE 'collation%';
```

在/pan/mysql/conf添加配置文件

```shell
cd /pan/mysql/conf
```

创建配置文件

```shell
touch my.cnf
```

配置文件mysql编码内容

```ini
[client]
port=3316
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4

[mysqld]
port=3316
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
```

重启容器

**注意如果启动容器后修改防火墙，需要重新启动douker，并重启容器**

# docker安装mysql主从

## 1、先安装两个mysql容器，mysql-master, mysql-slave，

计划mysql-master为主库，mysql-slave为从库

```shell
## 创建mysql-master
docker run -d -p 3317:3317 --privileged=true -v /pan/mysql-master/conf:/etc/mysql/conf.d -v /pan/mysql-master/logs:/logs -v /pan/mysql-master/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123.abc --name=mysql-master mysql:5.7.19
```

```shell
## 创建mysql-slave
docker run -d -p 3318:3318 --privileged=true -v /pan/mysql-slave/conf:/etc/mysql/conf.d -v /pan/mysql-slave/logs:/logs -v /pan/mysql-slave/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123.abc --name=mysql-slave mysql:5.7.19
```

## 2、编写mysql-master配置文件my.cnf

```ini
[client]
port=3317
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4

[mysqld]
port=3317
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
## 设置server_id，同一局域网中需要唯一
server_id=101
## 指定不需要同步的数据库名称
binlog-ignore-db=mysql  
## 指定需要同步的数据库名称  #binlog-do-db=
## 开启二进制日志功能  
#记录所有更改数据的语句，可以用于主从服务器之间的数据同步，以及服务器遇到故 障时数据的无损失恢复。
log-bin=mall-mysql-bin  
## 设置二进制日志使用内存大小（事务）
binlog_cache_size=1M  
## 设置使用的二进制日志格式（mixed,statement,row）
binlog_format=mixed  
#statement 记录所有的写操作到bin-log文件 缺点:sql语句执行set time=now() 会出现主从不一致  
#row 记录每行的变换  缺点:数据量大的时候,记录数据多
#mixed是statement和row的混合  
## 二进制日志过期清理时间。默认值为0，表示不自动清理。
expire_logs_days=7  
## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
## 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=1062

#通用查询日志
#记录所有连接的起始时间和终止时间，以及连接发送给数据库服务器的所有指令， 对我们复原操作的实际场
#景、发现问题，甚至是对数据库操作的审计都有很大的帮助。
general_log=ON  
general_log_file=/var/lib/mysql/mysql-general.log 

#慢查询日志
#记录所有执行时间超过long_query_time的所有查询，方便我们对查询进行优化。
slow_query_log=ON 
slow_query_log_file=/var/lib/mysql/mysql-slow.log 
long_query_time=3 
#设置慢查询的阈值为3秒，超出此设定值的SQL即被记录到慢查询日志
log_output=FILE
max_allowed_packet=200M

#错误日志
#记录MySQL服务的启动、运行或停止MySQL服务时出现的问题，方便我们了解服务器的 状态，从而对服务器进行维护。
log-error=/var/lib/mysql/mysql-error.log
```

## 3、编写mysql-slave配置文件my.cnf

```ini
[client]
port=3318
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4

[mysqld]
port=3318
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
## 设置server_id，同一局域网中需要唯一
server_id=102
## 指定不需要同步的数据库名称
binlog-ignore-db=mysql  
## 开启二进制日志功能，以备Slave作为其它数据库实例的Master时使用
log-bin=mall-mysql-slave1-bin  
## 设置二进制日志使用内存大小（事务）
binlog_cache_size=1M  
## 设置使用的二进制日志格式（mixed,statement,row）
binlog_format=mixed  
## 二进制日志过期清理时间。默认值为0，表示不自动清理。
expire_logs_days=7  
## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
## 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=all 

# relay_log配置中继日志
#用于主从服务器架构中，从服务器用来存放主服务器二进制日志内容的一个中间文件。 从服务器通过读取中继#日志的内容，来同步主服务器上的操作。
relay_log=mall-mysql-relay-bin  
## log_slave_updates表示slave将复制事件写进自己的二进制日志
log_slave_updates=1  
## slave设置为只读（具有super权限的用户除外）
read_only=1
```

## 4、重启mysql-master和mysql-slave

```shell
# 停止mysql-master
docker stop mysql-master
# 停止mysql-slave
docker stop mysql-slave

# 启动mysql-master
docker start mysql-master
# 启动mysql-slave
docker start mysql-slave
```

在主数据库中查看同步状态

```shell
# 进入容器
docker exec -it fd8079cd7290 /bin/bash
# 连接mysql
mysql -uroot -p
```

## 5、主机上建立帐户并授权 slave

创建用户给从机授权，为了让从机能够和主机连接起来

用户：slave，密码：123456

```sql
GRANT REPLICATION SLAVE ON *.* TO 'slave'@'%' IDENTIFIED BY '123456';
```

## 6、查询master状态

```sql
#查询master的状态
show master status; 
```

查询结果：

![](./img/docker-mysql主从1.PNG)

Binlog_Do_DB：需要复制的数据库。
Binlog_Ignore_DB：不需要复制的数据库。

记录下File和Position，配置slave需要用到

File：mall-mysql-bin.000001

Position：438

**注意：此步骤后不要再操作主服务器MySQL，防止主服务器状态值变化**

## 7、进入mysql-slave

```shell
docker exec -it mysql-slave /bin/bash
mysql -uroot -p
```

**设置复制主机的命令**

```sql
CHANGE MASTER TO MASTER_HOST='192.168.148.133',
MASTER_USER='slave',
MASTER_PASSWORD='123456',
MASTER_PORT=3317, 
MASTER_LOG_FILE='mall-mysql-bin.000001',MASTER_LOG_POS=438;
```

解释：
    CHANGE MASTER TO MASTER_HOST='主机的IP地址',
    MASTER_USER='slave'(刚刚配置的用户名),
    MASTER_PASSWORD='123456'(刚刚授权的密码),
    master_port=主机开放的端口 我这里是3310端口,
    MASTER_LOG_FILE='mysql-bin.具体数字',MASTER_LOG_POS=具体值(之前的Position);

**注意事项：如果在这里出现错误，先重置。执行完下面两条命令。**

```sql
stop slave;
reset master;
```

**执行上面的CHANGE MASTER命令后，启动从服务器复制功能**

```sql
start slave;
```

## 8、查看从服务器状态

```sql
show slave status\G
```

![](./img/docker-mysql主从2.PNG)

下面两个参数都是Yes，则说明主从配置成功！

```shell
Slave_IO_Running: Yes
Slave_SQL_Running: Yes
```

9、测试

主库新建数据库testdb 和数据表test_user,插入数据

![](./img/docker-mysql主从3.PNG)

从库查询数据：

![](./img/docker-mysql主从4.PNG)

测试主从同步成功。

# docker 搭建redis集群（3主3从）

对于大量数据（1~2亿），使用分布式缓存存储，集群机器节点分区方式主要要

1、哈希取余分区

2、一致性哈希算法分区

3、哈希槽分区（redis集群使用）

### 1、创建6个redis容器并运行

容器名：端口
redis-node-2:6382
redis-node-3:6383
redis-node-4:6384
redis-node-5:6385
redis-node-6:6386
redis-node-7:6387

执行命令：

```shell
docker run -d --name redis-node-2 --net host --privileged=true -v /pan/redis/share/redis-node-2:/data redis:6.2.6 --cluster-enabled yes --appendonly yes --port 6382

docker run -d --name redis-node-3 --net host --privileged=true -v /pan/redis/share/redis-node-3:/data redis:6.2.6 --cluster-enabled yes --appendonly yes --port 6383

docker run -d --name redis-node-4 --net host --privileged=true -v /pan/redis/share/redis-node-4:/data redis:6.2.6 --cluster-enabled yes --appendonly yes --port 6384

docker run -d --name redis-node-5 --net host --privileged=true -v /pan/redis/share/redis-node-5:/data redis:6.2.6 --cluster-enabled yes --appendonly yes --port 6385

docker run -d --name redis-node-6 --net host --privileged=true -v /pan/redis/share/redis-node-6:/data redis:6.2.6 --cluster-enabled yes --appendonly yes --port 6386 

docker run -d --name redis-node-7 --net host --privileged=true -v /pan/redis/share/redis-node-7:/data redis:6.2.6 --cluster-enabled yes --appendonly yes --port 6387
```

docker run 创建并运行容器
-d 守护进程运行
--name redis-node-2 容器名称
--net host 使用宿主机的IP和端口
--privileged=true 获取宿主机的root用户权限
-v /docker/redis/share/redis-node-2:/data redis:6.2.6 挂载容器卷
--cluster-enabled yes 开启redis集群
--appendonly yes 开启持久化
--port 6382 redis端口号

### 2、构建集群关系

进入容器redis-node-2(任意一台都行),为六台容器构建集群关系

```shell
docker exec -it redis-node-2 /bin/bash
```

构建集群

```shell
redis-cli --cluster create 192.168.146.133:6382 192.168.146.133:6383 192.168.146.133:6384 192.168.146.133:6385 192.168.146.133:6386 192.168.146.133:6387 --cluster-replicas 1
```

--cluster-replicas 1 表示集群主节点需要多少个从节点，我们用了6台，即3台服务器构成集群，每台服务器设置1台从服务器

执行上述命令后，redis可自动分配主从机器，对应如下：

![](./img/docker-redis主从配置1.png)

插槽分配：

Master[0] -> Slots 0 - 5460
Master[1] -> Slots 5461 - 10922
Master[2] -> Slots 10923 - 16383

Redis 集群中内置了 16384 个哈希槽，redis 会根据节点数量大致均等的将哈希槽映射到不同的节点。当需要在 Redis 集群中放置一个 key-value时，redis 先对 key 使用 crc16 算法算出一个结果，然后把结果对 16384 求余数，这样每个 key 都会对应一个编号在 0-16383 之间的哈希槽，也就是映射到某个节点上。

主从配置后关系图如下：

![](./img/docker-redis主从配置2.png)

master
redis-node-2:6382
redis-node-3:6383
redis-node-4:6384
slave
redis-node-5:6385
redis-node-6:6386
redis-node-7:6387
对应关系master -> slave
redis-node-2:6382 -> redis-node-6:6386
redis-node-3:6383 -> redis-node-7:6387
redis-node-4:6384 -> redis-node-5:6385

### 3、测试redis集群数据存储

进入进入容器redis-node-2(任意一台都行),为六台容器构建集群关系

链接redis集群

```shell
# -p端口，-c集群模式链接，集群不加-c部分key会因为哈希槽找不到无法存入
redis-cli -p 6382 -c
```

查看集群信息

```shell
cluster info
```

查看节点信息

```shell
cluster nodes
```

存入4个key-value查看结果

```shell
set k1 v1
set k2 v2
set k3 v3
set k4 v4
```

对于（master）redis-node-2:6382 -> （slave）redis-node-6:6386

停掉redis-node-2，redis-node-6将变成master, 4个key-value依然存在

![](./img/docker-redis主从配置3.png)

重启redis-node-2后，redis-node-2将变为redis-node-6的slave即：

（master）redis-node-6:6386 -> （slave）redis-node-2:6382

![](./img/docker-redis主从配置4.png)

再停掉redis-node-6，稍后重新启动即可变成最初的

（master）redis-node-2:6382 -> （slave）redis-node-6:6386

![](./img/docker-redis主从配置5.png)

集群搭建验证完成。

# docker-compose

docker-composea安装教程：[Install the Compose standalone](https://docs.docker.com/compose/install/other/)

设置权限：chmod +x docker-compose

docker-compose部署微服务应用依赖mysql和redis。

### 1、创建微服务

参考链接：

### 2、创建Dockerfile文件

```shell
touch Dockerfile
```

Dockerfile内容：

```dockerfile
# 基础镜像使用java
FROM java:8
# 作者
MAINTAINER panyibao
# VOLUME 指定临时文件目录为/tmp，在主机/var/lib/docker目录下创建了一个临时文件并链接到容器的/tmp
VOLUME /tmp
# 将jar包添加到容器中并更名为gulimall-docker-test.jar
ADD gulimall-docker-test-0.0.1-SNAPSHOT.jar gulimall-docker-test.jar
# 运行jar包
RUN bash -c 'touch /gulimall-docker-test.jar'
ENTRYPOINT ["java","-jar","/gulimall-docker-test.jar"]
#暴露10000端口作为微服务
EXPOSE 10000


# 构建镜像执行 docker build -t gulimall-docker-test:版本号 .
# 如：docker build -t gulimall-docker-test:1.0 .
```

### 3、构建镜像

```shell
docker build -t gulimall-docker-test:1.0 .
```

### 4、创建docker-compose.yml文件

```shell
touch docker-compose.yml
```

**docker-compose.yml内容**

```yaml
version: "3"

services:
  microService:
    image: gulimall-docker-test:1.0
    container_name: gulimall-docker-test
    ports:
      - "10000:10000"
    volumes:
      - /app/microService:/data
    networks:
      - gulimall_net
    depends_on:
      - redis
      - mysql

  redis:
    image: redis:6.0.8
    container_name: gulimall-test-redis
    ports:
      - "6379:6379"
    volumes:
      - /app/redis/redis.conf:/etc/redis/redis.conf
      - /app/redis/data:/data
    networks:
      - gulimall_net
    command: redis-server /etc/redis/redis.conf

  mysql:
    image: mysql:5.7
    container_name: gulimall-test-mysql
    environment:
      MYSQL_ROOT_PASSWORD: '123456'
      MYSQL_ALLOW_EMPTY_PASSWORD: 'no'
      MYSQL_DATABASE: 'docker_test'
      MYSQL_USER: 'panyibao'
      MYSQL_PASSWORD: '123456'
    ports:
      - "3306:3306"
    volumes:
      - /app/mysql/db:/var/lib/mysql
      - /app/mysql/conf/my.cnf:/etc/my.cnf
      - /app/mysql/init:/docker-entrypoint-initdb.d
    networks:
      - gulimall_net
    #解决外部无法访问
    command: --default-authentication-plugin=mysql_native_password

networks:
  gulimall_net:
```

### 5、通过docker-compose启动容器

```shell
##   -f调用文件。-d:开启守护进程
docker-compose -f docker-compose.yml up -d

## 停止docker-compose.yml中的容器
docker-compose -f docker-compose.yml stop
```

### 6、测试验证

浏览器访问微服务swagger页面,进行插入数据接口测试:

![](./img/docker-compose测试1.jpg)

查询数据库，验证数据：

![](./img/docker-compose测试2.jpg)

验证成功。

### 7、docker-compose常用命令

Compose常用命令  
docker-compose -h                           # 查看帮助  
docker-compose up                           # 启动所有docker-compose服务  
docker-compose up -d                        # 启动所有docker-compose服务并后台运行  
docker-compose down                         # 停止并删除容器、网络、卷、镜像。  
docker-compose exec  yml里面的服务id                 # 进入容器实例内部  docker-compose exec docker-compose.yml文件中写的服务id /bin/bash  
docker-compose ps                      # 展示当前docker-compose编排过的运行的所有容器  
docker-compose top                     # 展示当前docker-compose编排过的容器进程  

docker-compose logs  yml里面的服务id     # 查看容器输出日志  
dokcer-compose config     # 检查配置  
dokcer-compose config -q  # 检查配置，有问题才有输出  
docker-compose restart   # 重启服务  
docker-compose start     # 启动服务  
docker-compose stop      # 停止服务

**注意：**
1、docker-compose的命令需要在有docker-compose.yml文件的目录才可以执行，否则使用docker-compose的命令会报错；并且只有该yml文件执行了up操作即创建了该docker-compose.yml定义的容器之后才可以看到对应的容器。
比如执行docker-compose ps查看通过docker-compose运行了哪些容器。
2、docker-compose命令，在哪个目录下执行，那么就是对于该目录下的docker-compose.yml文件的操作，比如docker-compose up就是使用当前目录的docker-compose.yml文件创建启动容器，docker-compose ps那么就是查看当前目录docker-compose.yml文件定义的容器(前提是执行了up操作才能看到定义的这些容器)。
