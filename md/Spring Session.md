# Spring Session自动配置启动

Spring Session 由核心模块和具体存储方式相关联的实现模块构成。核心模块包含了 Spring Session 的基本抽象和 API。Spring Session 有两个核心组件：Session 和 SessionRepository。通过 SessionRepository 来操作 Session。Session 即会话，支持 HttpSession、WebSocket Session，以及其他与 Web 无关的 Session。Session 可以存储与用户相关的信息或者其他信息。Session可以有多种方式存储，例如：Redis，MongoDB，JDBC等，下面主要讲Redis的启动方式。

**spring自动配置模块spring-boot-autoconfigure**
spring-boot-autoconfigure-2.6.9.jar!\META-INF\spring.factories中自动引入了SessionAutoConfiguration，通过配置可实现自动引入对应的存储配置类。

**@EnableRedisHttpSession**

注解开启Spring Session的redis配置

@EnableRedisHttpSession也可以不标注，有spring-boot-autoconfigure自动引入Spring Session也可以生效

引入图示：

<img src="./img/spring-session-redis加载流程1.png" title="" alt="" width="692">

在SessionAutoConfiguration的@Import中通过ServletSessionRepositoryImplementationValidator.class检查了存储配置类是否存在，确保相应的存储库配置类存在于classpath,

```java
@Configuration(proxyBeanMethods = false)
        @ConditionalOnMissingBean(SessionRepository.class)
        @Import({ ServletSessionRepositoryImplementationValidator.class,
                ServletSessionConfigurationImportSelector.class })
        static class ServletSessionRepositoryConfiguration {

        }
```
