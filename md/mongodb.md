# mongodb创建数据库和账号

show dbs 查看所以数据库

use admin  使用admin数据库

1、创建管理账号：
在admin数据库创建
使用命令：db.createUser({user:"panAdmin",pwd:"123456",roles:[{role:"userAdminAnyDatabase",db:"admin"}]})
开启权限认证：
配置文件mongod.cfg配置
security: 
  authorization: enabled
或者服务启动命令添加 --auth
.\mongod --dbpath  "C:\Program Files\MongoDB\Server\7.0\data"  --auth
重启服务

测试验证：
db.auth("panAdmin","123456")
返回1成功

use testdb -- 创建数据库
db.testdb.insertOne({"name":"text"}) -- 插入一条数据才能 show dbs显示

设置用户名，给读写角色
db.createUser({user:'pan',pwd:'123456',roles:[{role:'readWrite',db:'testdb'}]})



连接数据库：

```url
mongodb://pan:123456@localhost:27017/?authMechanism=DEFAULT&authSource=testdb
```

此时pan账号只能操作testdb数据库
