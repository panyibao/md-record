# 1、什么是AOP?

AOP为Aspect Oriented Programming的缩写，意为：面向切面编程。AOP是OOP（面向对象程序设计）的延续。通常在业务主流程中都是自顶向下执行，但是在执行过程中往往还有其他的非主要业务流程的事情需要处理，例如日志记录，权限校验，异常处理等。我们希望可以将它们独立到非指导业务逻辑的方法中，进而改变这些行为的时候不影响业务逻辑的代码。

![](./img/AOP1.png)

AOP的实现有AspectJ、JDK动态代理、CGLIB动态代理，SpringAOP不是一种新的AOP实现，其底层采用的是JDK/CGLIB动态代理。

# 2、Spring AOP

官方文档地址：https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop 

## 2.1、Join point(加入点或者连接点)

spring允许使用Advice（增强逻辑）的地方，基本每个方法的前，后（两者都有也行），或抛出异常时都可以是连接点

![](./img/aop连接点.png)

## 2.2、Pointcut（切入点）

可以看做是链接点的集合，比如一个controller里所有的连接点。

在上面说的连接点的基础上，来定义切入点，一个类里，有15个方法，那就有几十个连接点，但是你并不想在所有方法附近都使用Advice（使用Weaving叫织入），只想让其中的几个，在调用这几个方法之前，之后或者抛出异常时干点什么，那么就用切点来定义这几个方法，让切点来筛选连接点，选中那几个想要的方法。

## 2.3、Advice（处理、增强逻辑）

就是想要的功能，如安全校验，事物，日志等。先定义好，然后在需要的地方通过切面加入。在特定连接点执行逻辑，增强逻辑的实现和执行的位置，位置包括“周围”、“之前”和“之后”，异常处理等。

![](./img/aop-Advice.png)

## 2.4、Aspect(切面)

切面是Advice增强逻辑和Pointcut切入点的结合。连接点是为了更好好理解切点。Advice说明了干什么和什么时候干（什么时候通过方法名中的before,after，around等就能知道），而切入点说明了在哪干（指定到底是哪个方法），这就是一个完整的切面定义，通常这会整理成一个类。

## 2.5、Target object（目标对象）

被增强的对象

![](./img/aop目标对象.png)

## 2.6、AOP代理（目标对象）

由AOP框架创建的对象，以实现一个切面（增强方法执行等）。在 Spring 框架中，AOP 代理 是 JDK 动态代理或 CGLIB 代理

## 2.6、Weaving（织入）

把切面应用到目标对象来创建新的代理对象的过程。这可以在编译时完成（使用 AspectJ 编译器）、加载时间或运行时。Spring AOP像其他纯Java AOP框架一样， 在运行时执行织入。

# 3、spring AOP 简单应用例子

AOP 基本不需要侵入原代码，只需要配置@EnableAspectJAutoProxy  和实现Aspect即可

## 3.1、目标对象

```java
@Slf4j
@RestController
@RequestMapping("member/login")
public class LoginController {

    @GetMapping("/userInfo")
    public R userInfo(String id, String name){
        log.info("userInfo ...");
        return R.ok().put("param", id + "_" + name);
    }

}
```

## 3.2、开启AOP代理

```java
@Configuration
@EnableAspectJAutoProxy
public class AspectConfig {
}
```

## 3.3、实现切面

代码：https://gitee.com/panyibao/gulimall/  子模块为gulimall-member

使用@Aspect注解标识为切面，@Component  加入到spring容器中使其生效。

下面实现了Before类型切点和Around（环绕）类型切点。

```java
// 注解标识切面
@Slf4j
@Component
@Aspect
public class LoginControllerAspect {


    // 切点注解，指定切点为com.pan.gulimall.member.controller.LoginController.userInfo()方法
    @Pointcut("execution(public * com.pan.gulimall.member.controller.LoginController.userInfo(..)))")
    private void userInfoPointcut() {
        // 空方法，仅用于承载@Pointcut，否则无法标识切点
    }

    /**
     * 方法前 Advice 通知（功能增强逻辑）
     * @Before 执行时机是LoginController.login()方法前
     * 参数JoinPoint joinPoint 链接点的信息，可以拿到LoginController.login()的参数
     */
    @Before("com.pan.gulimall.member.aspect.LoginControllerAspect.userInfoPointcut()")
    public void doUserInfoBefore(JoinPoint joinPoint) {
        // 获取参数打印日志
        Object[] args = joinPoint.getArgs();
        log.info("-----------------------LoginController.userInfo() args={}", JSON.toJSONString(args));
    }



    /**
     * 环绕 Advice 通知（功能增强逻辑）,可有返回值
     * @Before 执行时机是LoginController.login()方法前后
     * 参数ProceedingJoinPoint 和 JoinPoint的区别：
     * ProceedingJoinPoint继承了JoinPoint，扩展了proceed方法
     * 使得ProceedingJoinPoint可以执行代理对象的实际方法
     *
     */
    @Around("com.pan.gulimall.member.aspect.LoginControllerAspect.userInfoPointcut()")
    public R doUserInfoAround(ProceedingJoinPoint proceedingJoinPoint ) throws Throwable {
        // 计算LoginController.userInfo的执行时间，添加到返回结果
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        R proceed = (R)proceedingJoinPoint.proceed();
        stopWatch.stop();
        long totalTimeMillis = stopWatch.getTotalTimeMillis();
        R result = Optional.ofNullable(proceed).orElse(R.error());
        result.put("timeMillis", totalTimeMillis);
        log.info("-----------------------LoginController.userInfo() totalTimeMillis={}", totalTimeMillis);
        return result;
    }

}
```

**execution格式**
execution(modifiers-pattern? ret-type-pattern declaring-type-pattern?name-pattern(param-pattern) throws-pattern?)
括号中各个pattern分别表示：
修饰符匹配（modifier-pattern?）
返回值匹配（ret-type-pattern）可以为*表示任何返回值,全路径的类名等
类路径匹配（declaring-type-pattern?）
方法名匹配（name-pattern）可以指定方法名 或者 * 代表所有, set* 代表以set开头的所有方法
参数匹配（(param-pattern)）可以指定具体的参数类型，多个参数间用“,”隔开，各个参数也可以用“*”来表示匹配任意类型的参数，如(String)表示匹配一个String参数的方法；(*,String) 表示匹配有两个参数的方法，第一个参数可以是任意类型，而第二个参数是String类型；可以用(..)表示零个或多个任意参数
异常类型匹配（throws-pattern?）
其中后面跟着“?”的是可选项
