# SpringBoot

# 1、bootstrap.yml配置不生效

spring boot项目中如果需要使用bootstrap.yml,必须引入maven springcloud的上下文坐标，因为bootstrap.yml是针对应用级别的上下文

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-context</artifactId>
    <version>2.2.6.RELEASE</version>
</dependenc
```

# 2、约定优于配置

首先，约定优于配置是一种软件设计的范式，它的核心思想是减少软件开发人员
对于配置项的维护，从而让开发人员更加聚焦在业务逻辑上。
Spring Boot 就是约定优于配置这一理念下的产物，它类似于 Spring 框架下的一
个脚手架，通过 Spring Boot，我们可以快速开发基于 Spring 生态下的应用程序。
基于传统的 Spring 框架开发 web 应用，我们需要做很多和业务开发无关并且只
需要做一次的配置，比如
管理 jar 包依赖
web.xml 维护
Dispatch-Servlet.xml 配置项维护
应用部署到 Web 容器
第三方组件集成到 Spring IOC 容器中的配置项维护
而在 Spring Boot 中，我们不需要再去做这些繁琐的配置，Spring Boot 已经自
动帮我们完成了，这就是约定由于配置思想的体现。
**Spring Boot 约定由于配置的体现**有很多，比如
**Spring Boot Starter 启动依赖**，它能帮我们管理所有 jar 包版本
如果当前应用依赖了spring mvc相关的jar，那么Spring Boot会自动内置Tomcat
容器来运行 web 应用，我们不需要再去单独做应用部署。
Spring Boot 的自动装配机制的实现中，通过扫描约定路径下的 spring.factories
文件来识别配置类，实现 Bean 的自动装配。
默认加载的配置文件 application.properties 等等。
总的来说，约定优于配置是一个比较常见的软件设计思想，它的核心本质都是为
了更高效以及更便捷的实现软件系统的开发和维护

# 3、自动装配

## 3.1、什么是自动装配

在使用SpringBoot的时候，会自动将Bean装配到IoC容器中。例如我们在使用Redis数据库的时候，会引入依赖spring-boot-starter-data-redis。在引入这个依赖后，服务初始化的时候，会将操作Redis需要的组件注入到IoC容器中进行后续使用

自动装配大致过程如下：

获取到组件（例如spring-boot-starter-data-redis）META-INF文件夹下的spring.factories文件

spring.factories文件中列出需要注入IoC容器的类

将实体类注入到IoC容器中进行使用。

## 3.2、自动装配原理

自动装配大致流程是通过@SpringBootApplication进行实现，这个注解声明在SpringBoot的启动类上 

1）通过注解@SpringBootApplication=>@EnableAutoConfiguration=>@Import({AutoConfigurationImportSelector.class})实现自动装配

2）AutoConfigurationImportSelector类中重写了ImportSelector中selectImports方法，批量返回需要装配的配置类

3）通过Spring提供的SpringFactoriesLoader机制，扫描classpath下的META-INF/spring.factories文件，读取需要自动装配的配置类

4）依据条件筛选的方式，把不符合的配置类移除掉，最终完成自动装配

# 4、跨域问题

出于浏览器的同源策略限制。同源策略（Sameoriginpolicy）是一种约定，它是浏览器最核心也最基本的安全功能，如果缺少了同源策略，则浏览器的正常功能可能都会受到影响。可以说Web是构建在同源策略基础之上的，浏览器只是针对同源策略的一种实现。

同源策略会阻止一个域的javascript脚本和另外一个域的内容进行交互。所谓同源（即指在同一个域）就是两个页面具有相同的协议（protocol），主机（host）和端口号（port）

当一个请求url的协议、域名、端口三者之间任意一个与当前页面url不同即为跨域

java 后端 实现 CORS 跨域请求的方式：

**1.返回新的 CorsFilter(全局跨域)**
在任意配置类，返回一个 新的 CorsFIlter Bean ，并添加映射路径和具体的CORS配置路径。

**2.重写 WebMvcConfigurer(全局跨域)**

**3.使用注解 (局部跨域)**
在控制器(类上)上使用注解 **@CrossOrigin**:，表示该类的所有方法允许跨域。

**4.手动设置响应头(局部跨域)**
使用 HttpServletResponse 对象添加响应头(Access-Control-Allow-Origin)来授权原始域，这里 Origin的值也可以设置为 “*”,表示全部放行。

**5.使用自定义filter实现跨域**
自定义filter实现跨域，在web.xml中配置这个过滤器，使其生效
