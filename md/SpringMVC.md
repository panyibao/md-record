# SpringMVC

SpringMVC 是一款基于 Spring 框架的 MVC 框架，它采用了 各种设计模式，包括 FrontController、Dispatcher、ViewHelper 等。同时，SpringMVC 与 Spring 框架集成紧密，可以很方便地与其他 Spring 组件（如 Spring Security）进行整合。

# 1、SpringMVC 执行流程

**1.发送请求**

DispatcherServlet 前端控制器是 SpringMVC 的核心组件之一，它接收客户端的请求并将请求分派到对应的处理器（Handler）进行处理。在接收到请求后，DispatcherServlet 会将请求 URL 传递给 HandlerMapping 映射器进行处理，并根据请求 URL 匹配对应的 HandlerExecutionChain 处理器执行链。HandlerExecutionChain 包含了一个或多个 Interceptor 拦截器和一个 Handler 处理器，拦截器可以在请求处理前后进行一些额外的处理，而 Handler 则负责具体的业务处理。

**2.处理器执行**
DispatcherServlet前端控制器请求HandlerAdapter适配器执行Handler处理器，HandlerAdapter 适配器是 SpringMVC 中用于执行处理器（Handler）的重要组件。在 HandlerMapper 映射器确定了需要执行的 Handler 处理器之后，HandlerAdapter 就负责根据具体的 Handler 处理器类型，调用相应的方法进行处理。不同的 Handler 处理器类型通常需要不同的处理方式，因此需要不同的 HandlerAdapter 适配器实现来进行适配。

**3.返回处理结果**
在 HandlerAdapter 适配器执行完 Handler 处理器后，会获得一个 ModelAndView 对象，它包含了处理结果视图的名称和需要在视图中使用的模型数据。这个 ModelAndView 对象将被返回给 DispatcherServlet 前端控制器，以便进行下一步的处理。

**4.处理视图**
DispatcherServlet请求ViewResolver对ModelAndView进行视图解析，ViewResolver 视图解析器是 SpringMVC 中用于将 ModelAndView 解析为具体视图对象（View）的组件。视图解析器可以根据指定的视图名，通过配置的 ViewResolver 实现类查找对应的视图对象。常用的 ViewResolver 实现类有 InternalResourceViewResolver 等，它们都可以将 ModelAndView 中指定的视图名解析为 JSP 视图对象。

**5.处理结果**
在经过 ViewResolver 视图解析器的处理后，ModelAndView 对象将被解析为对应的 View 视图对象，即用于渲染视图的具体组件。View 组件可以是 JSP 视图、FreeMarker 模板、JSON 数据等不同形式的响应内容。

**6.返回响应**
最后一步是前端控制器 DispatcherServlet 对视图对象进行渲染。具体的渲染方式会根据 View 类型的不同而有所差异，例如 JSP 视图的渲染方式需要使用 Servlet API 和 JSP 引擎。渲染完成后，前端控制器将生成的响应数据返回给客户端浏览器进行展示。

详细流程图如下：

![](./img/SpringMVC执行流程图.jpg)
