# 1、安装

下载路径：

官网：[MySQL :: Download MySQL Community Server](https://dev.mysql.com/downloads/mysql/)

阿里镜像（推荐，下载速度快）：[mysql安装包下载_开源镜像站-阿里云](https://mirrors.aliyun.com/mysql/?spm=a2c6h.13651104.0.0.41bd5dc8I2TZu9)

**删除已存在的就服务**

```shell
## win删除服务命令：SC delete 服务名
SC delete MySQL57
## 或者使用mysql bin目录下的mysql删除
mysqld --remove MySQL57
```

安装以mysql-8.0.27-winx64为例：

解压后在./mysql-8.0.27-winx64下添加my.ini文件

编辑配置：

```ini
[mysqld]
# 设置3306端口
port=3306
# 设置mysql的安装目录   
basedir="E:\\mysql\\mysql-8.0.27-winx64
# 设置mysql数据库的数据的存放目录  
datadir="E:\\mysql\\mysql-8.0.27-winx64\\data"
# 允许最大连接数
max_connections=200
# 允许连接失败的次数。
max_connect_errors=10
# 服务端使用的字符集默认为utf8mb4
character-set-server=utf8mb4
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
# 默认使用“mysql_native_password”插件认证
#mysql_native_password
default_authentication_plugin=mysql_native_password
[mysql]
# 设置mysql客户端默认字符集
default-character-set=utf8mb4
[client]
# 设置mysql客户端连接服务端时默认使用的端口
port=3306
default-character-set=utf8mb4
```

创建环境变量：

新建参数名:MYSQL_HOME，
路径为：解压路径E:\mysql\mysql-8.0.27-winx64

添加path变量：%MYSQL_HOME%\bin

cmd在**管理员模式**下进入bin文件夹：

执行初始化命令：

```shell
mysqld --initialize --console
```

初始化后会生成一个默认密码：

执行安装命令：

```shell
mysqld --install MySQL
```

启动服务：

```shell
## 如果MySQL服务名不对，可查看服务列表看安装的服务名是什么
net start MySQL
```

修改密码：

```shell
## 链接到mysql,回车后要求输入密码，将上面初始化生成的密码输入即可
mysql -u root -p

## 执行修改密码命令
alter user 'root'@'localhost' identified by '123456';

## 退出mysql命令行
exit;
```

安装完成。

查看mysql字符集命令：

```sql
show variables where variable_name LIKE 'character\_set\_%' OR variable_name LIKE 'collation%';
```

# 2、mysql事务隔离级别

| 事务隔离级别                 | 脏读  | 不可重复读 | 幻读          |
| ---------------------- | --- | ----- | ----------- |
| Read uncommitted(读未提交) | 是   | 是     | 是           |
| Read committed(读提交)    | 否   | 是     | 是           |
| Repeatable read(可重复读取) | 否   | 否     | 是（innoDB不会） |
| Serializable(可序化)      | 否   | 否     | 否           |

这四种隔离级别，当存在多个事务并发冲突的时候,可能会出现脏读，不可重复读，幻读的一些问题，而innoDB在可重复读隔离级别模式下解决了幻读的一个问题，

**幻读：**

幻读是指在同一个事务中，前后两次查询相同范围的时候得到的结果不一致

![](./img/数据库幻读.jpg)

如图，第一个事务里面,我们执行一个范围查询，这个时候满足条件的数据只有一条，而在第二个事务里面，它插入一行数据并且进行了提交，接着第一个事务再去查询的时候，得到的结果比第一次查询的结果多出来一条数据，注意第一个事务的第一次和第二次查询,都在同一个事物里面，所以，幻读会带来数据一致性的问题

**InnoDB解决幻读：**

InnoDB引入**间隙锁**和 **next-key lock** 机制去解决幻读问题

假如现在存在这样一个B+Tree的索引结构，这个结构有4个索引元素，分别是1,4,7,10  
当我们通过主键索引查询一条记录,并且对这条记录通过`for update`加锁的时候

![](./img/数据库幻读示例1.jpg)

![](./img/数据库幻读示例2.jpg)

此时另外一个事务再执行

![](./img/数据库幻读示例3.jpg)

insert语句需要被阻塞,直到前面获得所的事务被释放，所以在innonDB设计一种间隙锁，它的主要功能是锁定一定范围内的索引记录

间隙锁图示：

![](./img/数据库幻读示例4-间隙锁.jpg)

next-key lock图示：

![](./img/数据库幻读示例5-next-key%20Lock.jpg)

每个数据行非唯一的索引列，都会存在一把next-key lock,当某个事务持有这一行数据的next-key lock的时候,会锁住一段在左开右闭区间的数据，因此当通过id > 4这样一个范围加锁的时候，InnoDB会去加一个next-key lock锁，锁定的区间范围是（4，7 ]（7，10 ]（10，+∞ ]。间隙锁和next-key lock的区别是在加锁的范围，间隙锁锁定的是两个索引之间的间隙,而next-key lock会锁定多个索引区间，它包含记录锁和间隙锁当我们使用范围查询不仅仅命中Record记录，还包含了Gap间隙的时候，在这种情况下使用的就是临键锁，也就是next-key lock它是Mysql里面默认的行锁算法

**注意：**

InnoDB里面通过间隙锁方式解决了幻读的问题但是加锁之后一定会影响到并发性能，对与性能较高的一些业务场景，可以把隔离级别设置不可重复，这个级别不存在间隙锁也就不存在这样的一个性能的影响
