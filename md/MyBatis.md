# 1、MyBatis缓存

基本概念：MyBatis包含一个非常强大的查询缓存特性，它可以非常方便地配置和定制。缓存可以极大的提升查询效率。
MyBatis系统中默认定义了两级缓存，分别是一级缓存和二级缓存。
基本介绍：
1、默认情况下，只有一级缓存（SqlSession级别的缓存，也称为本地缓存）开启。
2、二级缓存需要手动开启和配置，他是基于namespace级别的缓存。
3、为了提高扩展性，MyBatis定义了缓存接口Cache，我们可以通过实现Cache接口来自定义二级缓存。

## 1.1、一级缓存

**一级缓存（本地缓存）**
sqlSession级别的缓存，一级缓存一直是开启的，它实质上就是sqlSession级别的一个**Map。**

**作用**
与数据库同一次会话期间查询到的数据会放在本地缓存中，以后如果需要获取相同的数据，直接从缓存中拿，没必要再去查询数据库。

**一级缓存失效的情况**（没有使用到当前一级缓存的情况，效果就是还要再向数据库发出查询）
1)、sqlSession不同
2)、sqlSession相同，查询条件不同（当前一级缓存中还没有这个数据）。
3)、sqlSession相同，两次查询之间执行了增删改操作（这次增删改可能对当前数据有影响），实际上，这个是因为每个增删改查都有标签flushCache，增删改默认为flushCache=“true”，即执行完后就清除一级缓存和二级缓存。
4)、sqlSession相同，手动清除了一级缓存（缓存清空，session.clearCache()，注意，该方法只清除当前session的一级缓存）。

## 1.2、二级缓存

二级缓存（全局缓存）namespace级别的缓存，一个namespace对应一个二级缓存。 

**工作机制**

一个会话查询一条数据，这个数据就会被放在当前会话的一级缓存中。 如果会话关闭或提交，一级缓存中的数据会被保存到二级缓存中，新的会话查询信息，就可以参照二级缓存中的内容。 注意：不同的namespace查出的数据会放在自己对应的缓存（map）中。 效果：数据会从二级缓存中取出。查出的数据都会被默认先放在一级缓存中，只有会话提交或者关闭以后，一级缓存中的数据才会转移到二级缓存中。

**二级缓存使用流程**
开启全局二级缓存配置<setting name="cacheEnabled" value="true"/>。默认为true
去mapper.xml中配置使用二级缓存<cache></cache>,使用二级缓存。
我们的POJO需要实现序列化接口。

**缓存的相关配置**
1、cacaheEnabled=true/false：二级缓存的开启和关闭，一级缓存一直可用。

2、每个select标签都有属性useCache，默认为true。true：使用缓存；false：不使用缓存。注意：空控制的是二级缓存的使用与否，一级缓存一直可用。

3、每个增删改查都有标签flushCache。增删改默认为flushCache=“true”，即执行完后就清除一级缓存和二级缓存；查询默认为flushCache=“false”，即执行完后不清除一级和二级缓存。

4、sqlSession.clearCache()：只清除当前session的一级缓存。

5、localCacheScope（MyBatis3.3后新增）：本地缓存作用域，控制一级缓存，默认为SESSION。SESSION：当前会话的所有数据保存在会话缓存中；STATEMENT：可以禁用一级缓存。

一级缓存无过期时间，只有生命周期，缓存会先放在一级缓存中，当sqlSession会话提交或者关闭时才会将一级缓存刷新到二级缓存中；
二级缓存的过期时间默认是1小时，如果这个cache存活了一个小时，那么将整个清空一下。需要注意的是，并不是key-value的过期时间，而是这个cache的过期时间，是flushInterval，意味着整个清空缓存cache，所以不需要后台线程去定时检测，每当存取数据的时候，都有检测一下cache的生命时间。

# 2、MyBatis配置读取

```java
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
```

初始化SqlSessionFactory实例时会解析配置文件（XMLConfigBuilder类），并将配置文件保存到org.apache.ibatis.session.Configuration，包含环境配置，mapper.xml，Properties等配置信息。mapper.xml中的sql会被解析到Configuration的mappedStatements属性中。

# 3、Mybatis插件

Mybatis中的插件虽然名称叫插件，但实质上是通过动态代理实现的。和我们平时讲的插件概念不一样，但是本质上都是给外部提供接口进行扩展。

MyBatis 允许我们在映射语句执行过程中的某一点进行拦截调用。MyBatis允许我们使用插件来拦截的方法调用包括：

**Executor** (update, query, flushStatements, commit, rollback, getTransaction, close, isClosed)
**ParameterHandler** (getParameterObject, setParameters)
**ResultSetHandler** (handleResultSets, handleOutputParameters)
**StatementHandler** (prepare, parameterize, batch, update, query)

## 3.1、Executor

**Executor**：SqlSession使用执行器来执行SQL语句，其中mybatis中一共有3个执行器:

**SimpleExecutor**：每次执行一次update或者select，就会开启一个Statement对象，用完立刻关闭Statement对象。

**ReuseExecutor**: 可复用执行器，执行update或者select，会从一个Map<String, Statement>中以sql语句作为key来查找Statement对象，如果存在就使用，不存在就创建，用完后不关闭Statement对象，而是放入Map中。需要维护，防止占用内存过多。

**BatchExecutor**：执行update时，会将所有的sql和Statement都添加到一个批处理的集合中，等待统一执行，与JDBC批处理类似。

扩展Executor（自定义Interceptor插件），可以增强逻辑，如分页

**自定义插件** 在Mybatis中自定义插件只需要以下几步：

1、创建自定义插件类实现Interceptor接口；
2、使用@Intercepts注解：指定插件拦截四大对象中指定对象的指定方法；
3、在全局配置文件中注册插件

# 4、配置读取和初始化

## 4.1、普通mybatis启动配置读取和初始化

构建 SqlSessionFactory从类路径或其它位置中载加 XML资源文件

```java
String resource = "org/mybatis/example/mybatis-config.xml";
InputStream inputStream = Resources.getResourceAsStream(resource);
// 构建 SqlSessionFactory 
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
```

SqlSessionFactoryBuilder().build(inputStream)构建 SqlSessionFactory过程中会解析xml配置文件，跟进代码最终执行解析的方法是：

org. apache. ibatis. builder. xml. XMLConfigBuilder#parseConfiguration

![](./img/mybatisXML解析代码.jpg)

下面看下几个重要的步骤，以下面的xml配置示例：

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
  "https://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
  <properties resource="org/mybatis/example/config.properties">
    <!-- ... -->
    <property name="org.apache.ibatis.parsing.PropertyParser.enable-default-value" value="true"/> <!-- 启用默认值特性 -->
  </properties>
  <environments default="development">
    <environment id="development">
      <transactionManager type="JDBC"/>
      <dataSource type="POOLED">
        <property name="driver" value="${driver}"/>
        <property name="url" value="${url}"/>
        <property name="username" value="${username}"/>
        <property name="password" value="${password}"/>
      </dataSource>
    </environment>
  </environments>
  <mappers>
    <mapper resource="org/mybatis/example/BlogMapper.xml"/>
  </mappers>
</configuration>
```

mapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "https://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="org.mybatis.example.BlogMapper">
  <select id="selectBlog" resultType="Blog">
    select * from Blog where id = #{id}
  </select>
</mapper>
```

如图代码解析：

```java
private void parseConfiguration(XNode root) {
    try {
      // 解析properties节点，读取文件路径并读取properties文件，获取到对应的properties配置信息
      // 这里主要是数据源的配置信息
      propertiesElement(root.evalNode("properties"));
      Properties settings = settingsAsProperties(root.evalNode("settings"));
      // ....
      // 解析environments节点获取到dataSource配置
      // 可以看到xml配置里有用到${url}这些占位符，这些配置来自前面的properties文件配置
      // 所以这里需要先解析properties再到environments
      environmentsElement(root.evalNode("environments"));
      // 解析mappers配置，读取并解析对应的mapper.xml获取到配置的sql以便后期执行
      mapperElement(root.evalNode("mappers"));
    } catch (Exception e) {
      throw new BuilderException("Error parsing SQL Mapper Configuration. Cause: " + e, e);
    }
  }
```

对于mappers的配置方式有4种：

```xml
<!-- 1、将包内的映射器接口全部注册为映射器，代码中最先判断 -->
<mappers>
  <package name="org.mybatis.builder"/>
</mappers>
<!-- 2、使用相对于类路径的资源引用 -->
<mappers>
  <mapper resource="org/mybatis/builder/AuthorMapper.xml"/>
  <mapper resource="org/mybatis/builder/BlogMapper.xml"/>
  <mapper resource="org/mybatis/builder/PostMapper.xml"/>
</mappers>
<!-- 3、使用完全限定资源定位符（URL） -->
<mappers>
  <mapper url="file:///var/mappers/AuthorMapper.xml"/>
  <mapper url="file:///var/mappers/BlogMapper.xml"/>
  <mapper url="file:///var/mappers/PostMapper.xml"/>
</mappers>
<!-- 4、使用映射器接口实现类的完全限定类名 -->
<mappers>
  <mapper class="org.mybatis.builder.AuthorMapper"/>
  <mapper class="org.mybatis.builder.BlogMapper"/>
  <mapper class="org.mybatis.builder.PostMapper"/>
</mappers>
```

## 4.2、mybatis-plus整合springboot启动读取配置

**mybatis-plus-boot-starter启动：**

mybatis-plus-boot-starter的jar包中spring.factories配置了

```properties
org.springframework.boot.env.EnvironmentPostProcessor=\
  com.baomidou.mybatisplus.autoconfigure.SafetyEncryptProcessor
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  com.baomidou.mybatisplus.autoconfigure.IdentifierGeneratorAutoConfiguration,\
  com.baomidou.mybatisplus.autoconfigure.MybatisPlusLanguageDriverAutoConfiguration,\
  com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration
```

其中MybatisPlusAutoConfiguration有@Configuration注解，内部通过@Bean方式SqlSessionFactory

```java
    @Bean
    @ConditionalOnMissingBean
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
        // TODO 使用 MybatisSqlSessionFactoryBean 而不是 SqlSessionFactoryBean
        MybatisSqlSessionFactoryBean factory = new MybatisSqlSessionFactoryBean();
        factory.setDataSource(dataSource);
        factory.setVfs(SpringBootVFS.class);
        // 读取spring配置文件中的mybatis-plus全局配置文件
        // mybatis-plus.config-location = classpath:xxx.xml
        if (StringUtils.hasText(this.properties.getConfigLocation())) {
            factory.setConfigLocation(this.resourceLoader.getResource(this.properties.getConfigLocation()));
        }
        applyConfiguration(factory);
        if (this.properties.getConfigurationProperties() != null) {
            factory.setConfigurationProperties(this.properties.getConfigurationProperties());
        }
        // ....
        // 省略部分设置代码...
        // TODO 此处必为非 NULL
        // 获取mapper.xml文件Resource，为后续解析做准备
        Resource[] mapperLocations = this.properties.resolveMapperLocations();
        if (!ObjectUtils.isEmpty(mapperLocations)) {
            factory.setMapperLocations(mapperLocations);
        }
        // ....
        // 省略部分设置代码...
        // TODO 此处必为非 NULL
        GlobalConfig globalConfig = this.properties.getGlobalConfig();
        // TODO 注入填充器
        this.getBeanThen(MetaObjectHandler.class, globalConfig::setMetaObjectHandler);
        // TODO 注入主键生成器
        this.getBeansThen(IKeyGenerator.class, i -> globalConfig.getDbConfig().setKeyGenerators(i));
        // TODO 注入sql注入器
        this.getBeanThen(ISqlInjector.class, globalConfig::setSqlInjector);
        // TODO 注入ID生成器
        this.getBeanThen(IdentifierGenerator.class, globalConfig::setIdentifierGenerator);
        // TODO 设置 GlobalConfig 到 MybatisSqlSessionFactoryBean
        factory.setGlobalConfig(globalConfig);
        // 创建SqlSessionFactory，解析并读取对应的xml配置
        return factory.getObject();
    }
```

最后的代码factory.getObject();执行了SqlSessionFactory的创建：

跟进代码最终到

com. baomidou. mybatisplus. extension. spring. MybatisSqlSessionFactoryBean#buildSqlSessionFactory

buildSqlSessionFactory方法前面设置环境和默认值，然后再下面的代码中解析mybatis-config.xml和mapper.xml

如果设置了mybatis全局配置文件（mybatis-config.xml）会执行以下代码

```java
        if (xmlConfigBuilder != null) {
            try {
                xmlConfigBuilder.parse();
                LOGGER.debug(() -> "Parsed configuration file: '" + this.configLocation + "'");
            } catch (Exception ex) {
                throw new NestedIOException("Failed to parse config resource: " + this.configLocation, ex);
            } finally {
                ErrorContext.instance().reset();
            }
        }
```

xmlConfigBuilder.parse最终会执行到

com. baomidou. mybatisplus. core. MybatisXMLConfigBuilder#parseConfiguration

从备注中可以知道MybatisXMLConfigBuilder是从mybatis的XMLConfigBuilder 拷贝过来的

具体的方法parseConfiguration流程与原来的一样，也就是mybatis-config.xml解析过程和4.1相同。

然后执行以下代码解析mybatis-plus.mapper-locations=classpath*:/mapper/**/*.xml配置的mapper.xml

```java
        if (this.mapperLocations != null) {
            if (this.mapperLocations.length == 0) {
                LOGGER.warn(() -> "Property 'mapperLocations' was specified but matching resources are not found.");
            } else {
                for (Resource mapperLocation : this.mapperLocations) {
                    if (mapperLocation == null) {
                        continue;
                    }
                    try {
                        // 解析mapper，mapperLocation在前面MybatisPlusAutoConfiguration中已经设置
                        XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(mapperLocation.getInputStream(),
                            targetConfiguration, mapperLocation.toString(), targetConfiguration.getSqlFragments());
                        xmlMapperBuilder.parse();
                    } catch (Exception e) {
                        throw new NestedIOException("Failed to parse mapping resource: '" + mapperLocation + "'", e);
                    } finally {
                        ErrorContext.instance().reset();
                    }
                    LOGGER.debug(() -> "Parsed mapper file: '" + mapperLocation + "'");
                }
            }
        } else {
            LOGGER.debug(() -> "Property 'mapperLocations' was not specified.");
        }
        final SqlSessionFactory sqlSessionFactory = new MybatisSqlSessionFactoryBuilder().build(targetConfiguration);
        // ...
        // 后续处理代码省略
```

new MybatisSqlSessionFactoryBuilder().build(targetConfiguration);根据上面代码解析出的配置创建SqlSessionFactory并最终返回出去。

# 5、mybatis-plus整合springboot生成mapper接口代理对象到容器

自动配置类MybatisPlusAutoConfiguration在创建SqlSessionFactory的时候会解析mapper.xml文件（如上4.2）。其中解析方法为

**xmlMapperBuilder.parse():**

```java
  public void parse() {
    if (!configuration.isResourceLoaded(resource)) {
      configurationElement(parser.evalNode("/mapper"));
      configuration.addLoadedResource(resource);
      // 根据namespace绑定对应的mapper接口，内部会生成mapper接口的代理对象
      bindMapperForNamespace();
    }

    parsePendingResultMaps();
    parsePendingCacheRefs();
    parsePendingStatements();
  }
```

**bindMapperForNamespace():**

```java
  private void bindMapperForNamespace() {
    String namespace = builderAssistant.getCurrentNamespace();
    if (namespace != null) {
      Class<?> boundType = null;
      try {
        boundType = Resources.classForName(namespace);
      } catch (ClassNotFoundException e) {
        // ignore, bound type is not required
      }
      if (boundType != null && !configuration.hasMapper(boundType)) {
        // Spring may not know the real resource name so we set a flag
        // to prevent loading again this resource from the mapper interface
        // look at MapperAnnotationBuilder#loadXmlResource
        configuration.addLoadedResource("namespace:" + namespace);
        // 根据类型生成代理对象添加到configuration
        configuration.addMapper(boundType);
      }
    }
  }
```

跟踪方法configuration.addMapper(boundType);

最终进入到MybatisMapperRegistry#addMapper

```java
@Override
    public <T> void addMapper(Class<T> type) {
        if (type.isInterface()) {
            if (hasMapper(type)) {
                // TODO 如果之前注入 直接返回
                return;
                // TODO 这里就不抛异常了
//                throw new BindingException("Type " + type + " is already known to the MapperRegistry.");
            }
            boolean loadCompleted = false;
            try {
                // 生成mapper接口的代理工厂对象，添加到knownMappers中
                // TODO 这里也换成 MybatisMapperProxyFactory 而不是 MapperProxyFactory
                knownMappers.put(type, new MybatisMapperProxyFactory<>(type));
                // It's important that the type is added before the parser is run
                // otherwise the binding may automatically be attempted by the
                // mapper parser. If the type is already known, it won't try.
                // TODO 这里也换成 MybatisMapperAnnotationBuilder 而不是 MapperAnnotationBuilder
                MybatisMapperAnnotationBuilder parser = new MybatisMapperAnnotationBuilder(config, type);
                parser.parse();
                loadCompleted = true;
            } finally {
                if (!loadCompleted) {
                    knownMappers.remove(type);
                }
            }
        }
    }
```

上面是mapper.xml的解析和创建mapper代理工厂的过程，下面需要将mapper的代理对象创建出来并放到spring容器中。

MybatisPlusAutoConfiguration中有一个内部类：

```java
public static class AutoConfiguredMapperScannerRegistrar implements BeanFactoryAware, ImportBeanDefinitionRegistrar
```

AutoConfiguredMapperScannerRegistrar实现了ImportBeanDefinitionRegistrar接口。ImportBeanDefinitionRegistrar接口是也是spring的扩展点之一,它可以支持我们自己写的代码封装成BeanDefinition对象;实现此接口的类会回调postProcessBeanDefinitionRegistry方法，注册到spring容器中。

接口方法registerBeanDefinitions中使用BeanDefinitionBuilder来构造MapperScannerConfigurer的BeanDefinition对象。MapperScannerConfigurer实现了BeanDefinitionRegistryPostProcessor接口，BeanDefinitionRegistryPostProcessor接口是Spring中的一个扩展点，是的作用就是往Spring中注入BeanDefinition的，在Spring的refresh方法中执行调用bean工厂的后置处理器时进行执行。

跟进代码可以看到postProcessBeanDefinitionRegistry方法中创建了ClassPathMapperScanner的扫描器扫描mapper接口并设置了MapperFactoryBeanClass，这是MapperFactoryBean的子类型。

```java
scanner.setMapperFactoryBeanClass(this.mapperFactoryBeanClass);
```

在ClassPathMapperScanner#processBeanDefinitions的方法中会将扫描到的mapper接口的BeanDefinition对象的BeanClass设置成MapperFactoryBean；

```java
definition.setBeanClass(this.mapperFactoryBeanClass);
```

spring创建mapper接口的bean时会调用MapperFactoryBean的getObject()方法进行创建。

跟进getObject()方法，进入到MybatisMapperRegistry#getMappe方法

```java
    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
        // TODO 这里换成 MybatisMapperProxyFactory 而不是 MapperProxyFactory
        final MybatisMapperProxyFactory<T> mapperProxyFactory = (MybatisMapperProxyFactory<T>) knownMappers.get(type);
        if (mapperProxyFactory == null) {
            throw new BindingException("Type " + type + " is not known to the MybatisPlusMapperRegistry.");
        }
        try {
            // 通过mapper的代理工厂对象获取mapper接口的代理对象
            return mapperProxyFactory.newInstance(sqlSession);
        } catch (Exception e) {
            throw new BindingException("Error getting mapper instance. Cause: " + e, e);
       }
    }
```

到这里完成了mapper.xml和mapper接口的关联，并生成了bean对象到容器中。

**总结：spring管理mybatis-plus mapper的简单流程描述：**

1、mapper接口核心实现是JDK动态代理。

2、spring默认排除接口，接口无法注入到容器中

3、需要实现BeanDefinitionRegistryPostProcessor可以动态注册BeanDefinition（MapperScannerConfigurer实现）

4、自定义ClassPathBeanDefinitionScanner重写排除接口的方法（ClassPathMapperScanner#isCandidateComponent）

5、mapper接口无法实例化，将mapper接口的BeanDefinition对象的BeanClass设置成MapperFactoryBean（由动态代理实现）

6、MapperFactoryBean的getObject()方法创建动态代理（工厂方法设计模式）
