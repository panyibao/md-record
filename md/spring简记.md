# spring-framework

## 1、@PostConstruct相关

方法注解，创建对象后执行初始化时执行，与实现InitializingBean接口的afterPropertiesSet方法差不多，但两者存在执行先后问题.

在AbstractAutowireCapableBeanFactory#initializeBean(...)方法中

```java
    protected Object initializeBean(String beanName, Object bean, @Nullable RootBeanDefinition mbd) {
        if (System.getSecurityManager() != null) {
            AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                invokeAwareMethods(beanName, bean);
                return null;
            }, getAccessControlContext());
        }
        else {
            invokeAwareMethods(beanName, bean);
        }

        Object wrappedBean = bean;
        if (mbd == null || !mbd.isSynthetic()) {
            // 这里会执行@PostConstruct标记的初始化方法
            wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);
        }

        try {
            // 这里会执行实现了InitializingBean接口的afterPropertiesSet方法
            invokeInitMethods(beanName, wrappedBean, mbd);
        }
        catch (Throwable ex) {
            throw new BeanCreationException(
                    (mbd != null ? mbd.getResourceDescription() : null),
                    beanName, "Invocation of init method failed", ex);
        }
        if (mbd == null || !mbd.isSynthetic()) {
            wrappedBean = applyBeanPostProcessorsAfterInitialization(wrappedBean, beanName);
        }

        return wrappedBean;
    }
```

## 2、@RestControllerAdvice相关

1、通过@ControllerAdvice注解可以将对于控制器的全局配置放在同一个位置。
2、注解了@RestControllerAdvice的类的方法可以使用@ExceptionHandler、@InitBinder、@ModelAttribute注解到方法上。
3、@RestControllerAdvice注解将作用在所有注解了@RequestMapping的控制器的方法上。
4、@ExceptionHandler：用于指定异常处理方法。当与@RestControllerAdvice配合使用时，用于全局处理控制器里的异常。
5、@InitBinder：用来设置WebDataBinder，用于自动绑定前台请求参数到Model中。
6、@ModelAttribute：本来作用是绑定键值对到Model中，当与@ControllerAdvice配合使用时，可以让全局的@RequestMapping都能获得在此处设置的键值对

## 3、@Conditional相关

@Conditional是Spring4新提供的注解，它的作用是根据某个条件创建特定的Bean，通过实现Condition接口，并重写matches接口来构造判断条件。
总的来说，就是根据特定条件来控制Bean的创建行为，这样可以利用这个特性进行一些自动的配置。

@Conditional(ServletSessionCondition.class)

@ConditionalOnBean（仅仅在当前上下文中存在某个对象时，才会实例化一个Bean）  
@ConditionalOnClass（某个class位于类路径上，才会实例化一个Bean）  
@ConditionalOnExpression（当表达式为true的时候，才会实例化一个Bean）  
@ConditionalOnMissingBean（仅仅在当前上下文中不存在某个对象时，才会实例化一个Bean）  
@ConditionalOnMissingClass（某个class类路径上不存在的时候，才会实例化一个Bean）  
@ConditionalOnNotWebApplication（不是web应用）

## 4、HandlerInterceptor与WebRequestInterceptor的异同

**通常使用HandlerInterceptor即可**

两个接口都可用于Contrller层请求拦截，接口中定义的方法作用也是一样的。

```java
//HandlerInterceptor
boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)throws Exception;
void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)throws Exception;
void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)throws Exception;

//WebRequestInterceptor
void preHandle(WebRequest request) throws Exception;
void postHandle(WebRequest request, ModelMap model) throws Exception;
void afterCompletion(WebRequest request, Exception ex) throws Exception;
```

WebRequestInterceptor间接实现了HandlerInterceptor，他们之间使用WebRequestHandlerInterceptorAdapter适配器类联系。

不同点
1.WebRequestInterceptor的入参WebRequest是包装了HttpServletRequest 和HttpServletResponse的，通过WebRequest获取Request中的信息更简便。
2.WebRequestInterceptor的preHandle是没有返回值的，说明该方法中的逻辑并不影响后续的方法执行，所以这个接口实现就是为了获取Request中的信息，或者预设一些参数供后续流程使用。
3.HandlerInterceptor的功能更强大也更基础，可以在preHandle方法中就直接拒绝请求进入controller方法。

## 5、@Transactional注解

### 1、声明式事务

声明式事务管理建立在AOP之上的。其本质是对方法前后进行拦截，然后在目标方法开始之前创建或者加入一个事务，在执行完目标方法之后根据执行情况提交或者回滚事务。
声明式事务管理也有两种常用的方式，一种是基于tx和aop名字空间的xml配置文件，另一种就是基于@Transactional注解。基于注解的方式更简单易用。

**异常相关**
可查的异常（checked exceptions）:Exception下除了RuntimeException外的异常
不可查的异常（unchecked exceptions）:RuntimeException及其子类和错误（Error）

非运行时异常是RuntimeException以外的异常，类型上都属于Exception类及其子类。如IOException、SQLException等以及用户自定义的Exception异常。对于这种异常，JAVA编译器强制要求我们必需对出现的这些异常进行catch并处理，否则程序就不能编译通过。

@Transactional(rollbackFor = Exception.class)

**在@Transactional注解中如果不配置rollbackFor属性,那么事物只会在遇到RuntimeException的时候才会回滚,加上rollbackFor=Exception.class,可以让事物在遇到非运行时异常时也回滚**

通过代理实现事务的提交回滚

![](./img/aop-Advice.png)

![](./img/aop目标对象.png)

根据上图的逻辑以下代码：

```java
@Service
public class MemberService {

    private OrderService orderService;

    public MemberService(OrderService orderService){
        this.orderService = orderService;
    }

    public void add(){
        orderService.add1();
    }
}
```

```java
@Service
public class OrderService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public void add1(){
        jdbcTemplate.execute("insert into ...");
        add2();
    }

    @Transactional(propagation = Propagation.NEVER)
    public void add2(){
        jdbcTemplate.execute("insert into ...");
    }
}
```

上面代码OrderService中add2()的注解@Transactional(propagation = Propagation.NEVER)

表示以非事务方式执行，如果存在事务，则引发异常。

实际当MemberService中执行add方法的时候，add1中确实开启了事务，但是add2并不会抛出异常。

原因是@Transactional是由AOP代理实现的，实际执行应该由**代理对象**执行，根据上面的代码MemberService中的OrderService对象是由spring自动注入的aop代理之后的对象，所以@Transactional生效，开启了事务，而当执行完事务增强逻辑之后AOP会调用**目标对象**的add1方法，当add1方法中调用add2时是直接通过目标对象调用，并没有事务增强逻辑，所以不会执行@Transactional(propagation = Propagation.NEVER)的判断。

要想实现执行@Transactional(propagation = Propagation.NEVER)的判断可以使用如下方法：

将add2移动到另一个类OrderServiceBean中，OrderService注入OrderServiceBean的代理对象即可执行增强逻辑

```java
@Component
public class OrderServiceBean {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(propagation = Propagation.NEVER)
    public void add2(){
        jdbcTemplate.execute("insert into ...");
    }
}
```

```java
@Service
public class OrderService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private OrderServiceBean orderServiceBean;

    @Transactional
    public void add1(){
        jdbcTemplate.execute("insert into ...");
        orderServiceBean.add2();
    }

}
```

**为什么有些公司禁止使用@Transactional 声明式事务？**

1、在方法上增加@Transaction 声明式事务，如果一个方法中的存在较多耗时操作，
就容易引发长事物问题，而长事物会带来锁的竞争影响性能，同时也会导致数据库
连接池被耗尽，影响程序的正常执行。
2、如果方法存在嵌套调用，而被嵌套调用的方法也声明了@Transaction 事物，就会
出现事物的嵌套调用行为，容易引起事物的混乱造成程序运行结果出现异常
3、@Transaction 声明式事务是将事物控制逻辑放在注解中，如果项目的复杂度增加，
事务的控制可能会变得更加复杂，导致代码可读性和维护性下降。
所以，为了避免这类问题，有些公司会推荐使用编程式事务，这样可以更加灵活地控制
事务的范围，减少事务的锁定时间，提高系统的性能

### 2、事务传播机制

Spring事务的传播机制有以下7种类型：

1. **REQUIRED**
   REQUIRED是默认的传播机制。如果当前方法已经有了一个事务，那么调用方法将在该事务中执行；如果当前方法没有事务，那么调用方法将开启一个新的事务。

2. **SUPPORTS**
   SUPPORTS表示当前方法支持事务，但是如果调用方法没有事务，那么当前方法不会开启事务。

3. **MANDATORY**
   MANDATORY表示当前方法必须在一个事务中执行，如果调用方法没有事务，那么将会抛出异常。

4. **REQUIRES_NEW**
   REQUIRES_NEW表示当前方法必须开启一个新的事务，如果调用方法已经有了一个事务，那么该事务将被挂起，当前方法将在新的事务中执行。
   
   这种传播机制下，需要小心死锁问题。A事务被挂起了，如果B事务要加的锁被A占用了就会发生死锁。

5. **NOT_SUPPORTED**
   NOT_SUPPORTED表示当前方法不应该在事务中执行，如果调用方法有事务，那么该事务将被挂起，当前方法将在没有事务的上下文中执行。

6. **NEVER**
   NEVER表示当前方法不应该在事务中执行，如果调用方法有事务，那么将会抛出异常。

7. **NESTED**
   NESTED表示当前方法将在一个嵌套的事务中执行，如果调用方法已经有了一个事务，那么当前方法将嵌套在该事务中执行；如果调用方法没有事务，那么当前方法将开启一个新的事务。

**REQUIRED、REQUIRES_NEW、NESTED的区别：**

REQUIRED（默认的传播机制）：
如果当前没有事务，则新建事务
如果当前存在事务，则加入当前事务，合并成一个事务

REQUIRES_NEW：
新建事务，如果当前存在事务，则把当前事务挂起
这种传播机制下，需要小心死锁问题。A事务被挂起了，如果B事务要加的锁被A占用了就会发生死锁。

NESTED：
如果当前没有事务，则新建事务
如果当前存在事务，则创建一个当前事务的子事务（嵌套事务），子事务不能单独提交，只能和父事务一起提交。

### 3、事务隔离级别

**数据库事务隔离级别：**

**1）Read Uncommitted（读未提交）**
一个事务在执行过程中，既可以访问其他事务未提交的新插入的数据，又可以访问未提交的修改数据。如果一个事务已经开始写数据，则另外一个事务不允许同时进行写操作，但允许其他事务读此行数据。此隔离级别可防止丢失更新。
**2）Read Committed（读已提交）oracle默认**
一个事务在执行过程中，既可以访问其他事务成功提交的新插入的数据，又可以访问成功修改的数据。读取数据的事务允许其他事务继续访问该行数据，但是未提交的写事务将会禁止其他事务访问该行。此隔离级别可有效防止脏读。
**3）Repeatable Read（可重复读取）mysql默认**
一个事务在执行过程中，可以访问其他事务成功提交的新插入的数据，但不可以访问成功修改的数据。读取数据的事务将会禁止写事务（但允许读事务），写事务则禁止任何其他事务。此隔离级别可有效防止不可重复读和脏读。
**4）Serializable（可串行化）**
提供严格的事务隔离。它要求事务序列化执行，事务只能一个接着一个地执行，不能并发执行。此隔离级别可有效防止脏读、不可重复读和幻读。但这个级别可能导致大量的超时现象和锁竞争，在实际应用中很少使用。

**Spring设置事务：**

@Transactional

参数描述：
**readOnly** 该属性用于设置当前事务是否为只读事务，设置为true表示只读，false则表示可读写，默认值为false。例如：@Transactional(readOnly=true)
**rollbackFor**该属性用于设置需要进行回滚的异常类数组，当方法中抛出指定异常数组中的异常时，则进行事务回滚。例如：指定单一异常类：@Transactional(rollbackFor=RuntimeException.class)指定多个异常类：@Transactional(rollbackFor={RuntimeException.class, Exception.class})
**rollbackForClassName**该属性用于设置需要进行回滚的异常类名称数组，当方法中抛出指定异常名称数组中的异常时，则进行事务回滚。例如：指定单一异常类名称@Transactional(rollbackForClassName=”RuntimeException”)指定多个异常类名称：@Transactional(rollbackForClassName={“RuntimeException”,”Exception”})
**noRollbackFor**该属性用于设置不需要进行回滚的异常类数组，当方法中抛出指定异常数组中的异常时，不进行事务回滚。例如：指定单一异常类：@Transactional(noRollbackFor=RuntimeException.class)指定多个异常类：@Transactional(noRollbackFor={RuntimeException.class, Exception.class})
noRollbackForClassName 该属性用于设置不需要进行回滚的异常类名称数组，当方法中抛出指定异常名称数组中的异常时，不进行事务回滚。例如：指定单一异常类名称：@Transactional(noRollbackForClassName=”RuntimeException”)指定多个异常类名称：@Transactional(noRollbackForClassName={“RuntimeException”,”Exception”})
**propagation**该属性用于设置事务的传播行为。例如：@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
**isolation**该属性用于设置底层数据库的事务隔离级别，事务隔离级别用于处理多事务并发的情况，通常使用数据库的默认隔离级别即可，基本不需要进行设置
**timeout**该属性用于设置事务的超时秒数，默认值为-1表示永不超时

**isolation事务隔离级别：**

1、Isolation.DEFAULT（默认）
这是默认值，表示跟连接的数据库设置的隔离级别一致。

2、Isolation.READ_UNCOMMITTED（未授权读取级别）
以操作同一行数据为前提，读事务允许其他读事务和写事务，未提交的写事务禁止其他写事务（但允许其他读事务）。此隔离级别可以防止更新丢失，但不能防止脏读、不可重复读、幻读。此隔离级别可以通过“排他写锁”实现。

3、Isolation.READ_COMMITTED（授权读取级别）
以操作同一行数据为前提，读事务允许其他读事务和写事务，未提交的写事务禁止其他读事务和写事务。此隔离级别可以防止更新丢失、脏读，但不能防止不可重复读、幻读。此隔离级别可以通过“瞬间共享读锁”和“排他写锁”实现。

4、Isolation.REPEATABLE_READ（可重复读取级别）
以操作同一行数据为前提，读事务禁止其他写事务（但允许其他读事务），未提交的写事务禁止其他读事务和写事务。此隔离级别可以防止更新丢失、脏读、不可重复读，但不能防止幻读。此隔离级别可以通过“共享读锁”和“排他写锁”实现。

5、Isolation.SERIALIZABLE（序列化级别）
提供严格的事务隔离。它要求事务序列化执行，事务只能一个接着一个地执行，不能并发执行。此隔离级别可以防止更新丢失、脏读、不可重复读、幻读。如果仅仅通过“行级锁”是无法实现事务序列化的，必须通过其他机制保证新插入的数据不会被刚执行查询操作的事务访问到。

Spring 事务隔离级别设置为（isolation = Isolation.DEFAULT）时，以数据库的隔离级别为准。Spring 事务隔离级别设置为非（isolation = Isolation.DEFAULT）时，Spring会拿到当前会话链接，重写了数据库的隔离级别，但没有直接修改数据库的隔离级别，此时以 Spring 事务为准。如果Spring设置的隔离级别数据库不支持，以数据库为准

## 6、推断构造方法

Spring在创建Bean的生命周期中，在实例化得到一个Bean对象的过程中就需要用到Bean的构造方法，一个类（Class）可能存在一个构造方法和多个构造方法的情况：

**一个构造方法**
一个类（Class）只有一个构造方法的情况，这个构造方法要么是无参构造方法，要么是有参构造方法，对于一个构造方法的情况，Spring采用构造方法实例化Bean对象的流程：

如果只有一个无参构造方法，那么Spring只能采用这个构造方法进行Bean对象的实例化。

如果只有一个有参构造方法，如果采用AnnotationConfigApplicationContext创建Spring容器时，Spring会根据构造方法的参数信息去寻找bean，然后传给构造方法、如果采用ClassPathXMLApplicationContext创建Spring容器时，表示使用XML的方式来使用bean，要么在XML中指定构造方法的参数值（手动指定），要么配置autowire=constructor让Spring自动取选择bean做为构造方法的参数。

**多个构造方法**
一个类（Class）中有多个构造方法的情况，Spring采用构造方法实例化Bean对象的流程：

如果开发者指定了想要使用的构造方法，那么Spirng就采用这个构造方法。

如果开发者没有指定想要使用的构造方法，则看开发者有没有让Spring自动去选择构造方法。

如果开发者没有让Spring自动去选择构造方法，则Spring利用无参构造方法，如果没有无参构造方法，则报错。

**指定构造方法**
开发者指定使用构造方法的方式：

xml中可以使用标签来指定构造方法参数，根据指定构造方法参数的个数，从而确定指定使用的构造方法。

通过@Autowired注解，@Autowired注解可以写在构造方法上，在构造方法上写@autowired注解，表示开发者指定采用这个构造方法，Spring通过byType+byName的方式去查找符合的bean做为构造方法的参数值。

**如果在多个构造方法上写了@Autowired注解，那么Spring启动会报错**，@Autowired注解有一个属性required，默认值为true，所以一个类中只能有一个构造方法标注@Autowired或@Autowired(required=true)，有多个会报错。但是可以有多个@Autowired(required=false)，这种情况需要Spring从这些构造方法中取自动选择一个构造方法。

Spring自动选择构造方法
如果开发者没有指定使用的构造方法，则看开发者有没有让Spring自动去选择构造方法，对于Sping自动选择构造方法只能用在ClassPathXMLApplicationContext，AnnotationConfigApplicationContext没有办法去指定bean可以让Spring自动取选择构造方法。可以在xml中指定某个bean的autowire=constructor。

## 7、依赖查找

代码：

```java
@Configuration
public class ServiceBeanConfig {

    @Bean("orderService1")
    public OrderService getOrderService1(){
        return new OrderService("service1");
    }

    @Bean("orderService2")
    public OrderService getOrderService2(){
        return new OrderService("service2");
    }
}
```

```java
@Configuration
public class ServiceBeanConfig {

    @Bean("orderService1")
    public OrderService getOrderService1(){
        return new OrderService("service1");
    }

    @Bean("orderService2")
    public OrderService getOrderService2(){
        return new OrderService("service2");
    }
}
```

```java
@Slf4j
@Service
public class MemberService {

    private OrderService orderService;

    public MemberService(OrderService orderService){
        this.orderService = orderService;
        log.debug("----------------------------------------------------");
        log.debug(orderService.toString());
        log.debug("----------------------------------------------------");
    }
}
```

最终MemberService中注入的是orderService，

**spring会先根据beanType查找，如果只找到一个，直接使用，如果找到多个bean再根据beanName查找。如果beanType能找到多个再根据beanName找不到则报错**

## 8、@Configuration，@Bean单例

@Bean 基础声明
Spring的@Bean注解用于告诉方法，产生一个Bean对象，然后这个Bean对象交给Spring管理。**产生这个Bean对象的方法Spring只会调用一次，随后这个Spring将会将这个Bean对象放在自己的IOC容器中**。

SpringIOC 容器管理一个或者多个bean，这些bean都需要在@Configuration注解下进行创建，在一个方法上使用@Bean注解就表明这个方法需要交给Spring进行管理。

有如下代码：

```java
@Configuration
public class ServiceBeanConfig {

    @Bean("orderService0")
    public OrderService getOrderService0(){
        OrderService orderService1 = getOrderService1();
        OrderService orderService2 = getOrderService1();
        System.out.println("________________________________________________");
        System.out.println(orderService1.hashCode());
        System.out.println(orderService2.hashCode());
        System.out.println(orderService2 == orderService1);
        System.out.println("________________________________________________");
        return orderService1;
    }

    @Bean("orderService1")
    public OrderService getOrderService1(){
        return new OrderService("service1");
    }

}
```

```shell
## getOrderService0方法打印结果
________________________________________________
-910525136
-910525136
true
________________________________________________
```

解释：从代码上看每次调用getOrderService1会new一个对象，但是结果看到getOrderService0中orderService1和orderService2是同一个对象。

**@Configuration会将ServiceBeanConfig的对象进行代理**，spring调用getOrderService0方法时实际上是通过**代理对象**调的方法。此时方法中调用getOrderService1()自然也就是代理对象的方法，代理对象增强了getOrderService1方法判断spring容器中是否存在Bean，存在直接返回，不存在时才创建

通过在JDK的lib目录下，找到sa-jdi.jar执行：java -classpath sa-jdi.jar “sun.jvm.hotspot.HSDB”  可查看cglib代理后的方法，在IDEA查看sa-jdi工具生成的class文件可以看到调用目标方法时使用的是super.getOrderService0();所以对象仍然还是代理对象。

![](./img/cglig代理反编译.jpg)

**@Configuration有一个属性proxyBeanMethods默认值为true**

查看注释可知是指定是否应该代理@Bean方法以强制执行Bean生命周期行为，例如，即使在用户代码中直接调用@Bean方法的情况下，也要返回共享的单例Bean实例。此功能需要方法拦截，通过运行时生成的CGLIB子类实现，该子类具有一些限制，例如配置类及其方法不允许声明final。

## 9、Bean的作用域

Spring支持五个作用域：**singleton、prototype、request、session、global session**

**1.singleton**：默认作用域Spring IOC容器仅存在一个Bean实例，Bean以单例方式存在，在创建容器时就同时自动创建了一个Bean对象。作用域范围是ApplicationContext中。

**2.prototype**：每次从容器中调用Bean时，都会返回一个新的实例，即每次调用getBean时。作用域返回是getBean方法调用直至方法结束。

相当于执行newXxxBean().Prototype是原型类型，再我们创建容器的时候并没有实例化，而是当我们获取Bean的时候才会去创建一个对象，而且我们每次获取到的对象都不是同一个对象。

**WebApplicationContext环境下会多三个**：
**3.request**：每次HTTP请求都会创建一个新的Bean，作用域范围是每次发起http请求直至拿到相应结果。该作用域仅适用于WebApplicationContext环境。

**4.session**：首次http请求创建一个实例，作用域是浏览器首次访问直至浏览器关闭。

同一个HTTP Session共享一个Bean，不同Session使用不通的Bean，仅适用于WebApplicationContext环境。

**5.global-session**：作用域范围是WebApplicationContext中。一般用于Portlet应用环境，该运用域仅适用于WebApplicationContext环境。

作用域范围比较：
prototype < request < session < global-session < singleton

为什么要定义作用域：
可以通过Spring配置的方式限定Spring Bean的作用范围，可以起到对Bean使用安全的保护作用。

## 10、Spring注入Bean到IOC容器的方式

**1、XML+Bean标签**
在application.xml文件中配置Bean标签

**2、@Compent注解**
通过@Compent注解放在类名上，用@ComponentScan扫描包下带有@Componet注解的bean，然后加至容器。
注意@Service,@Controller这些是复合注解，内部包含了@Component

**3、@Configration+@Bean**
注解声明为配置类（@Configration+@Bean），@Configuration用来声明一个配置类，然后使用@Bean注解声明一个bean，将其注入容器中

**4、@Import导入需要注入的Bean**
特殊情况下我们可以通过@Import导入需要注入的Bean，@Import可以导入三种类型的Bean对象
（1）导入一个普通的对象
（2）导入一个类，该类实现了ImportBeanDefinitionRegistrar接口，在重写的registerBeanDefinitions方法里面，能拿到BeanDefinitionRegistry bd的注册器，能手工往beanDefinitionMap中注册 beanDefinition
（3）导入一个类 该类实现了ImportSelector 重写selectImports方法该方法返回了String[]数组的对象，数组里面的类都会注入到spring容器当中

**5、容器初始化手动注入Bean**

```java
AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.registerBean("testService", TestService.class);
```

**6、可以实现FactoryBean接口，重写getObject方法，自定义注入bean。**
OpenFeign里的动态代理就是使用FactoryBean来实现的

**7、实现ImportBeanDefinitionRegistrar接口，可以动态注入Bean实例。**
ImportBeanDefinitionRegistrar接口是也是spring的扩展点之一,它可以支持我们自己写的代码封装成BeanDefinition对象;实现此接口的类会回调postProcessBeanDefinitionRegistry方法，注册到spring容器中。这个在Spring Boot里的启动注解有用到
例如

```java
public class ImportBeanDefinitionRegistrarTest implements ImportBeanDefinitionRegistrar{
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

        BeanDefinition beanDefinition = new GenericBeanDefinition();
        beanDefinition.setBeanClassName(TestBean.class.getName());
        MutablePropertyValues values = beanDefinition.getPropertyValues();
        values.addPropertyValue("id", 1);
        values.addPropertyValue("name", "ZhangSan");
        //这里注册bean
        registry.registerBeanDefinition("testBean", beanDefinition );

    }

}
```

# 11、过滤器和拦截器的区别

1、运行顺序不同（如图）：过滤器是在 Servlet 容器接收到请求之后，但在 Servlet
被调用之前运行的；而拦截器则是在 Servlet 被调用之后，但在响应被发送到客户
端之前运行的。

![](./img/过滤器拦截器执行顺序.jpg)

2、配置方式不同：过滤器是在 web.xml 中进行配置；而拦截器的配置则是在 Spring
的配置文件中进行配置，或者使用注解进行配置。
3、Filter 依赖于 Servlet 容器，而 Interceptor 不依赖于 Servlet 容器
4、Filter 在过滤是只能对 request 和 response 进行操作，而 interceptor 可以对
request、response、handler、modelAndView、exception 进行操作

# 12、Spring框架中的单例Bean是否线程安全？

Spring中的Bean对象默认是单例的，框架并没有对bean进行多线程的封装处理。
如果Bean是有状态的，那么就需要开发人员自己来保证线程的安全，最简单的办法就是改变Bean的作用域，把singleton改成prototype,这样每次请求bean对象就相当于是创建新的对象来保证线程的安全。
**有状态就是有数据存储的功能**
**无状态就是不会存储数据**
controller,service,dao，本身并不是线程安全的，只是调用里面的方法，而且多线程调用一个实例的方法，会在内存中复制一遍，这是自己线程的工作内存，是最安全的。

因此**在进行使用的时候，不要在Bean中声明任何有状态的实例变量或者类变量，如果必须如此，也推荐使用ThreadLocal把变量变成线程私有，如果bean的实例变量或者类变量需要在多个线程之间共享，那么就只能使用synchronized,lock,cas等这些实现线程同步的方法**

# 13、Spring中的设计模式

## 1.工厂模式：

工厂模式（Factory Pattern）是 Java 中最常用的设计模式之一。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。在工厂模式中，我们在创建对象时不会对客户端暴露创建逻辑，并且是通过使用一个共同的接口来指向新创建的对象。
Spring 中的BeanFactory就是工厂模式的体现，通过getBean方法来获取Bean对象。

## 2.单例模式：

单例模式（Singleton Pattern）是 Java 中最简单的设计模式之一。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。 这种模式涉及到一个单一的类，该类负责创建自己的对象，同时确保只有单个对象被创建。这个类提供了一种访问其唯一的对象的方式，可以直接访问，不需要实例化该类的对象。
在Spring中的bean默认使用单例，我们也可以手动设置scope改变bean的状态 。Spring注册bean过程中使用DDC单例模式，首先从缓存singletonObjects中获取bean实例，如果为null，对singletonObjects加锁，然后从缓存中获取锁，如果还是为null，就创建bean。这样双重判断，能够避免在加锁的瞬间，有其他依赖注入引发bean实例的创建，从而造成重复创建的结果。

## 3.代理模式：

在代理模式（Proxy Pattern）中，一个类代表另一个类的功能。这种类型的设计模式属于结构型模式。在代理模式中，我们创建具有现有对象的对象，以便向外界提供功能接口。
代理又分静态代理和动态代理，熟悉Spring AOP的，应该了解AOP使用动态代理实现的，分JDK和CGlib动态代理（JdkDynamicAopProxy和CglibAopProxy）。

## 4.原型模式：

原型模式（Prototype Pattern）是用于创建重复的对象，同时又能保证性能。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。这种模式是实现了一个原型接口，该接口用于创建当前对象的克隆。当直接创建对象的代价比较大时，则采用这种模式。例如，一个对象需要在一个高代价的数据库操作之后被创建。我们可以缓存该对象，在下一个请求时返回它的克隆，在需要的时候更新数据库，以此来减少数据库调用。
在 spring 中用到的原型模式有： scope="prototype" ，每次获取的是通过克隆生成的新实例，对其进行修改时对 原有实例对象不造成任何影响。

## 5.策略模式：

在策略模式（Strategy Pattern）中，一个类的行为或其算法可以在运行时更改。这种类型的设计模式属于行为型模式。在策略模式中，我们创建表示各种策略的对象和一个行为随着策略对象改变而改变的 context 对象。策略对象改变 context 对象的执行算法。
Spring中提供了InstantiationStrategy（实例化策略接口，子类被用来根据rootBeanDefinition来创建实例对象）实例化策略接口，其中有两个实现，分别是SimpleInstantiationStrategy以及CglibSubclassingInstantiationStrategy。加载资源文件的方式，使用了不同的方法，比如ClassPathResource,FileSystemResource,UrlResource但他们都有共同的接口Resource;在AOP的实现中，采用了两种不同的方式，JDK动态代理和CGLIB代理。

## 6.适配器模式：

适配器模式（Adapter Pattern）是作为两个不兼容的接口之间的桥梁。这种类型的设计模式属于结构型模式，它结合了两个独立接口的功能。这种模式涉及到一个单一的类，该类负责加入独立的或不兼容的接口功能。举个真实的例子，读卡器是作为内存卡和笔记本之间的适配器。您将内存卡插入读卡器，再将读卡器插入笔记本，这样就可以通过笔记本来读取内存卡。
SpringAOP中 AdvisorAdapter ，它有三个实现：MethodBeforAdviceAdapter、AfterReturnningAdviceAdapter、ThrowsAdviceAdapter。 Spring会根据不同的 AOP 配置来使用对应的 Advice，与策略模式不同的是，一个方法可以同时拥有多个Advice。

## 7.观察者模式：

当对象间存在一对多关系时，则使用观察者模式（Observer Pattern）。比如，当一个对象被修改时，则会自动通知依赖它的对象。观察者模式属于行为型模式。
Spring 事件驱动模型中的三种角色：

### 事件角色：

Spring 中默认的几种事件，都是对 ApplicationContextEvent 的实现(继承自ApplicationContextEvent)：
ContextStartedEvent：ApplicationContext 启动后触发的事件;
ContextStoppedEvent：ApplicationContext 停止后触发的事件;
ContextRefreshedEvent：ApplicationContext 初始化或刷新完成后触发的事件;
ContextClosedEvent：ApplicationContext 关闭后触发的事件。

### 事件监听者角色：

ApplicationListener 充当了事件监听者角色，它是一个接口，里面只定义了一个 onApplicationEvent（）方法来处理ApplicationEvent
在 Spring中我们只要实现 ApplicationListener 接口实现 onApplicationEvent() 方法即可完成监听事件

### 事件发布者角色：

ApplicationEventPublisher 接口的publishEvent（）这个方法在AbstractApplicationContext类中被实现，阅读这个方法的实现，你会发现实际上事件真正是通过ApplicationEventMulticaster来广播出去的

### Spring 的事件流程总结

定义一个事件: 实现一个继承自 ApplicationEvent，并且写相应的构造函数；
定义一个事件监听者：实现 ApplicationListener 接口，重写 onApplicationEvent() 方法；
使用事件发布者发布消息: 可以通过 ApplicationEventPublisher 的 publishEvent() 方法发布消息。

## 8.模板模式：

在模板模式（Template Pattern）中，一个抽象类公开定义了执行它的方法的方式/模板。它的子类可以按需要重写方法实现，但调用将以抽象类中定义的方式进行。这种类型的设计模式属于行为型模式。定义一个操作中的算法的骨架，而将一些步骤延迟到子类中。模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
Spring中的 AbstractPlatformTransactionManager 事务管理器 就是非常经典的模板模式的应用，父类都对于自己要实现的逻辑搭建了一个骨架，主要的功能是由抽象方法完成，由子类来完成。

## 9.责任链模式：

责任链模式（Chain of Responsibility Pattern）为请求创建了一个接收者对象的链。这种模式给予请求的类型，对请求的发送者和接收者进行解耦。这种类型的设计模式属于行为型模式。在这种模式中，通常每个接收者都包含对另一个接收者的引用。如果一个对象不能处理该请求，那么它会把相同的请求传给下一个接收者，依此类推。
DispatcherServlet 中的 doDispatch() 方法中获取与请求匹配的处理器HandlerExecutionChain，this.getHandler() 方法的处理使用到了责任链模式。

## 10、装饰者模式

装饰模式指的是在不必改变原类文件和使用继承的情况下，动态地扩展一个对象的功能。它是通过创建一个包装对象，也就是装饰来包裹真实的对象。

Spring源码中类型带Wrapper或者Decorator的都是

# 14、@Import注解

代码地址：

./md-code/spring-import;

./md-code/spring-import-source

spring-import使用@Import，导入spring-import-source模块的类到spring容器中  
spring-import-source模块没有spring ，  
提供类：  

com.pan.service.source.OrderService  

com.pan.service.source.UserServic  

**@Import注解提供了三种用法**  

1、@Import一个普通类 spring会将该类加载到spring容器中  

2、@Import一个类，该类实现了**ImportBeanDefinitionRegistrar**接口，在重写的registerBeanDefinitions方法里面，能拿到BeanDefinitionRegistry bd的注册器，能手工往beanDefinitionMap中注册 beanDefinition  

3、@Import一个类 该类实现了ImportSelector 重写selectImports方法该方法返回了String[]数组的对象，数组里面的类都会注入到spring容器当中  

**ImportBeanDefinitionRegistrar**例子：  
通过@Import注解引入MyImportBeanDefinitionRegistrar类，通过添加BeanDefinition的方式将外部类添加到spring容器中。  

**FactoryBean**：InterfaceFactoryBean 通过FactoryBean方式+代理的方式，实现本模块定义接口，外部模块实现。类似于mybatis的mapper接口。
