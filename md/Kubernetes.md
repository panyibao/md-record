# kubernetes部署

版本
Docker： 23.0.1
Kubernetes: v1.21.0
calico: 3.21

**如果使用新版要注意Kubernetes和calico的版本对应关系**

## 系统环境准备

配置主机名 hostnamectl set-hostname xxx

hostnamectl set-hostname master

添加主机名称解析 cat >> /etc/hosts 回车后开始编辑输入内容

```shell
cat >>  /etc/hosts
192.168.146.133 master
EOF
#按 cntl+c 组合键结束编辑。
```

**主机安全配置**

kubernetes和docker在运行中会产生大量的iptables规则，为了不让系统规则跟它们混淆，直接关闭系统的规则
**关闭firewalld**
systemctl stop firewalld
systemctl disable firewalld
firewall-cmd --state

**关闭iptables服务**
systemctl stop iptables
systemctl disable iptables

selinux是inux系统下的一个安全服务,如果不关闭它，在安装集群中会产生各种各样的奇葩问题
sed -ri 's/SELINUX=enforcing/SELINUX=disabled/' etc/selinux/config

swap分区指的是虚拟内存分区，它的作用是在物理内存使用完之后，将磁盘空间虚拟成内存来使用
启用swap设备会对系统的性能产生非常负面的影响，因此kubernetes要求每个节点都要禁用swap设备
但是如果因为某些原因确实不能关闭swap分区，就需要在集群安装过程中通过明确的参数进行配置说明

**关闭swap分区**

swapoff -a

永久关闭swap分区（使用kubeadm部署必须关闭swap分区，修改配置文件后需要重启操作系统）

cat etc/fstab
#/dev/mapper/cl-swap swap swap defaults 0 0

在swap文件系统对应的行，行首添加#表示注释

在kubernetes中service有两种代理模型，-种是基于iptables的，一种是基于ipvs的。

**添加网桥过滤及地址转发**

```shell
cat > /etc/sysctl.d/k8s.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
#按 cntl+c 组合键结束编辑。
modprobe br_netfilter
```

**查看模块**

lsmod | grep br_netfilter

**使配置文件生效**

sysctl -p /etc/sysctl.d/k8s.conf

**开启ipvs**

在所有节点添加ipvs模块（所有节点执行）

```shell
yum install ipset ipvsadm -y
cat >> /etc/sysconfig/modules/ipvs.modules <<EOF
#!/bin/bash
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack_ipv4
EOF
```

**加载并检查模块**

```shell
chmod 755 /etc/sysconfig/modules/ipvs.modules && bash /etc/sysconfig/modules/ipvs.modules && lsmod | grep -e ip_vs -e nf_conntrack_ipv4
```

#允许iptables检测桥接流量

#将桥接的 IPv4 流量传递到 iptables 的链：

## 修改 /etc/sysctl.conf

**如果有配置，则修改**

```shell
sed -i "s#^net.ipv4.ip_forward.*#net.ipv4.ip_forward=1#g"  /etc/sysctl.conf
sed -i "s#^net.bridge.bridge-nf-call-ip6tables.*#net.bridge.bridge-nf-call-ip6tables=1#g"  /etc/sysctl.conf
sed -i "s#^net.bridge.bridge-nf-call-iptables.*#net.bridge.bridge-nf-call-iptables=1#g"  /etc/sysctl.conf
sed -i "s#^net.ipv6.conf.all.disable_ipv6.*#net.ipv6.conf.all.disable_ipv6=1#g"  /etc/sysctl.conf
sed -i "s#^net.ipv6.conf.default.disable_ipv6.*#net.ipv6.conf.default.disable_ipv6=1#g"  /etc/sysctl.conf
sed -i "s#^net.ipv6.conf.lo.disable_ipv6.*#net.ipv6.conf.lo.disable_ipv6=1#g"  /etc/sysctl.conf
sed -i "s#^net.ipv6.conf.all.forwarding.*#net.ipv6.conf.all.forwarding=1#g"  /etc/sysctl.conf
```

**可能没有，追加**

```shell
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-ip6tables = 1" >> /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.forwarding = 1"  >> /etc/sysctl.conf
```

**执行命令以应用**

```shell
sysctl -p
```

## kubernetes单机部署

### 配置国内的k8s的yum源

```shell
cat << EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```

### yum源更新

```shell
yum update
```

### 卸载旧版本

```shell
yum remove -y kubelet kubeadm kubectl
```

### 查看可以安装的版本

```shell
yum list kubelet --showduplicates | sort -r
```

### 安装

```shell
yum install -y kubelet-1.21.0 kubeadm-1.21.0 kubectl-1.21.0
#也可以不指定版本yum install -y kubelet kubeadm kubectl
```

### 开机启动kubelet

```shell
systemctl enable kubelet && systemctl start kubelet
```

执行kubeadm config images list 查看K8S集群需要的docker镜像

```shell
kubeadm config images list
```

下载核心镜像 kubeadm config images list：查看需要哪些镜像###########
封装成images.sh文件，注意版本对应

```shell
tee ./images.sh <<-'EOF'
#!/bin/bash
images=(
kube-apiserver:v1.21.0
kube-proxy:v1.21.0
kube-controller-manager:v1.21.0
kube-scheduler:v1.21.0
coredns:v1.8.0
etcd:3.4.13-0
pause:3.4.1
)
for imageName in ${images[@]} ; do
docker pull registry.cn-hangzhou.aliyuncs.com/hzx_k8s_images/$imageName
done
EOF
####因为国内拉去镜像需要限流所以需要从阿里云进行拉去
#####封装结束
chmod +x images.sh && ./images.sh
```

### 添加主机名称映射

 集群情况下每个节点必须加master的映射

这里文档之前做过了，也可以不加

#添加主机名称解析 cat >> /etc/hosts 回车后开始编辑输入内容
cat >>  /etc/hosts
192.168.146.133 master
EOF
#按 cntl+c 组合键结束编辑。

### k8s 主节点初始化

```shell
kubeadm init \
--apiserver-advertise-address=192.168.146.133 \
--control-plane-endpoint=master \
--image-repository registry.cn-hangzhou.aliyuncs.com/hzx_k8s_images \
--kubernetes-version v1.21.0 \
--service-cidr=10.96.0.0/16 \
--pod-network-cidr=10.244.0.0/16 \
--ignore-preflight-errors=all
```

#--apiserver-advertise-address=192.168.146.133   
#这个参数就是master主机的IP地址，例如我的Master主机的IP是：192.168.146.133
#--control-plane-endpoint=master
#多主节点必选项,用于指定主节点的固定访问地址,可是IP地址或DNS名称,会被用于集群管理员及集群组件的kubeconfig配置文件的API Server的访问地址
#注意：如果用的时候云服务器一定要用内网IP
#--image-repository=registry.aliyuncs.com/google_containers 
#这个是镜像地址，由于国外地址无法访问，故使用的阿里云仓库地址：registry.aliyuncs.com/
#--kubernetes-version=v1.21.0  
#这个参数是下载的k8s软件版本号
#--service-cidr=10.96.0.0/12     
#这个参数后的IP地址直接就套用10.96.0.0/12 ,以后安装时也套用即可，不要更改
#--pod-network-cidr=10.244.0.0/16
#k8s内部的pod节点之间网络可以使用的IP段，不能和service-cidr写一样，如果不知道怎么配，就先用这个10.244.0.0/16

当您使用kubeadm init时，请指定pod-network-cidr。确保主机/主网络的IP不在您引用的子网中。

即如果您的网络运行在192.168.*.*使用10.0.0.0/16

如果您的网络是10.0.*.*使用192.168.0.0/16

192.168.0.1与192.168.100.0/16在同一子网中。简而言之，当您使用16子网标记时，它意味着使用192.168.*.*中的任何内容。

#--ignore-preflight-errors=all
#忽略检查的一些错误,如kubernetes和docker版本对应关系检查，实际中按指定关系版本安装，不建议加这个命令

#结束以后，按照控制台引导继续往下

成功安装后有如下内容显示：

![](./img/kubernetes-2.jpg)

将内容复制出来：

```shell
Your Kubernetes control-plane has initialized successfully!

# 使用集群执行下面的命令
To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

# root用户可以运行下面的命令
Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

# 您现在应该在集群中部署一个pod网络插件
You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

# 现在，您可以通过复制证书颁发机构来加入任意数量的控制平面节点（主节点）
# 和每个节点上的服务帐户密钥，然后以root身份运行以下操作：
# 通过下面命令添加主节点
You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join master:6443 --token ol47ta.phebup66nvpgeb6q \
        --discovery-token-ca-cert-hash sha256:b254fc968e55a1e12f409a108ed059fc3beb0d1b4528be9b16144c1b29f8329f \
        --control-plane

# 然后，您可以通过在每个工作节点上以root身份运行以下操作来加入任意数量的工作节点：
# 通过下面命令添加工作节点
Then you can join any number of worker nodes by running the following on each as root:

kubeadm join master:6443 --token ol47ta.phebup66nvpgeb6q \
        --discovery-token-ca-cert-hash sha256:b254fc968e55a1e12f409a108ed059fc3beb0d1b4528be9b16144c1b29f8329f
```

**根据提示执行下面命令**

```shell
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/
```

主节点执行查看节点命令

```shell
kubectl get nodes
```

结果：

![](./img/kubernetes-3.jpg)

当创建单机版的 k8s 时，这个时候 master 节点是默认不允许调度 pod,执行命令

```shell
kubectl taint nodes --all node-role.kubernetes.io/master-
kubectl taint nodes --all node.kubernetes.io/disk-pressure-
```

**部署网络插件Calico**

```shell
# 下载配置文件
curl https://projectcalico.docs.tigera.io/archive/v3.21/manifests/calico.yaml -O
# 修改pod-network-cidr=10.244.0.0/16地址,cat + grep 查找位置行数，vim编辑
cat calico.yaml | grep -n -C20  192.168
vim calico.yaml +4243

# yaml文件基于集群安装 apply 应用 -f 文件 calico.yaml文件名
kubectl apply -f calico.yaml
```

注意如果安装kubernetes时--pod-network-cidr=10.244.0.0/16有改动calico.yaml中的对应参数也要改动：

![](./img/kubernetes-4.jpg)

### 解决coredns镜像无法拉去

查看镜像信息：

```shell
kubectl get pods coredns-6cf748779-4r9gv -n kube-system -o yaml | grep image:
```

结果显示：

image: registry.cn-hangzhou.aliyuncs.com/hzx_k8s_images/coredns/coredns:v1.8.0

- image: registry.cn-hangzhou.aliyuncs.com/hzx_k8s_images/coredns/coredns:v1.8.0

通过docker制作镜像：

```shell
# 这里要拉取v1.8.0，latest版本可能版本不对应报错
docker pull louwy001/coredns-coredns:v1.8.0
docker tag docker.io/louwy001/coredns-coredns:v1.8.0 registry.cn-hangzhou.aliyuncs.com/hzx_k8s_images/coredns/coredns:v1.8.0# kubernete命令
```

kube-controllers无法拉取时也可以按此方法制作

### 污点

当某种条件为真时，节点控制器会自动给节点添加一个污点。当前内置的污点包括：

**node.kubernetes.io/not-ready**：节点未准备好。这相当于节点状态 Ready 的值为 "False"。
**node.kubernetes.io/unreachable**：节点控制器访问不到节点. 这相当于节点状态 Ready 的值为 "Unknown"。
**node.kubernetes.io/memory-pressure**：节点存在内存压力。
**node.kubernetes.io/disk-pressure**：节点存在磁盘压力。
**node.kubernetes.io/pid-pressure**: 节点的 PID 压力。
**node.kubernetes.io/network-unavailable**：节点网络不可用。
**node.kubernetes.io/unschedulabl**e: 节点不可调度。
**node.cloudprovider.kubernetes.io/uninitialized**：如果 kubelet 启动时指定了一个 "外部" 云平台驱动， 它将给当前节点添加一个污点将其标志为不可用。在 cloud-controller-manager 的一个控制器初始化这个节点后，kubelet 将删除这个污点。

**删除污点**
kubectl taint nodes --all node-role.kubernetes.io/master-
kubectl taint nodes --all node.kubernetes.io/disk-pressure-

## 重置kubeadm

```shell
kubeadm reset

#清除环境
kubeadm reset
rm -rf /etc/cni/*
ipvsadm --clear
iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X
systemctl stop kubelet
systemctl stop docker
rm -rf /var/lib/cni/*
rm -rf /var/lib/kubelet/*
rm -rf $HOME/.kube/config
systemctl start docker
```

## 卸载k8s

```shell
yum -y remove kubelet kubeadm kubectl
sudo kubeadm reset -f
sudo rm -rvf $HOME/.kube
sudo rm -rvf ~/.kube/
sudo rm -rvf /etc/kubernetes/
sudo rm -rvf /etc/systemd/system/kubelet.service.d
sudo rm -rvf /etc/systemd/system/kubelet.service
sudo rm -rvf /usr/bin/kube*
sudo rm -rvf /etc/cni
sudo rm -rvf /opt/cni
sudo rm -rvf /var/lib/etcd
sudo rm -rvf /var/etcd
```

## 常用命令

查看集群所有节点

```shell
kubectl get nodes
```

根据配置文件创建集群资源

```shell
# kubectl apply -f xxx.yaml
kubectl apply -f calico.yaml
```

查看集群部署了那些应用

```shell
# 部署了那些应用
kubectl get pods -A
# 部署了那些应用 显示更多信息，包含ip
kubectl get pods -n kube-system -o wide
```

查看pod详细信息

```shell
kubectl describe pod calico-node-9bgbs -n kube-system
```

删除pod自动重新安装

```shell
kubectl delete pod coredns-6cf748779-55mkl -n kube-system
```

kubectl cluster-info —查看k8s 集群的信息
kubectl version 查看当前k8s 集群的版本
kubectl get ns 获取所有名称空间 ns也可以用namespace
kubectl create ns hello 创建名称空间hello ns也可以用namespace
kubectl delete ns hello 删除名称空间hello ns也可以用namespace
kubectl logs pod名 查看pod日志
kubectl logs -f pod名 查看实时pod日志
kubectl run pod --image=nginx -n udiannet-dev 在udiannet-dev的namespace 下运行一个pod，不指定-n udiannet-dev默认是default
kubectl delete pods pod-864f9875b9-492tz -n udiannet-dev 删除该pod（要指定namespace）namespace里面会有一个pod控制器Deployment，当namespace里面的pod被删除的时候，pod控制器就会帮忙创建一个新的pod，这就是kubernetes的自我修复功能

推荐使用deployment的方式部署pod

**kubectl create deployment my-nginx --image=nginx:latest --port=80 -n dev** 在dev命名空间创建一个名为my-nginx的deployment，运行nginx镜像

kubectl scale deployment my-nginx --replicas=2 -n dev 在命名空间dev下根据名为my-nginx的deployment扩展为2个Pod

**查看deployment的信息**
kubectl get deploy
kubectl get deploy -o wide
kubectl get deployment -w  #-w:watch，此时处于监控状态，直到available.
kubectl get pod -o wide

**查看deployment的详细信息**
kubectl get pods --show-labels -n dev
kubectl describe deploy my-nginx -n dev

**进入Kubernetes的pod**
kubectl exec -ti your-pod-name -n your-namespace – /bin/sh

删除
kubectl delete deploy my-nginx -n dev

如：
my-nginx1：10.244.219.93:80
my-nginx2：10.244.219.94:80

**暴露my-nginx80端口到8000，创建服务（service）资源**
kubectl expose deploy my-nginx --port=8000 --target-port=80
查看service资源
kubectl get service
结果如下
名称：my-nginx   IP:10.96.221.8 
可以通过 10.96.221.8:8000 的方式访问到my-nginx的两个服务（负载均衡）
或者通过my-nginx.default.svc:8000 访问 my-nginx服务名，default名称空间，svc固定service简写
以上仅能在进群内部pod进行访问

删除服务资源
kubectl delete svc my-nginx
**暴露到集群外部**
kubectl expose deploy my-nginx --port=8000 --target-port=80 --type=NodePort
--type="": 服务类型ClusterIP(集群内访问，默认), NodePort(节点端口模式，可以集群外访问)

查看service结果
my-nginx     NodePort    10.96.184.206   <none>        8000:30083/TCP   6s
NodePort模式可通过外部浏览器访问到my-nginx，这里随机映射到了30083，通过公网ip（虚拟机使用虚拟机ip）可以访问如http://192.168.146.133:30083/ 

# 安装kubernetes-dashboard

具体版本支持可以去github查找https://github.com/kubernetes/dashboard/releases
这里选择版本：v2.4.0

**下载配置文件**

```shell
curl https://raw.githubusercontent.com/kubernetes/dashboard/v2.4.0/aio/deploy/recommended.yaml -O
```

#默认Dashboard只能集群内部访问，修改Service为NodePort类型，暴露到外部：
vim recommended.yaml

 vim 查找ports字符串

: /ports
修改暴露到外部端口spec.ports.nodePort: 30001
修改spec.type为NodePort

![](./img/kubernetes-6.jpg)

**通过配置文件创建pod**

```shell
kubectl apply -f recommended.yaml
```

**添加资源权限用户**

```shell
vim admin-token.yaml
```

用户配置文件admin-token.yaml内容：

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
```

应用用户配置文件

```shell
kubectl apply -f admin-token.yaml
```

获取kubernetes-dashboard登陆token所在位置

```shell
kubectl -n kubernetes-dashboard get secrets
```

![](./img/kubernetes-7.jpg)

重新获取kubernetes-dashboard登陆token

```shell
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```

![](./img/kubernetes-8.jpg)

访问地址：https://192.168.146.133:30001/  输入token登陆，查看现有pods

![](./img/kubernetes-9.jpg)

安装完成。

# 安装KubeSphere

## 前置环境

安装nfs文件系统服务器nfs-server

在每个机器上安装nfs-utils
yum install -y nfs-utils
在master 执行以下命令,执行命令启动 nfs 服务并且创建共享目录并且生效配置,检查配置是否生效exportfs
echo "/nfs/data/ *(insecure,rw,sync,no_root_squash)" > /etc/exports
mkdir -p /nfs/data
systemctl enable rpcbind
systemctl enable nfs-server
systemctl start rpcbind
systemctl start nfs-server
exportfs -r

显示可挂载文件夹
showmount -e 192.168.146.133
创建文件夹
mkdir -p /nfs/data
挂载文件夹
mount -t nfs 192.168.146.133:/nfs/data /nfs/data

## 配置默认 StorageClass

请使用 kubectl get sc 进行确认Kubernetes 集群已配置默认 StorageClass

配置默认存储

创建一个yaml文件，并且使用命令 kubectl apply -f sc.yaml 创建pod,创建好之后使用kubectl get sc查看配置是否生效。

```yaml
## 创建了一个存储类
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: nfs-storage
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
provisioner: k8s-sigs.io/nfs-subdir-external-provisioner
parameters:
  archiveOnDelete: "true"  ## 删除pv的时候，pv的内容是否要备份
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nfs-client-provisioner
  labels:
    app: nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nfs-client-provisioner
  template:
    metadata:
      labels:
        app: nfs-client-provisioner
    spec:
      serviceAccountName: nfs-client-provisioner
      containers:
        - name: nfs-client-provisioner
          image: registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/nfs-subdir-external-provisioner:v4.0.2
          # resources:
          #    limits:
          #      cpu: 10m
          #    requests:
          #      cpu: 10m
          volumeMounts:
            - name: nfs-client-root
              mountPath: /persistentvolumes
          env:
            - name: PROVISIONER_NAME
              value: k8s-sigs.io/nfs-subdir-external-provisioner
            - name: NFS_SERVER
              value: 192.168.146.133 ## 指定自己nfs服务器地址
            - name: NFS_PATH  
              value: /nfs/data  ## nfs服务器共享的目录
      volumes:
        - name: nfs-client-root
          nfs:
            server: 192.168.146.133
            path: /nfs/data
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: nfs-client-provisioner-runner
rules:
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["create", "update", "patch"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: run-nfs-client-provisioner
subjects:
  - kind: ServiceAccount
    name: nfs-client-provisioner
    # replace with namespace where provisioner is deployed
    namespace: default
roleRef:
  kind: ClusterRole
  name: nfs-client-provisioner-runner
  apiGroup: rbac.authorization.k8s.io
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: leader-locking-nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
rules:
  - apiGroups: [""]
    resources: ["endpoints"]
    verbs: ["get", "list", "watch", "create", "update", "patch"]
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: leader-locking-nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
subjects:
  - kind: ServiceAccount
    name: nfs-client-provisioner
    # replace with namespace where provisioner is deployed
    namespace: default
roleRef:
  kind: Role
  name: leader-locking-nfs-client-provisioner
  apiGroup: rbac.authorization.k8s.io
```

## 安装metrics-server集群指标监控组件

创建metrics.yaml使用命令kubectl apply -f metrics.yaml 创建pod，安装好之后使用kubectl top nodes 即可查看每个节点的cpu内存等使用情况

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  labels:
    k8s-app: metrics-server
    rbac.authorization.k8s.io/aggregate-to-admin: "true"
    rbac.authorization.k8s.io/aggregate-to-edit: "true"
    rbac.authorization.k8s.io/aggregate-to-view: "true"
  name: system:aggregated-metrics-reader
rules:
- apiGroups:
  - metrics.k8s.io
  resources:
  - pods
  - nodes
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  labels:
    k8s-app: metrics-server
  name: system:metrics-server
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - nodes
  - nodes/stats
  - namespaces
  - configmaps
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server-auth-reader
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: extension-apiserver-authentication-reader
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server:system:auth-delegator
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:auth-delegator
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  labels:
    k8s-app: metrics-server
  name: system:metrics-server
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:metrics-server
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: v1
kind: Service
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server
  namespace: kube-system
spec:
  ports:
  - name: https
    port: 443
    protocol: TCP
    targetPort: https
  selector:
    k8s-app: metrics-server
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server
  namespace: kube-system
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  strategy:
    rollingUpdate:
      maxUnavailable: 0
  template:
    metadata:
      labels:
        k8s-app: metrics-server
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --kubelet-insecure-tls
        - --secure-port=4443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        image: registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/metrics-server:v0.4.3
        imagePullPolicy: IfNotPresent
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /livez
            port: https
            scheme: HTTPS
          periodSeconds: 10
        name: metrics-server
        ports:
        - containerPort: 4443
          name: https
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /readyz
            port: https
            scheme: HTTPS
          periodSeconds: 10
        securityContext:
          readOnlyRootFilesystem: true
          runAsNonRoot: true
          runAsUser: 1000
        volumeMounts:
        - mountPath: /tmp
          name: tmp-dir
      nodeSelector:
        kubernetes.io/os: linux
      priorityClassName: system-cluster-critical
      serviceAccountName: metrics-server
      hostNetwork: true
      volumes:
      - emptyDir: {}
        name: tmp-dir
---
apiVersion: apiregistration.k8s.io/v1
kind: APIService
metadata:
  labels:
    k8s-app: metrics-server
  name: v1beta1.metrics.k8s.io
spec:
  group: metrics.k8s.io
  groupPriorityMinimum: 100
  insecureSkipTLSVerify: true
  service:
    name: metrics-server
    namespace: kube-system
  version: v1beta1
  versionPriority: 100
```

应用： kubectl apply -f metrics.yaml
检测： kubectl top nodes 

## 安装KubeSphere

下载核心文件

```shell
wget https://github.com/kubesphere/ks-installer/releases/download/v3.2.1/kubesphere-installer.yaml
wget https://github.com/kubesphere/ks-installer/releases/download/v3.2.1/cluster-configuration.yaml
```

安装

```shell
kubectl apply -f kubesphere-installer.yaml
kubectl apply -f cluster-configuration.yaml
```

使用命令查看进度

```shell
kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l app=ks-install -o jsonpath='{.items[0].metadata.name}') -f
```

**解决找不到证书的问题**

```shell
kubectl -n kubesphere-monitoring-system create secret generic kube-etcd-client-certs  --from-file=etcd-client-ca.crt=/etc/kubernetes/pki/etcd/ca.crt  --from-file=etcd-client.crt=/etc/kubernetes/pki/apiserver-etcd-client.crt  --from-file=etcd-client.key=/etc/kubernetes/pki/apiserver-etcd-client.key
```

查看pod是否都正常：

![](./img/kubesphere-1.jpg)

通过以下命令来检查控制台的端口（默认为 30880）：

```shell
kubectl get svc/ks-console -n kubesphere-system
```

浏览器可通过ip和端口进行访问

# Kubernetes核心组件

客户端：
**kubectl**
kubectl是Kubernetes的命令行工具，它是Kubernetes的官方命令行客户端。它允许用户通过命令行与Kubernetes集群进行交互，并执行各种操作，包括管理集群中的资源对象、配置集群、故障排查和日志查看等。

**kube-dashboard**
kube-dashboard 是 Kubernetes 的 Web 控制台，提供了一个可视化界面来管理和监控集群。它可以查看集群的状态、Pod 的运行情况、服务的暴露情况等，并提供一些基本的操作功能，如创建、删除和扩缩容 Pod。

服务端：
**1.1 kube-apiserver**
kube-apiserver 是 Kubernetes API 的入口点，它充当控制平面的前端接口，负责处理外部请求并与其他组件通信。它接收来自用户、命令行工具和其他组件的请求，并将其转换为 API 对象的操作。它还提供了认证、授权和准入控制等安全机制。

**1.2 kube-controller-manager**
kube-controller-manager 是一组控制器的集合，用于监控和管理集群中的各种资源。它包括节点控制器、副本控制器、服务控制器、端点控制器等。这些控制器负责监视资源的状态变化，并根据需要采取相应的操作，确保集群中的资源处于期望的状态。

**1.3 cloub-controller-manager**
kube-controller-manager是Kubernetes的一个组件，它提供了一个控制平面，用于管理Kubernetes集群。Cloud Controller Manager通过插件机制，可以对接各种云服务提供商(第三方云服务)的资源，例如阿里云的负载均衡（CLB，原SLB）、虚拟私有云（VPC）等。这样，Kubernetes集群就可以与这些云服务商的资源进行交互，实现负载均衡、跨节点通信等功能。

**1.4 kube-scheduler**
kube-scheduler 负责将新创建的 Pod 调度到集群中的节点上。它根据一系列调度策略和资源需求，选择合适的节点来运行 Pod。它考虑节点的负载情况、资源可用性、亲和性和反亲和性等因素，以实现负载均衡和高效的资源利用。

**1.5 etcd**
etcd 是 Kubernetes 的分布式键值存储系统，用于存储集群的配置数据和状态信息。它提供高可用性和一致性，并用于存储集群中的各种信息，如节点信息、Pod 信息、服务信息、配置信息等。

Node 组件：
2.1 Kubelet
Node节点的监视器，以及与master节点的通讯器。kubelet 是master节点安插在node节点上的“眼线”，它会定时向API server 汇报自己的Node节点上运行的服务状态，并接受来自master节点的指示采取调整措施。 
从 Master 节点获取自己节点上 Pod 的期望状态（比如运行什么容器、运行的副本数量、网络或者存储如何配置等），直接跟容器引擎交互实现容器的生命周期管理，如果自己节点上 Pod 的状态与期望状态不一致，则调用对应的容器平台接口（即 docker 的接口）达到这个状态。管理镜像和容器的清理工作，保证节点上镜像不会占满磁盘空间，退出的容器不会占用太多资源。

2.2 Kube-Proxy
1、在每个 Node 节点上实现 Pod 网络代理，是 Kubernetes Service资源的载体，负责维护网络规划和四层负载均衡工作。负责写入规则至 iptables、ipvs 实现服务映射访问的。
2、Kube-Proxy 本身不是直接给 Pod 提供网络，Pod 的网络是由 Kubelet 提供的，Kube-Proxy实际上维护的是虚拟的 Pod 集群网络。
3、Kube-api server 通过监控 Kube-Proxy 进行对 Kubernetes Service 的更新和端点的维护。
4、Kube-Proxy 是 K8S 集群内部的负载均衡器。它是一个分布式代理服务器，在 K8S 的每个节点上都会运行一个Kube-proxy。

**容器运行时环境**

Kubernetes 支持多种容器运行时，包括 Docker、containerd、CRI-O 和其他实现 Kubernetes Container Runtime Interface (CRI) 的运行时。它是 Kubernetes 的前端，处理和响应命令行接口（CLI）、用户接口（UI）或其他管理系统的请求。Kubernetes（简称 K8s）是一个开源的，用于自动化部署、扩展和管理容器化应用

组件结构如图：

![](./img/Kubernetes核心组件图示.png)

# 服务的分类

## 1、无状态

代表应用(Nginx,Apache) 

不会对本地环境产生任何依赖，例如不会存储数据到本地磁盘。 

优点：对客户端透明，无依赖关系，可以高效实现扩容迁移 

缺点：不能存储数据，需要额外的数据服务支撑。

## 2、有状态

代表应用(MySql,Redis)
会对本地环境产生依赖，例如需要存储数据到本地磁盘。
优点：可以存储数据，实现数据管理
缺点：集群环境下需要实现主从，数据同步，备份，水平扩容复杂。

# 资源和对象

       **Kubernetes 中的所有内容都被抽象为“资源”   **** ，如 Pod、Service、Node 等都是资源。**“对象”就是“资源”的实例**，是持久化的实体。如某个具体的 Pod、某个具体的 Node。Kubernetes 使用这些实体去表示整个集群的状态。

  对象的创建、删除、修改都是通过 “Kubernetes API”，也就是 “Api Server” 组件提供的 API 接口，这些是 RESTful 风格的 Api，与 k8s 的“万物皆对象”理念相符。命令行工具 “kubectl”，实际上也是调用 kubernetes api。
  K8s 中的资源类别有很多种，kubectl 可以通过配置文件来创建这些 “对象”，配置文件更像是描述对象“属性”的文件，配置文件格式可以是 “JSON” 或 “YAML”，常用 “YAML”。

# 资源分类：

集群资源分类(按适用性范围分类)：
**集群级别**：作用在集群之上，集群下所有资源都可以共享使用
**名称空间级别**：作用在命名空间之上，通常只能在该命名空间范围内使用
**元数据型**：对于资源的元数据描述，每一个资源都可以使用元空间的数据

集群级别

## 1.1集群级别的常用资源

**namespace名称空间**;

**node**：Node不像其他的资源（如Pod,Namespace）,Node本质上不是Kubernetes来创建的，Kubernetes只是管理Node上的资源。
虽然可以通过Mainfest创建一个Node对象，但Kubernetes也只是去检查是否真的是有这么一个Node,如果检查失败也不会往上调度Pod

**ClusterRole**：ClusterRole是一种特殊类型的角色，它允许对整个集群中的资源进行授权。与Role对象不同，ClusterRole对象是集群范围的。这意味着ClusterRole对象可以控制整个Kubernetes集群中的资源，而不仅仅是单个命名空间中的资源。ClusterRole对象用于授予用户、组或服务帐户对集群级别资源的访问权限。

**ClusterRoleBinding**：将集群角色绑定到主体(RBAC的主体（subject）: User：用户Group：用户组ServiceAccount：服务账号)

## 1.2命名空间级别的常用资源

### 1.2.1工作负载型

#### Pod

Pod是kubernetes中最小的资源管理组件，Pod也是最小化运行容器化应用的资源对象。一个Pod代表着集群中运行的一个进程。kubernetes中其他大多数组件都是围绕着Pod来进行支撑和扩展Pod功能的。一个Pod中运行一个容器。“每个Pod中一个容器” 的模式是最常见的用法，一个Pod中也可以同时封装几个需要紧密耦合互相协作的容器，它们之间共享资源（网络，文件系统等）

![](./img/Kubernetes-Pod组成.png)

Pod有一个**副本**概念，一个Pod可以被复制成多分，每一份可以被称为一个副本，这些副本除了一些描述性信息（Pod的名字，uid等）不一样以外，其他信息都是一样的，例如Pod的内部容器，容器数量，容器里面运行的应用等这些信息都是一样的，这些副本提供同样的功能。

Pod的**控制器**(资源控制器)通常包含一个名为replicas的属性。replicas属性指定了特定Pod的副本数量，当当前集群中该Pod的数量与该属性值不一致时，k8s会采取一些策略去使得当前状态满足配置的要求。

## 1.3 元数据型的常用资源

**Horizontal Pod Autoscaler（HPA）**：k8s可以通过HPA自动监测pod的负载情况，实现pod数量的自动调整，HPA可以很好的对容器实现动态的弹性伸缩功能。

**PodTemplate**：是用于创建pod的规范，并且包含在deployment、job和daemonset中，控制器通过PodTemplate信息来创建pod

**LimitRange**:  用于限制Pod中容器使用的资源量。它允许集群管理员在命名空间级别上设置容器资源的最大和最小值，以确保应用程序使用的资源量在可控范围内。LimitRange可以用于限制CPU、内存、存储和容器的资源数量等，以满足应用程序的需求，并确保集群的性能和可用性。

# 资源控制器

Kubernetes 中内建了很多controller (控制器),这些相当于一个状态机，用来控制Pod的具体状态和行为；

## 1、适用无状态服务资源控制器

**ReplicationController**：适用无状态服务，用来确保容器应用的副本数始终保持在用户定义的副本数，即如果有容器异常退出，会自动创建新的pod来替代；而异常多出来的容器也会自动回收

**ReplicaSet**：适用无状态服务，ReplicaSet跟ReplicationController没有本质的不同，只是名字不一样，并且ReplicaSet支持集合式的selector(也就是通过标签来控制对相应的Pod生效)。

**Deployment**：适用无状态服务
Deployment为Pod和ReplicaSet提供了一个声明式定义(declarative)方法，用来替代以前的ReplicationController来方便的管理应用。
应用场景包括： 

**定义Deployment来创建Pod和ReplicaSet**

 **滚动升级和回滚** 

**应用平滑扩容和缩容** 

**暂停和继续Deployment** 例如：单需要改多个配置时不是改一条升级一条，而是先暂停Deployment，改为所有配置之后再启动，Deployment再去做升级

**DaemonSet**：守护进程， 

**StatefulSet**：适用有状态服务，

 **Job/CronJob**：定时任务

## 2、适用有状态服务资源控制器

### StatefulSet

**StatefulSet**：StatefulSet是Kubernetes中用于管理有状态应用的一种控制器，它为Pod提供了稳定的、有序的、唯一的标识，确保在集群中正确地部署、扩展和更新有状态服务。

特点：

**稳定的持久化存储**
**稳定的网络标准**
**有序部署，有序扩展**
**有序收缩，有序删除**

**稳定的网络标识**： 使用Headless Service (相比普通Service只是将spec.clusterIP定义为None) 来维 护Pod网络身份，会为每个Pod分配一个数字编号并且按照编号顺序部署。还需要在StatefulSet添加 serviceName: “nginx”字段指定StatefulSet控制器要使用这个Headless Service。
稳定主要体现在主机名和Pod A记录：
• 主机名： <statefulset名称>- <编号>
• Pod DNS A记录： <statefulset名称-编号>. <service-name> . <namespace>.svc.cluster.local

**稳定的持久化存储**： StatefulSet的存储卷使用VolumeClaimTemplate创建，称为卷申请模板， 当StatefulSet使用 VolumeClaimTemplate创建一个PersistentVolume时，同样也会为每个Pod分配并创建一个编号的PVC， 每个PVC绑定对应的PV，从而保证每个Pod都拥有独立的存储。

PersistentVolume（PV）是群集中的一块存储，可以是NFS、iSCSI、本地存储等，由管理员配置或使用存储类动态配置
PersistentVolumeClaim（PVC）是一个持久化存储卷，在创建pod时可以定义PVC类型的存储卷，PVC可以用来访问各种类型的持久化存储，如本地存储、网络存储、云存储等，而不必关心这些资源的实际位置和类型，PVC的使用可以使应用程序更加灵活和可移植，同时也可以提高存储资源的利用率。
PVC和PV它们是一一对应的关系，PV如果被PVC绑定了，就不能被其他PVC使用了

## 守护进程DaemonSet

DaemonSet是Kubernetes中用于确保每个节点（或满足特定条件的节点）上都运行一个副本的控制器。它的主要用途包括部署系统级别的守护进程、日志收集代理、监控探针等需要在集群内每个节点上都有实例的应用场景。 DaemonSet确保每个目标节点上只运行一个由其管理的Pod实例。当有新节点加入集群时，DaemonSet会自动在新节点上创建对应的Pod；当节点被删除或从集群中移除时，其上的DaemonSet Pod也会相应地被清理。

**使用场景**
**系统级服务**：如网络插件（如Calico、Flannel）、存储驱动（如Flexvolume、CSI）、日志收集代理（如Fluentd、Logstash）等，这些服务通常需要在每个节点上独立运行。

**系统监控**：如Prometheus Node Exporter、Grafana Agent等，用于收集节点级的性能指标和事件数据。

**基础设施工具**：如SSH访问代理（如Teleport）、节点级安全扫描工具等，这些工具需要在每个节点上提供一致的服务。

# 服务发现(Service , Ingress)

## Service

Kubernetes Service定义了这样一种抽象：一个Pod的逻辑分组，一种可以访问它们的策略 —— 通常称为微服务。这一组Pod能够被Service访问到，通常是通过Label Selector，
通俗的讲：SVC负责检测Pod的状态信息，不会因pod的改动IP地址改变（因为关注的是标签），导致Nginx负载均衡影响
Service能够提供负载均衡的能力，但是在使用上有以下限制：
默认只提供 4 层负载均衡能力（IP+端口），而没有 7 层功能（主机名和域名），但有时我们可能需要更多的匹配规则来转发请求，这点上 4 层负载均衡是不支持的
后续可以通过Ingress方案，添加7层的能力

Service 在 K8s 中有以下四种类型
Clusterlp：默认类型，自动分配一个仅Cluster内部可以访问的虚拟IP
NodePort：在ClusterlP基础上为Service在每台机器上绑定一个端口，这样就可以通过<NodeIP>:NodePort 来访问该服务
LoadBalancer：在NodePort的基础上，借助 cloud provider 创建一个外部负载均衡器，并将请求转发到
<NodeIP>：NodePort
ExternalName：把集群外部的服务引入到集群内部来，在集群内部直接使用。没有任何类型代理被创建，这只有kubernetes1.7或更高版本的kube-dns才支持

## Ingress

Ingress是在Kubernetes集群中暴露HTTP和HTTPS服务的API对象。它为集群中的服务提供外部访问，并提供负载均衡、SSL终止和基于名称的虚拟主机等功能。Ingress通过将流量路由到不同的后端Service来实现这些功能。Ingress需要一个Ingress Controller来实现其规则，这个控制器可以是Kubernetes自带的或者是第三方提供的

简单来说，**Service用于在Kubernetes集群内部暴露服务，而Ingress则用于将服务暴露到集群外部，使得外部用户可以通过HTTP或HTTPS协议访问应用程序，相当于nginx的反向代理**

Kubernetes服务发现图:

![](./img/Kubernetes服务发现1.png)

Kubernetes网络访问：

![](./img/Kubernetes服务发现-网络访问.png)



# 存储

## Volume

volume(存储卷)是pod中能够被多个容器（一个pod可以有多个容器）访问的共享目录

**k8s的volume和docker的volume区别：**
Kubernete中的volume被定义在pod上，然后被一个pod的多个容器挂载到具体的文件目录下
Kubernetes中的volume和pod的生命周期相同，但与容器的生命周期不相关
即容器重启volume不会丢失
但是pod重启volume卷会丢失
注意：这里的volume卷丢失不代表volume对应的实际地址会丢失，而是spec.volumes的定义丢失。



## CSI

Kubernetes从1.9版本开始引入容器存储接口 Container Storage Interface (CSI)机制，用于在Kubernetes和外部存储系统之间建立一套标准的存储管理接口，通过该接口为容器提供存储服务。

CSI Controller的主要功能是 提供存储服务视角对存储资源和存储卷进行管理和操作。在Kubernetes中建议将其部署为单实例Pod，可以使用StatefulSet或 Deployment控制器进行部署，设置副本数量为1，保证一种存储插件只运行一个控制器实例。
CSI Node的主要功能是对主机（Node ）上的Volume进行管理和操作，在 Kubernetes中建议将其部署为DaemonSet，在需要提供存储资源的各个Node上都运行一个Pod。





# 配置

## ConfigMap

是一种api对象，用来非机密性的数据保存到键值对中。使用时，pod可以将其用作环境变量、命令行参数或存储中的配置文件。
ConfigMap并不提供加密或保密的功能。 如果你想存储的数据是机密的，请使用 Secret， 或者使用其他第三方工具来保证你的数据的私密性，而不是用 ConfigMap。
ConfigMap 在设计上不是用来保存大量数据的。在 ConfigMap 中保存的数据不可超过 1 MiB。如果需要保存超出此尺寸限制的数据，考虑挂载存储卷 或者使用独立的数据库或者文件服务



## Secret

Secret 是 Kubernetes 中用于存储敏感信息的对象。与 ConfigMap 类似，Secret 也可以存储配置信息，用于敏感数据，并提供了额外的安全特性来保护这些数据。

**使用场景**
存储凭证：数据库密码、API 密钥、OAuth 令牌。
TLS 证书：存储 HTTPS 服务器的私钥和证书。
配置加密数据：用于安全地管理任何敏感配置。

**优点**：
安全性：比 ConfigMap 提供更高的安全性，适合存储敏感数据。
灵活性：可以以环境变量或文件的形式注入到 Pod 中。
隔离：敏感数据与应用配置分离，降低泄露风险。
**缺点**：
加密限制：Secrets 在 etcd 中默认是以 Base64 编码存储，并非加密，需要依赖外部工具加密。
管理复杂性：需要适当的权限管理和策略来防止未授权访问。



## Downward API

Downward API 用于在容器中获取 POD 的基本信息，kubernetes原生支持

Downward API提供了两种方式用于将 POD 的信息注入到容器内部：
1.环境变量：用于单个变量，可以将 POD 信息直接注入容器内部。
2.Volume挂载：将 POD 信息生成为文件，直接挂载到容器内部中去。

Downward API支持字段：

```yaml
spec.nodeName - 宿主机名字
status.hostIP - 宿主机 IP
metadata.name - Pod 的名字
metadata.namespace - Pod 的 Namespace
status.podIP - Pod 的 IP
spec.serviceAccountName - Pod 的 Service Account 的名字
metadata.uid - Pod 的 UID
metadata.labels['<KEY>'] - 指定 <KEY> 的 Label 值
metadata.annotations['<KEY>'] - 指定 <KEY> 的 Annotation 值
metadata.labels - Pod 的所有 Label
metadata.annotations - Pod 的所有 Annotation
```
