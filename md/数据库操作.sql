
-- oracle
-- 查询表的索引
SELECT * FROM ALL_INDEXES WHERE TABLE_NAME = '表名';
-- 查看执行计划
EXPLAIN PLAN FOR SELECT * FROM 表名 WHERE 查询条件;
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);
-- 查看定时任务 字段：
-- JOB ： job 的唯一标识
-- SCHEMA_USER ： 该 job 所属的 Schema 用户
-- OWNER ：job 所属的 Oracle 用户
-- WHAT ： 该 job 启动的程序或 SQL 语句
-- NEXT_DATE ： 下一次启动时间
-- INTERVAL ： 执行间隔
select * from dba_jobs;
select * from all_jobs;
select * from user_jobs;
SELECT owner, job_name, job_action, schedule_name, repeat_interval, start_date, enabled, state FROM dba_scheduler_jobs;

-- 查看存储过程，其中NAME为存储过程名字，需要全部用大写英文。
SELECT * FROM user_source  WHERE NAME = 'P_CY_PRIZEPOOLUPDATER' ORDER BY line;


-- ————————————————————————————————————————————————————
-- 开启SQL跟踪
ALTER SESSION SET SQL_TRACE = TRUE;

-- 查询当前正在执行的SQL语句
SELECT sql_id, sql_text
FROM   v$sql
WHERE  sql_id IN (SELECT sql_id FROM v$session WHERE status = 'ACTIVE');
-- ————————————————————————————————————————————————————

-- mysql
-- 查看执行计划
SHOW INDEX FROM table_name;
-- 查询表的索引
EXPLAIN SELECT * FROM 表名 WHERE 查询条件;

-- SHOW PROCESSLIST 命令可以查看当前正在执行的SQL语句和相关的会话信息。通过该命令，我们可以实时监控MySQL中正在执行的SQL语句，以及其执行状态、执行时间等信息。
SHOW PROCESSLIST


