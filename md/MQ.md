# MQ的本质

MQ（Message Queue）消息队列，是基础数据结构中“先进先出”的一种数据结构。指把要传输的数据（消息）放在队列中，用队列机制来实现消息传递——生产者产生消息并把消息放入队列，然后由消费者去处理。消费者可以到指定队列拉取消息，或者订阅相应的队列，由MQ服务端给其推送消息

# MQ的作用

## 一、流量消峰

当一台服务器收到大量请求时，避免服务器被压垮，从而使用MQ进行流量消峰，分批处理大量的请求，确保系统的稳定性

**以稳定的系统资源应对突发的。**

## 二、异步解耦

上游系统对下游系统同步调用的话，降低系统的并发du和吞吐量，一般采用MQ的方式，使上游系统和下游系统进行异步调用，并且达到解耦的目的

**解耦之后可以实现数据分发，生产者发送一个消息之后，可以多个消费者来处理。**

## 三、数据收集

分布式数据会产生海量数据，如业务日志、监控数据、用户行为等，通过MQ进行数据采集

# MQ缺点

**一、系统可用性降低**

一但宕机，整个业务就会产生影响，需要使用高可用方案。

**二、系统复杂度提高**

引入MQ之后，数据链路就会变得很复杂，如何保证消息不丢失，消费不会重复消费，怎么保证消息的顺序性？

**三、数据一致性**

A系统发消息，需要有B、C两个系统处理，如果B系统处理成功，C系统处理失败，就会造成数据一致性的问题。

# MQ产品对比

## Kafka

优点：吞吐量非常大，性能非常好，集群高可用。

缺点：会丢失数据，功能比较单一。

使用场景：日志分析，大数据采集。

## RabbitMQ

优点：消息可靠性高，功能全面。

缺点：吞吐量比较低，消息累积会严重性能，erlang语言不好定制。

使用场景：小规模场景

## RocketMQ

优点：高吞吐，高性能，高可用，功能非常全面。

缺点：开源版功能不如云上商业版。官方文档和周边生态还不够成熟。客户端只支持java。

使用场景：几乎是全场景。

# RocketMQ事务消息

当我们在业务逻辑中发送消息时，消息与业务的事务之间难以保证一致性，如果业务代码出现异常，如果已发送的消息无法回滚，则很会出现数据不一致的情况，RocketMQ的事务消息支持在业务逻辑与发送消息之间提供事务保证，RocketMQ通过两阶段的方式提供事务消息的支持。

**事务消息**：MQ 提供类似 X/Open XA 的分布事务功能，通过 MQ 事务消息能达到分布式事务的最终一致。
**半消息**：暂不能投递的消息，发送方已经将消息成功发送到了 MQ 服务端，但是服务端未收到生产者对该消息的二次确认，此时该消息被标记成“暂不能投递”状态，处于该种状态下的消息即半消息。
**半消息回查**：由于网络闪断、生产者应用重启等原因，导致某条事务消息的二次确认丢失，MQ 服务端通过扫描发现某条消息长期处于“半消息”时，需要主动向消息生产者询问该消息的最终状态（Commit 或是 Rollback），该过程即消息回查。

**流程：**
1.发送方向 MQ 服务端发送事务消息；
2.MQ Server 将消息持久化成功之后，向发送方 ACK 确认消息已经发送成功，此时消息为半消息。
3.发送方开始执行本地事务逻辑。
4.发送方根据本地事务执行结果向 MQ Server 提交二次确认（Commit 或是 Rollback），MQ Server 收到 Commit 状态则将半消息标记为可投递，订阅方最终将收到该消息；MQ Server 收到 Rollback 状态则删除半消息，订阅方将不会接受该消息。
5.在断网或者是应用重启的特殊情况下，上述步骤4提交的二次确认最终未到达 MQ Server，经过固定时间后 MQ Server 将对该消息发起消息回查。
6.发送方收到消息回查后，需要检查对应消息的本地事务执行的最终结果。
7.发送方根据检查得到的本地事务的最终状态再次提交二次确认，MQ Server 仍按照步骤4对半消息进行操作。

# 当RocketMQ中消息堆积时，可以采取以下几种方式来处理：

1. **扩容消费者：**
   增加消费者数量可以提高消息的消费速度，从而减少消息堆积。您可以根据实际情况增加消费者的数量，确保消费者能够及时处理消息。

2. **调整消费者配置：**
   检查消费者的配置参数，如消费线程数、消费批量大小等，确保其能够充分利用系统资源，提高消息的消费效率。

3. **优化消息生产者：**
   检查消息生产者的配置参数，如发送超时时间、发送重试次数等，确保消息能够及时发送到RocketMQ服务器。

4. **调整消息队列数量：**
   根据消息堆积的情况，可以考虑增加消息队列的数量。通过增加消息队列，可以提高消息的并发处理能力，加快消息的消费速度。

5. **增加消息消费的并发度：**
   如果消费者的处理逻辑允许并行处理消息，可以增加消息消费的并发度。通过增加并发度，可以提高消息的处理速度，减少消息堆积。

6. **监控和报警：**
   设置监控系统，实时监控RocketMQ的消息堆积情况。当消息堆积超过一定阈值时，触发报警机制，及时采取相应的处理措施。

7. **扩容RocketMQ集群：**
   如果以上方法无法解决消息堆积问题，可以考虑扩容RocketMQ集群，增加消息的处理能力和存储容量。

总之，处理RocketMQ消息堆积需要综合考虑消费者、生产者、队列、并发度等多个因素，并根据实际情况采取相应的优化和调整措施。

# RocketMQ

## 1、多消费者多NamesrvAddr

rocketmq同一个项目，不同消费者订阅不同NamesrvAddr的Mq消息时需要设置InstanceName，

否则在初始化第一个消费者后，会一直使用第一个消费者的配置而不是使用后续初始化消费者的配置

consumer.setInstanceName("mcsspacelogConsumer");

源码跟踪：

从consumer.start();开始

​ org.apache.rocketmq.client.impl.consumer.DefaultMQPushConsumerImpl.start();

​ start方法里mQClientFactory的初始化是关键：

​ this.mQClientFactory = MQClientManager.getInstance().getOrCreateMQClientInstance(this.defaultMQPushConsumer, this.rpcHook);

​ 方法中：

​ String clientId = clientConfig.buildMQClientId(); // #这个如果不设置InstanceName得到的clientId就是一样的值

MQClientInstance instance = this.factoryTable.get(clientId); // #这里就会拿到上一个消费者创建好的实例，导致消费者创建错误

```java
    // clientConfig.buildMQClientId 方法
    public String buildMQClientId() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClientIP());

        sb.append("@");
        sb.append(this.getInstanceName()); // 不设置则取默认值
        if (!UtilAll.isBlank(this.unitName)) {
            sb.append("@");
            sb.append(this.unitName);
        }

        return sb.toString();
    }
```

## 2、MessageListener消息监听

内部消息监听，每次消费的数据量是1

当MyMessageListener实现的是rocketmq的MessageListenerConcurrently（**并发消费**）接口或者MessageListenerOrderly（**有序消费**）接口时，方法consumeMessage中参数List list里只有一条数据

```java
public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
}
```

原因如下：以MessageListenerConcurrently为例，MessageListenerOrderly同理

消费者DefaultMQPushConsumer启动时会在start方法中判断：

```java
// org.apache.rocketmq.client.impl.consumer.DefaultMQPushConsumerImpl.start()方法
public synchronized void start() throws MQClientException {
    // 以上省略.....................
    if (this.getMessageListenerInner() instanceof MessageListenerOrderly) {
        this.consumeOrderly = true;
        // 创建对应的service
        this.consumeMessageService =
            new ConsumeMessageOrderlyService(this, 
                                 (MessageListenerOrderly) this.getMessageListenerInner());
    } else if (this.getMessageListenerInner() instanceof MessageListenerConcurrently) {
        this.consumeOrderly = false;
        // 创建对应的service
        this.consumeMessageService =
            new ConsumeMessageConcurrentlyService(this, 
                                 (MessageListenerConcurrently) this.getMessageListenerInner());
    }
    // 以下省略.....................           
}
//--------------------------------------------------------------
// MyMessageListener中的consumeMessage()方法由ConsumeMessageConcurrentlyService的consumeMessageDirectly()方法调用，该方法只会添加一条消息到list中

    @Override
    public ConsumeMessageDirectlyResult consumeMessageDirectly(MessageExt msg, String brokerName) {

        // 以上省略.....................
        List<MessageExt> msgs = new ArrayList<MessageExt>();
        msgs.add(msg);
        MessageQueue mq = new MessageQueue();
        mq.setBrokerName(brokerName);
        mq.setTopic(msg.getTopic());
        mq.setQueueId(msg.getQueueId());
        // ..............
        ConsumeConcurrentlyStatus status = this.messageListener.consumeMessage(msgs, context);

        //以下省略..................... 
        return result;
    }
```

## 3、 验证RocketMQ功能

RocketMQ自带了发送与接收消息的脚本tools.cmd，用来验证RocketMQ的功能是否正常。
tool.cmd脚本需要带参数执行，无法用简单的双击方式启动。
打开cmd窗口并跳转到bin目录下 **启动消费者:** 与mqbroker.cmd脚本类似，启动tool.cmd命令之前我们要指定NameServer地址。
这里我们采用命令方式指定，并启动消费者。依次执行如下命令：

```shell
## 先设置 NAMESRV_ADDR
set "NAMESRV_ADDR=localhost:9876"
## 启动消费者
tools.cmd org.apache.rocketmq.example.quickstart.Consumer-Drocketmq.client.logRoot=E:/logs/Rocketmq/ -Drocketmq.client.logLevel=warn
```

**启动生产者**

再打开一个cmd窗口，依次执行如下命令：

```shell
## 先设置 NAMESRV_ADDR
set "NAMESRV_ADDR=localhost:9876"
## 启动生产者
tools.cmd org.apache.rocketmq.example.quickstart.Producer -Drocketmq.client.logRoot=E:/logs/Rocketmq/ -Drocketmq.client.logLevel=warn
```

同时消费者能消费到消息

RocketMQ功能验证成功。

## 4、RocketMQ修改存储文件位置

RocketMQ默认会将消息，日志这个文件存储在C盘的${user.home}/store中

可以将内容复制到其他文件夹内如：E:/data/store

进入的RocketMQ的conf文件夹，修改broker.conf为如下内容

```editorconfig
brokerClusterName = DefaultCluster
brokerName = broker-a
brokerId = 0
deleteWhen = 04
fileReservedTime = 48
brokerRole = ASYNC_MASTER
flushDiskType = ASYNC_FLUSH
storePathRootDir=E:/data/store
#commitLog存储路径
storePathCommitLog=E:/data/store/commitlog
#消费队列存储路径
storePathConsumeQueue=E:/data/store/consumequeue
#消息索引存储路径
storePathIndex=E:/data/store/index
#checkpoint 文件存储路径
storeCheckpoint=E:/data/store/checkpoint
#abort 文件存储路径
abortFile=E:/data/store/abort
```

进入RocketMQ的bin文件通过命令启动mqbroker并指定配置文件

```shell
start mqbroker.cmd -n 127.0.0.1:9876 -c ../conf/broker.conf autoCreateTopicEnable=true
```

## 5、RocketMQ管理命令mqadmin

**查看命令如何使用：**

sh mqadmin help 命令名称

**查看topic消息：** sh mqadmin consumeMessage -n 192.168.221.210:9876 -t vsbo **查看topic列表：** sh mqadmin topicList -n 192.168.221.210:9876 **查看查consumer的信息：** sh mqadmin consumerConnection -n 192.168.221.210:9876 -g campaign_subscribe
sh mqadmin consumerConnection -n 10.17.49.28:9876 -g member-task-consumer **发送MQ消息：** sh mqadmin sendMessage -t vsbo -c subscribe -n 10.17.49.28:9876 -p '{"traceID":"ef651de3d6e948dda48ca77de5d3d6e0","oprTime":"20220922125142508","oprEndTime":"20220922125142800","serviceName":"createSubscribeRelation","intfName":"createSubscribeRelation","reqMsgSize":470,"resultCode":"0","account":"18777427433","ordUserID":"16608193888","productID":"Honour_Marketing22070701","productName":"钻石畅享会员","originalPrice":"0","discount":"100","chargeType":0,"channelID":"13","accessInfo":{},"extInfo":{"orderID":"800104048386_02940715-1867-4f0e-a6fb-02f265bfc057_CloudSpaceContractSubscribeOrder","thirdOrderID":""},"envID":"100001"}'
