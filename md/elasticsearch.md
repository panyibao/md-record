# elasticsearch记录

# 1、cat查询es信息

get方式查询es的一些信息/cat/allocation

```apl
/_cat/allocation      #查看单节点的shard分配整体情况
/_cat/shards          #查看各shard的详细情况
/_cat/shards/{index}  #查看指定分片的详细情况
/_cat/master          #查看master节点信息
/_cat/nodes           #查看所有节点信息
/_cat/indices         #查看集群中所有index的详细信息
/_cat/indices/{index} #查看集群中指定index的详细信息
/_cat/segments        #查看各index的segment详细信息,包括segment名, 所属shard, 内存(磁盘)占用大小, 是否刷盘
/_cat/segments/{index}#查看指定index的segment详细信息
/_cat/count           #查看当前集群的doc数量
/_cat/count/{index}   #查看指定索引的doc数量
/_cat/recovery        #查看集群内每个shard的recovery过程.调整replica。
/_cat/recovery/{index}#查看指定索引shard的recovery过程
/_cat/health          #查看集群当前状态：红、黄、绿
/_cat/pending_tasks   #查看当前集群的pending task
/_cat/aliases         #查看集群中所有alias信息,路由配置等
/_cat/aliases/{alias} #查看指定索引的alias信息
/_cat/thread_pool     #查看集群各节点内部不同类型的threadpool的统计信息,
/_cat/plugins         #查看集群各个节点上的plugin信息
/_cat/fielddata       #查看当前集群各个节点的fielddata内存使用情况
/_cat/fielddata/{fields}     #查看指定field的内存使用情况,里面传field属性对应的值
/_cat/nodeattrs              #查看单节点的自定义属性
/_cat/repositories           #输出集群中注册快照存储库
/_cat/templates              #输出当前正在存在的模板信息
```

**查询所有数据**

```http
GET _search
{
  "query": {
    "match_all": {}
  }
}
```

**响应说明**

```json
{
    "took" : 346,          // 整个检索消耗的时间, 单位是毫秒. 包括线程池中的等待时间、集群中分布式搜索+收集结果的时间
    "timed_out" : false,   // 默认不启用超时机制, 若启用, 需要设置具体的时间值
    "_shards" : {          // 搜索用到的shard数, 以及成功/失败的shard数
        "total" : 5,
        "successful" : 5,
        "skipped" : 0,
        "failed" : 0       // 一个Shard的Primary和Replicas都挂掉, 它才被认为失败
    },
    "hits" : {
        "total" : 10,      // 本次搜索命中(搜索到)的结果总数
        "max_score" : 1.0, // 本次搜索的所有结果中, 最大的相关度分数
        "hits" : [         // 默认显示查询结果中的前10条记录, 根据_score降序排序
            {
                "_index" : "book_shop",
                "_type" : "books",
                "_id" : "2",
                "_score" : 1.0,  // 相关度得分, 越相关, 分值越大, 排位越靠前
                "_source" : {
                    "name" : "Java编程思想",
                    "category" : "编程语言",
                    "author" : "Bruce Eckel",
                    "price" : 105.0,
                    "publisher" : "机械工业出版社",
                    "date" : "2016-01-01"
                }
            } 
        ]
    }
}
```

# 2、新增修改数据

1、POST即能新增数据也能更新数据
2、PUT也可以新增数据和更新数据

POST 新增。如果不指定id，会自动生成 id。指定 id 就会修改这个数据，并新增版本号；
PUT 可以新增也可以修改。PUT 必须指定 id；由于 PUT 需要指定 id，一般都用来做修改；

索引下的类型（type）ES7已废弃，但还可以用，ES8才真正的去掉了type

```http
## 保存一个数据，保存在哪个索引的哪个类型下，指定用哪个唯一标识
PUT customer/external/1
{
    "name":"John Doe"
} 
```

**响应说明**

```json
{
    "_index": "customer",  // 表明该数据在哪个索引（数据库）下；
    "_type": "external",   // 哪些类型下（表）
    "_id": "1",            // 保存数据的id是几
    "_version": 1,         // 保存数据的版本
    "result": "created",   // 保存的结果，created为新建，同样的请求发送多次，后面的均为更新updated，看下图
    "_shards": {           // 分片信息
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "_seq_no": 0,
    "_primary_term": 1
}
```

post + /_update，请求体用{"doc":}包装起来，同样可实现更新操作

```http
# POST + _update 请求体需要使用{"doc":}包装起来
POST customer/external/1/_update
{
    "doc":{
        "name":"John Doe1"
    }
}
```

**加不加update有的区别：**
加了update，除了请求体需要用{"doc":}包起来，在更新内容不变的情况下，version和_seq_no是不变的

# 3、删除文档

```http
# 删除文档
DELETE customer/external/1
```

```http
# 删除索引
DELETE customer
```

# 4、批量API

POST customer/external/_bulk

必须是post，两行为一个整体，语法格式

```json
{"index":{"_id":"1"}}//index索引一条数据，id为1
{"name":"Jone Doe"}//index索引数据的内容
{"index":{"_id":"2"}}
{"name":"Jane Doe"}
```

```http
POST customer/external/_bulk
{"index":{"_id":"1"}}
{"name":"Jone Doe"}
{"index":{"_id":"2"}}
{"name":"Jane Doe2"}
```

```http
POST /_bulk 没有指定任何索引的任何类型，说明是要对整个es批量操作
{"delete":{"_index":"website","_type":"blog","_id":"123"}}删除哪个索引哪个类型下id为哪个的数据，删除没有请求体，请求信息都在元数据中　　
{"create":{"_index":"website","_type":"blog","_id":"123"}}创建操作
{"title":"My first blog post"}创建操作的内容
{"index":{"_index":"website","_type":"blog"}}保存记录
{"title":"my second blog post"}保存的内容
{"update":{"_index":"website","_type":"blog","_id":"123"}}更新记录
{"doc":{"title":"My updated blog post"}}更新的内容
```

# 5、查询数据

```http
# customer/external/1 ： 索引/类型/id
GET customer/external/1
{
  "name":"John Doe"
} 
```
