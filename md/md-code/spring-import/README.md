
spring-import使用@Import，导入spring-import-source模块的类到spring容器中
spring-import-source模块没有spring ，
提供类：

com.pan.service.source.OrderService

com.pan.service.source.UserServic




**@Import注解提供了三种用法**

1、@Import一个普通类 spring会将该类加载到spring容器中

2、@Import一个类，该类实现了ImportBeanDefinitionRegistrar接口，在重写的registerBeanDefinitions方法里面，能拿到BeanDefinitionRegistry bd的注册器，能手工往beanDefinitionMap中注册 beanDefinition

3、@Import一个类 该类实现了ImportSelector 重写selectImports方法该方法返回了String[]数组的对象，数组里面的类都会注入到spring容器当中


ImportBeanDefinitionRegistrar例子：
通过@Import注解引入MyImportBeanDefinitionRegistrar类，通过添加BeanDefinition的方式将外部类添加到spring容器中。

**FactoryBean**：InterfaceFactoryBean 通过FactoryBean方式+代理的方式，实现本模块定义接口，外部模块实现。类似于mybatis的mapper接口


**ClassPathBeanDefinitionScanner**
public class InfScanner extends ClassPathBeanDefinitionScanner
InfScanner继承ClassPathBeanDefinitionScanner扫描指定路径下的接口，生成BeanDefinition