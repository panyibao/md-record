package com.pan.timport.registrar;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;

import java.io.IOException;
import java.util.Set;

/**
 *
 * 接口扫描器
 * @author panyibao
 */
public class InfScanner extends ClassPathBeanDefinitionScanner {
    public InfScanner(BeanDefinitionRegistry registry) {
        super(registry);
    }

    @Override
    protected boolean isCandidateComponent(MetadataReader metadataReader) throws IOException {
        // 该方法是用来判断是否符合配置了TypeFilter参数,这里为自定义扫描，设置为true
        return true;
    }

    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
        // isCandidateComponent完成一些基本检查：检查当前类不是抽象类、不是接口，如果是抽象类，需要指定lookup方法。
        // 这里自定义的就是要扫描接口
        AnnotationMetadata metadata = beanDefinition.getMetadata();
        return metadata.isInterface();
    }

    @Override
    protected Set<BeanDefinitionHolder> doScan(String... basePackages) {
        Set<BeanDefinitionHolder> beanDefinitionHolders = super.doScan(basePackages);
        for (BeanDefinitionHolder beanDefinitionHolder : beanDefinitionHolders) {
            BeanDefinition beanDefinition = beanDefinitionHolder.getBeanDefinition();

            // 设置构造参数
            beanDefinition.getConstructorArgumentValues().addGenericArgumentValue(beanDefinition.getBeanClassName());
            // 指定自定义的FactoryBean：InterfaceFactoryBean进行创建
            beanDefinition.setBeanClassName(InterfaceFactoryBean.class.getName());
        }
        return beanDefinitionHolders;
    }
}
