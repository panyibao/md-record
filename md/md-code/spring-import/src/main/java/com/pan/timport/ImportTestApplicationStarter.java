package com.pan.timport;

import com.pan.service.source.OrderService;
import com.pan.service.source.UserService;
import com.pan.timport.registrar.InfMapperScan;
import com.pan.timport.registrar.MyImportBeanDefinitionRegistrar;
import com.pan.timport.service.TestService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author panyibao
 */
@ComponentScan(basePackages = "com.pan.timport")
@InfMapperScan("com.pan.timport.registrar.inf")
// @Import({MyImportBeanDefinitionRegistrar.class, UserService.class, OrderService.class})
public class ImportTestApplicationStarter {
    public static void main(String[] args) {
        // spring解析启动类ImportTestApplicationStarter时，会解析@InfMapperScan，发现有@Import为spring的注解
        // 这时会把注解解析到AnnotationMetadata importingClassMetadata，包含@InfMapperScan的信息也在里面
        // AnnotationMetadata importingClassMetadata这个在MyImportBeanDefinitionRegistrar有用到
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(ImportTestApplicationStarter.class);
        TestService testService = (TestService) applicationContext.getBean("testService");
        testService.info();
        testService.testJump();
        testService.testRun();
        testService.testTall();
    }
}
