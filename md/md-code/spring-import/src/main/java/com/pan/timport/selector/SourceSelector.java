package com.pan.timport.selector;

import com.pan.service.source.OrderService;
import com.pan.service.source.UserService;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.Order;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author panyibao
 */
public class SourceSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        System.out.println(" SourceSelector selectImports ");
        return new String[]{OrderService.class.getName()};
    }
}
