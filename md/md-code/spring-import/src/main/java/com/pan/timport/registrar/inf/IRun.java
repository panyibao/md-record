package com.pan.timport.registrar.inf;

import com.pan.service.source.annotation.InfParam;

/**
 * @author panyibao
 */
public interface IRun {
    @InfParam(paramName = "timport", paramValue = "run")
    public void run();
}
