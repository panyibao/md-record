package com.pan.timport.service;

import com.pan.service.source.OrderService;
import com.pan.service.source.UserService;
import com.pan.timport.registrar.inf.IJump;
import com.pan.timport.registrar.inf.IRun;
import com.pan.timport.registrar.inf.ITall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author panyibao
 */
@Service
public class TestService {
    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private IJump jump;

    @Autowired
    private IRun run;

    @Autowired
    private ITall tall;

    public void info(){
        String type = "test";
        System.out.println(userService.getInfo(type));
        System.out.println(orderService.getInfo(type));
    }

    public void testJump(){
        System.out.println("timport testJump");
        jump.jump();
    }

    public void testRun(){
        System.out.println("timport testRun");
        run.run();
    }

    public void testTall(){
        System.out.println("timport testTall");
        tall.tall();
    }
}
