package com.pan.timport.registrar;

import com.pan.service.source.InterfaceProxyFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * @author panyibao
 */
public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // 创建外部代理工厂bean的beanDefinition
        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition.setBeanClass(InterfaceProxyFactory.class);
        registry.registerBeanDefinition("interfaceProxyFactory", beanDefinition);

        /*
        // 这里IJump.class, IRun.class写死了接口
        // 可以通过注解+自定义scanner扫描指定包里的接口，并生成对应的BeanDefinition
        Stream.of(IJump.class, IRun.class).forEach(
                aClass -> {
                    AbstractBeanDefinition beanDefinition1 = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
                    beanDefinition1.setBeanClass(InterfaceFactoryBean.class);
                    beanDefinition1.getConstructorArgumentValues().addGenericArgumentValue(aClass);
                    registry.registerBeanDefinition(aClass.getSimpleName(), beanDefinition1);
                }
        );
        */
        // 使用scanner后新增的接口就可以被扫描到自动生成BeanDefinition
        // 获取接口代理包扫描配置
        Map<String, Object> annotationAttributes = importingClassMetadata.getAnnotationAttributes(InfMapperScan.class.getName());
        String  scanInfPath = (String) annotationAttributes.get("value");
        // 使用自定义的scanner扫描内部需要代理的接口,并生成对应的BeanDefinition
        InfScanner infScanner = new InfScanner(registry);
        infScanner.scan(scanInfPath);
    }
}
