package com.pan.timport.registrar;

import com.pan.service.source.InterfaceProxyFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author panyibao
 */
public class InterfaceFactoryBean implements FactoryBean {

    @Autowired
    private InterfaceProxyFactory interfaceProxyFactory;

    private Class aClass;

    public InterfaceFactoryBean(Class aClass){
        this.aClass = aClass;
    }

    @Override
    public Object getObject() throws Exception {
        return interfaceProxyFactory.newInstance(aClass);
    }

    @Override
    public Class<?> getObjectType() {
        return aClass;
    }
}
