package com.pan.timport.registrar;

import com.pan.service.source.OrderService;
import com.pan.service.source.UserService;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author panyibao
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({MyImportBeanDefinitionRegistrar.class, UserService.class, OrderService.class})
@Documented
public @interface InfMapperScan {
    String value();
}
