package com.pan.timport.registrar.inf;

import com.pan.service.source.annotation.InfParam;

/**
 * @author panyibao
 */
public interface IJump {

    @InfParam(paramName = "timport", paramValue = "jump")
    public void jump();
}
