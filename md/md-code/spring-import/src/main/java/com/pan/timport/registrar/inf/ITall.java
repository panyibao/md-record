package com.pan.timport.registrar.inf;

import com.pan.service.source.annotation.InfParam;

public interface ITall {

    @InfParam(paramName = "timport", paramValue = "run")
    public void tall();
}
