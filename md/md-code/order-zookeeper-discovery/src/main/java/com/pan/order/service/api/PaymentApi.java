package com.pan.order.service.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(contextId = "paymentApi", name = "payment-zookeeper-discovery", fallback = PaymentApiFallback.class)
public interface PaymentApi {

    @GetMapping("/payment/info")
    public String info();

}
