package com.pan.order.controller;

import com.pan.order.service.api.PaymentApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单模块接口
 * @author panybiao
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {
    public PaymentApi paymentApi;

    public OrderController(PaymentApi paymentApi){
        this.paymentApi = paymentApi;
    }

    /**
     * 获取信息
     * @return String
     */
    @GetMapping("/info")
    public String info(){
        return "order result; " + paymentApi.info();
    }
}
