package com.pan.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * zookeeper注册中心订单模块
 * @author panyibao
 */
@EnableFeignClients
@SpringBootApplication
public class ZKOrderApplicationStarter {
    public static void main(String[] args) {
        SpringApplication.run(ZKOrderApplicationStarter.class, args);
    }
}
