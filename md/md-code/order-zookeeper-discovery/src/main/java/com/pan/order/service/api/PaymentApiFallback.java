package com.pan.order.service.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * PaymentApi降级实现
 * @author panybiao
 */
@Slf4j
@Component
public class PaymentApiFallback implements PaymentApi {

    @Override
    public String info() {
        return "PaymentApiFallback info";
    }
}
