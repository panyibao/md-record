package com.pan;

import com.pan.sharding.ShardingApplicationStarter;
import com.pan.sharding.entity.TestUser;
import com.pan.sharding.service.TestUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingApplicationStarter.class)
public class AppTest {

    @Autowired
    private TestUserService testUserService;

    /**
     * 测试sharding添加数据
     * 使用master
     */
    @Test
    public void shardingAdd(){
        TestUser testUser = new TestUser();
        testUser.setId(5);
        testUser.setName("aaa");
        testUserService.add(testUser);
    }

    /**
     * 测试sharding查询数据
     * 使用slave0
     */
    @Test
    public void shardingFind(){
        TestUser testUser = testUserService.find(1);
        System.out.println("==========" + (testUser == null ? "null" : testUser.toString()));
    }

    /**
     * 测试sharding 添加后查询，无事务
     */
    @Test
    public void shardingAddFind(){
        TestUser user = new TestUser();
        user.setId(6);
        user.setName("aaa");
        TestUser testUser = testUserService.addNoTransactional(user);
        System.out.println("==========" + (testUser == null ? "null" : testUser.toString()));
    }

    /**
     * 测试sharding 添加后查询，有事务
     */
    @Test
    public void shardingAddFindTransactional(){
        TestUser user = new TestUser();
        user.setId(7);
        user.setName("aaa");
        TestUser testUser = testUserService.addTransactional(user);
        System.out.println("==========" + (testUser == null ? "null" : testUser.toString()));
    }

}
