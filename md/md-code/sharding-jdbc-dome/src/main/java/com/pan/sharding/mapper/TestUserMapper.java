package com.pan.sharding.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pan.sharding.entity.TestUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 读写分离测试mapper
 * @author panyibao
 */
@Mapper
public interface TestUserMapper extends BaseMapper<TestUser> {
}
