package com.pan.sharding.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.sharding.entity.TestUser;
import com.pan.sharding.mapper.TestUserMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * sharding读写分离测试服务
 * @author panyibao
 */
@Service
public class TestUserService extends ServiceImpl<TestUserMapper, TestUser> {

    /**
     * sharding自动判断使用master数据源
     * @param user TestUser
     */
    public void add(TestUser user){
        getBaseMapper().insert(user);
    }

    /**
     * sharding自动判断使用slave0数据源
     *
     * 当处于事务中时，即便只是读数据也使用主库
     * @param i long
     */
    // @Transactional
    public TestUser find(long i) {
        return getBaseMapper().selectById(i);
    }

    /**
     * sharding自动判断
     * add使用master数据源
     * find使用slave0数据源
     * @param user TestUser
     */
    public TestUser addNoTransactional(TestUser user) {
        add(user);
        return find(user.getId());
    }

    /**
     * 为了保证主从库间的事务一致性，避免跨服务的分布式事务，ShardingSphere-JDBC的 主从模型中 事务中的数据读写均用主库
     * sharding自动判断
     * add使用master数据源
     * find也使用master数据源
     * @param user TestUser
     */
    @Transactional
    public TestUser addTransactional(TestUser user) {
        add(user);
        return find(user.getId());
    }
}
