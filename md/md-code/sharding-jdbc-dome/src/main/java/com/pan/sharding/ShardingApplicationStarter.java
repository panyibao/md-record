package com.pan.sharding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author panyibao
 */
@SpringBootApplication
public class ShardingApplicationStarter {
    public static void main( String[] args ) {
        SpringApplication.run(ShardingApplicationStarter.class, args);
    }
}
