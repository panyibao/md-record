package com.pan.study.rocketMq.controller;

import com.pan.study.rocketMq.producer.TestProducer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author panyibao
 */
@RestController
public class TestController {

    @Resource
    private TestProducer testProducer;

    @GetMapping("/sendMsg")
    public String sendMsg(){
        testProducer.sendMqMessage("test msg");
        return "success";
    }
}
