package com.pan.study.rocketMq.consume;

/**
 *
 * @author panyibao
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 消费顺序消息
 * 配置RocketMQ监听
 * ConsumeMode.ORDERLY:顺序消费
 * 也可以通过实现MessageListenerOrderly接口实现有序消费
 * @author qzz
 */
@Slf4j
@Service
@RocketMQMessageListener(consumerGroup = "test",topic = "test-topic-orderly",consumeMode = ConsumeMode.ORDERLY)
public class TestConsume implements RocketMQListener<String> {
    @Override
    public void onMessage(String message) {
        log.info("consumer 顺序消费:" + message);
    }
}
/*
class TestConsume2 implements MessageListenerOrderly {

    @Override
    public ConsumeOrderlyStatus consumeMessage(List<MessageExt> list, ConsumeOrderlyContext consumeOrderlyContext) {
        return null;
    }
}*/
