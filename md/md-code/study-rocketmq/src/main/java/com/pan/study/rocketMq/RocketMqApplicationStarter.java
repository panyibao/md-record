package com.pan.study.rocketMq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author panyibao
 */
@SpringBootApplication
public class RocketMqApplicationStarter {
    public static void main(String[] args) {
        SpringApplication.run(RocketMqApplicationStarter.class, args);
    }
}
