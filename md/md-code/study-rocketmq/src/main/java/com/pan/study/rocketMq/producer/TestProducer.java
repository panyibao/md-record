package com.pan.study.rocketMq.producer;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author panyibao
 */
@Component
public class TestProducer {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * syncSendOrderly，发送同步顺序消息；
     * asyncSendOrderly，发送异步顺序消息；
     * sendOneWayOrderly，发送单向顺序消息；
     * 一般我们用syncSendOrderly方法发送同步顺序消息。
     * @param msg
     */
    public void sendMqMessage(String msg){
        //参数一：topic   如果想添加tag,可以使用"topic:tag"的写法
        //参数二：消息内容
        //参数三：hashKey 用来计算决定消息发送到哪个消息队列， 一般是订单ID，产品ID等
        rocketMQTemplate.syncSendOrderly("test-topic-orderly",msg,"111111");
        // rocketMQTemplate.syncSendOrderly 内部会使用一个SelectMessageQueueByHash的MessageQueueSelector
        // 对hashKey"111111"进行计算，再根据topic队列数进行取模运算，hashKey一样就能发送到同一个队列，从而保证消息的顺序性

    }
}
