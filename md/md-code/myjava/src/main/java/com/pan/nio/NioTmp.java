package com.pan.nio;

import javax.imageio.IIOException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

/**
 * NIO 使用
 * FileChannel：文件通道FileChannel是用于读取，写入，文件的通道。FileChannel只能被InputStream、OutputStream、RandomAccessFile创建。使用fisChannel.transferTo()可以极大的提高文件的复制效率，他们读和写直接建立了通道，还能有效的避免文件过大导致内存溢出。
 * java.nio.channels.FileChannel (抽象类)：用于读、写文件的通道。
 *
 * ServerSocketChannel：
 * Java NIO中的 ServerSocketChannel 是一个可以监听新进来的TCP连接的通道, 就像标准IO中的ServerSocket一样。ServerSocketChannel类在 java.nio.channels包中。
 *
 *
 * @author panyibao
 */
public class NioTmp {
    public static void main(String[] args) {
        try{
            // 打开通道（）
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            // 绑定地址和端口
            serverSocketChannel.socket().bind(new InetSocketAddress(9000));
            // 设置为非阻塞
            serverSocketChannel.configureBlocking(false);
            // 打开Selector用于处理Channel事件，对服务器来说是创建epoll
            Selector selector = Selector.open();
            // 将Channel注册到Selector，指定事件为OP_ACCEPT事件
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("启动服务成功");

            while (true) {
                // 阻塞等待需要处理的事件发生，事件发生后跳出阻塞，执行下面的代码
                selector.select();

                // 获取selector中注册的全部事件的SelectionKey
                // 这里为上面注册的SelectionKey.OP_ACCEPT，和下面注册的
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();

                // 遍历selectionKeys处理
                while (iterator.hasNext()){
                    SelectionKey selectionKey = iterator.next();

                    // 如果是OP_ACCEPT事件，进行连接获取和事件注册
                    if (selectionKey.isAcceptable()){
                        // 获取注册的渠道
                        ServerSocketChannel channel = (ServerSocketChannel) selectionKey.channel();
                        // 进行建立连接
                        SocketChannel socketChannel = channel.accept();
                        // 设置成非阻塞
                        socketChannel.configureBlocking(false);
                        // 将链接后的Channel注册到Selector，处理读事件
                        socketChannel.register(selector, SelectionKey.OP_READ);

                        System.out.println("客户端链接成功");
                    } else if (selectionKey.isReadable()){
                        // 获取注册的渠道
                        SocketChannel socketChannel = (SocketChannel) selectionKey.channel();

                        // 使用java.nio.ByteBuffer类中的方法allocate()分配新的ByteBuffer。
                        // 该方法需要一个参数，即缓冲区的容量。它返回分配的新的ByteBuffer。
                        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
                        int len = socketChannel.read(byteBuffer);
                        if (len > 0) {
                            System.out.println("read data: " + new String(byteBuffer.array()));
                        }else if (len == -1){
                            System.out.println("客户端断开连接");
                            socketChannel.close();
                        }
                    }
                    // 从集合里删除本次处理的key，防止重复处理
                    iterator.remove();
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }

    }
}
