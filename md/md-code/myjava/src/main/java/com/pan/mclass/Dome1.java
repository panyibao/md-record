package com.pan.mclass;

/**
 * @author panyibao
 * java 类加载初始化分析
 */
public class Dome1 {
    public static void main(String[] args) {
        // 可以看出两个DomeTest类的静态代码块和public static int value = 1;互换了位置
        // 但是执行仍然是从上往下执行
        // 相当于声明语句提前并赋默认值0，再从上往下执行赋值语句
        //（value = 1和静态块value = 2）
        System.out.println(DomeTest1.value); // 输出2
        System.out.println(DomeTest2.value); // 输出1
    }
}

class DomeTest1 {
    public static int value = 1;
    static {
        value = 2;
    }
}

class DomeTest2 {
    static {
        value = 2;
    }
    public static int value = 1;
}
