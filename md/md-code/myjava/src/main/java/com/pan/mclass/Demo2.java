package com.pan.mclass;

/**
 * @author panyibao
 * 继承相关静态变量初始化
 */
public class Demo2 {
    public static void main(String[] args) {
        // 使用父类DemoA1.a，不涉及子类初始化 输出 1
        System.out.println("DemoA1.a : " + DemoA1.a);
        // 使用子类DemoB1.a ，因为a在父类中，直接访问父类DemoA1.a,不执行子类DemoB1的初始化方法，输出 1
        System.out.println("DemoB1.a : " + DemoB1.a);
        // new 会执行子类DemoB1的初始化方法， 输出3
        System.out.println("new DemoB1().a : " + new DemoB1().a);
        // new DemoB1()执行了初始化方法，DemoA1.a和DemoB1.a和new DemoB1().a都是一样的结果，输出3
        System.out.println("new DemoB1() 后的 DemoA1.a : " + DemoA1.a);
        System.out.println("new DemoB1() 后的 DemoB1.a : " + DemoB1.a);
    }
}

class DemoA1{
    static int a = 0;
    static {
        a = 1;
    }
}

class DemoB1 extends DemoA1{
    static {
        a = 3;
    }
}