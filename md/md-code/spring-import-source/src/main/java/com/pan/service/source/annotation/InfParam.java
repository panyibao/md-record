package com.pan.service.source.annotation;

import java.lang.annotation.*;

/**
 * 接口参数
 * @author panyibao
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InfParam {
    String paramName();
    String paramValue();
}
