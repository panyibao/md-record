package com.pan.service.source;

import com.pan.service.source.annotation.InfParam;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author panyibao
 */
public class ProxyInterfaceImpl<T> implements InvocationHandler {

    private Class interfaces;
    public ProxyInterfaceImpl(Class interfaces){
        this.interfaces = interfaces;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        InfParam infParam = method.getAnnotation(InfParam.class);
        String name = infParam.paramName();
        String value = infParam.paramValue();
        System.out.println("执行source模块的接口实现方法");
        System.out.println(name + " - " + value);
        Class<?> returnType = method.getReturnType();
        return null;
    }
}
