package com.pan.service.source;

import com.pan.service.source.annotation.InfParam;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 代理生成接口对象
 * @author panyibao
 */
public class InterfaceProxyFactory<T> {

    /**
     * 通过代理实现接口，参数通过InfParam传入
     * @param interfaces 接口类
     * @return
     */
    public T newInstance(Class interfaces){
        ProxyInterfaceImpl<T> proxyInterface = new ProxyInterfaceImpl(interfaces);
        return (T) Proxy.newProxyInstance(interfaces.getClassLoader(), new Class[]{interfaces}, proxyInterface);
    }
}
