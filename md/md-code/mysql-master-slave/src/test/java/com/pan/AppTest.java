package com.pan;


import com.pan.ds.MysqlMasterSlaveApplication;
import com.pan.ds.entity.TestUser;
import com.pan.ds.service.MasterUserService;
import com.pan.ds.service.TestUserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit test for simple App.
 * @author panyibao
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MysqlMasterSlaveApplication.class)
class AppTest{

    @Autowired
    private TestUserService testUserService;
    @Autowired
    private MasterUserService masterUserService;

    @Test
    void testFind(){
        TestUser testUser1 = testUserService.findMaster(1);
        System.out.println(testUser1 == null ? "null" : testUser1.toString()
                + "=====================================");
        TestUser testUser2 = testUserService.findSlave(1);
        System.out.println(testUser2 == null ? "null" : testUser2.toString()
                + "=====================================");
        TestUser testUser3 = testUserService.findPrimary(1);
        System.out.println(testUser3 == null ? "null" : testUser3.toString()
                + "=====================================");
    }

    @Test
    void testDsDs(){
        TestUser testUser1 = masterUserService.findDsDs(1);
        System.out.println(testUser1 == null ? "null" : testUser1.toString()
                + "=====================================");
        TestUser testUser3 = masterUserService.findDSTransactional(1);
        System.out.println(testUser3 == null ? "null" : testUser3.toString()
                + "=====================================");
        TestUser testUser2 = masterUserService.findTransactional(1);
        System.out.println(testUser2 == null ? "null" : testUser2.toString()
                + "=====================================");
    }

    @Test
    void testAddTransactional(){
        TestUser testUser1 = masterUserService.addById(5);
        System.out.println(testUser1 == null ? "null" : testUser1.toString()
                + "=====================================");
    }
}
