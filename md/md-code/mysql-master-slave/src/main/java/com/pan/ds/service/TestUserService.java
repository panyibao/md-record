package com.pan.ds.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.ds.entity.TestUser;
import com.pan.ds.mapper.TestUserMapper;
import org.springframework.stereotype.Service;

/**
 * 读写分离测试服务
 * @author panyibao
 */
@Service
public class TestUserService extends ServiceImpl<TestUserMapper, TestUser> {
    public void add(TestUser user){
        getBaseMapper().insert(user);
    }

    @DS("master")
    public TestUser findMaster(int i) {
        return getBaseMapper().selectById(i);
    }

    @DS("slave_1")
    public TestUser findSlave(int i) {
        return getBaseMapper().selectById(i);
    }

    public TestUser findPrimary(int i) {
        return getBaseMapper().selectById(i);
    }
}
