package com.pan.ds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pan.ds.entity.TestUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 读写分离测试mapper
 * @author panyibao
 */
@Mapper
public interface TestUserMapper extends BaseMapper<TestUser> {
}
