package com.pan.ds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * mysql读写分离
 * @author panyibao
 */
@SpringBootApplication
public class MysqlMasterSlaveApplication {
    public static void main( String[] args ){
        SpringApplication.run(MysqlMasterSlaveApplication.class, args);
    }
}
