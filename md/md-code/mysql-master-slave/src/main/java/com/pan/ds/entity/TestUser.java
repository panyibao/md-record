package com.pan.ds.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

/**
 * mysql读写分离测试用户类
 * @author panyibao
 */

@Data
@ToString
@TableName("test_user")
public class TestUser {

    private long id;

    private String name;
}
