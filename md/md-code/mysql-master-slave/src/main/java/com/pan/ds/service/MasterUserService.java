package com.pan.ds.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.dynamic.datasource.annotation.DSTransactional;
import com.pan.ds.entity.TestUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author panyibao
 */
@Service
public class MasterUserService {

    @Autowired
    private TestUserService testUserService;

    /**
     * findDsDs @DS("master")
     * findSlave @DS("slave_1")
     * 实际使用slave_1, 即嵌套@DS时，使用的是最终的配置
     * @param i
     * @return
     */
    @DS("master")
    public TestUser findDsDs(int i) {
        return testUserService.findSlave(i);
    }

    /**
     * findDSTransactional @DS("master")
     * findSlave @DS("slave_1")
     * 实际使用slave_1, 即嵌套@DS时，使用的是最终的配置，@DSTransactional不会影响数据源的使用
     * @param i
     * @return
     */
    @DSTransactional
    @DS("master")
    public TestUser findDSTransactional(int i) {
        return testUserService.findSlave(i);
    }

    /**
     * findTransactional @DS("master")
     * findSlave @DS("slave_1")
     * 实际使用master, 即嵌套@DS时，使用@Transactional事务开始时的数据源
     * @param i
     * @return
     */
    @Transactional
    @DS("master")
    public TestUser findTransactional(int i) {
        return testUserService.findSlave(i);
    }


    /**
     * 测试主从事务@DS嵌套时，是否是同一个数据源
     * findTransactional @DS("master")
     * findSlave @DS("slave_1")
     * 实际使用master, 即嵌套@DS时，使用@Transactional事务开始时的数据源
     * @param i
     * @return
     */
    @Transactional
    @DS("master")
    public TestUser addById(int i) {
        TestUser user = new TestUser();
        user.setId(i);
        user.setName("p" + i);
        testUserService.add(user);
        TestUser slave = testUserService.findSlave(i);
        // 测试异常是否能正常回滚
        double s = 1/0;
        return slave;
    }

}
