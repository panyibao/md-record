package com.pan.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * zookeeper注册中心支付模块
 * @author panyibao
 */
@SpringBootApplication
public class ZKPaymentApplicationStarter {
    public static void main(String[] args) {
        SpringApplication.run(ZKPaymentApplicationStarter.class, args);
    }
}
