package com.pan.payment.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付接口
 * @author panybiao
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    /**
     * 获取信息
     * @return String
     */
    @GetMapping("/info")
    public String info(){
        return "payment info";
    }

}
