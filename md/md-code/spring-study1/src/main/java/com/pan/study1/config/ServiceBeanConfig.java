package com.pan.study1.config;

import com.pan.study1.service.OrderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * bean配置
 * @author panyibao
 */
@Configuration
public class ServiceBeanConfig {

    @Bean("orderService0")
    public OrderService getOrderService0(){
        OrderService orderService1 = getOrderService1();
        OrderService orderService2 = getOrderService1();
        System.out.println("________________________________________________");
        System.out.println(orderService1.hashCode());
        System.out.println(orderService2.hashCode());
        System.out.println(orderService2 == orderService1);
        System.out.println("________________________________________________");
        return orderService1;
    }

    @Bean("orderService1")
    public OrderService getOrderService1(){
        return new OrderService("service1");
    }

    @Bean("orderService2")
    public OrderService getOrderService2(){
        return new OrderService("service2");
    }
}
