package com.pan.study1.service;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author panyibao
 */
@Data
@ToString
@Service
public class OrderService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private OrderServiceBean orderServiceBean;

    @Transactional
    public void add1(){
        jdbcTemplate.execute("insert into ...");
        orderServiceBean.add2();
    }

    @Transactional(propagation = Propagation.NEVER)
    public void add2(){
        jdbcTemplate.execute("insert into ...");
    }


    private String name;

    public OrderService(){
        name = "orderService";
    }

    public OrderService(String name){
        this.name = name;
    }
}
