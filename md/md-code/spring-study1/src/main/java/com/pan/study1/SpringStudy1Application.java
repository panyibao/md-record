package com.pan.study1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Hello world!
 * @author panyibao
 */
@Slf4j
@SpringBootApplication
@EnableConfigurationProperties
public class SpringStudy1Application {
    public static void main( String[] args ){
        SpringApplication.run(SpringStudy1Application.class, args);
    }
}
