package com.pan.study1.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 测试服务
 * @author panyibao
 */
@Slf4j
@Service
public class TestService {

    @Autowired
    @Qualifier("orderService0")
    private OrderService orderService0;
}
