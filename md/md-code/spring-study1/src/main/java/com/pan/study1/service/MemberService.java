package com.pan.study1.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author panyibao
 */
@Slf4j
@Service
public class MemberService {

    private OrderService orderService;
    public MemberService(OrderService orderService){
        this.orderService = orderService;
        log.debug("----------------------------------------------------");
        log.debug(orderService.toString());
        log.debug("----------------------------------------------------");
    }

    public void add(){
        orderService.add1();
    }
}
