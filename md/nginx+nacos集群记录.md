# nginx安装

**1. gcc 安装**
安装 nginx 需要先将官网下载的源码进行编译，编译依赖 gcc 环境，如果没有 gcc 环境，则需要安装：

```shell
#安装 gcc
sudo yum install gcc-c++
#查看 gcc版本
gcc -v
```

**2.pcre、pcre-devel安装**

pcre是一个perl库，包括perl兼容的正则表达式库，nginx的http模块使用pcre来解析正则表达式，所以需要安装pcre库。

```shell
sudo yum install -y pcre pcre-devel
```

**3.zlib 安装**
zlib 库提供了很多种压缩和解压缩的方式， nginx 使用 zlib 对 http 包的内容进行 gzip ，所以需要在 Centos 上安装 zlib 库。

```shell
sudo yum install -y zlib zlib-devel
```

**4.OpenSSL 安装**
OpenSSL 是一个强大的安全套接字层密码库，囊括主要的密码算法、常用的密钥和证书封装管理功能及 SSL 协议，并提供丰富的应用程序供测试或其它目的使用。
nginx 不仅支持 http 协议，还支持 https（即在ssl协议上传输http），所以需要在 Centos 安装 OpenSSL 库。

```shell
sudo yum install -y openssl openssl-devel
```

**5.下载nginx包地址**

https://nginx.org/en/download.html

```shell
#解压命令
tar -zxvf nginx-1.20.2.tar.gz
```

**6.使用默认nginx配置**

```shell
./configure
```

**7.编译并且安装**

```shell
make && make install

#查找安装路径：
whereis nginx
```

由于安装路径默认：/usr/local/[nginx](https://so.csdn.net/so/search?q=nginx&spm=1001.2101.3001.7020)，如果当前用户没有root权限，make失败，修改用户权限或者安装路径

修改安装路径：

```shell
./configure --prefix="/home/pan/pan/mynginx/nginx"
# 重新执行make
make && make install
```

**8.启动、停止nginx**

```shell
#进入安装目录的sbin目录 cd /usr/local/nginx/sbin/
cd /home/pan/pan/mynginx/nginx/sbin
./nginx
./nginx -s stop
./nginx -s quit
./nginx -s reload

#启动报错
nginx: [emerg] bind() to 0.0.0.0:80 failed (13: Permission denied)
#原因 当前用户对该位置没有写入权限， 使用sudo执行命令

#访问报错
#查看防火墙状态 systemctl status firewalld
# 开启防火墙 service firewalld start
# 重启防火墙 service firewalld restart
# 关闭防火墙 service firewalld stop
# 开放端口 
# 查询端口是否开放sudo firewall-cmd --query-port=80/tcp
# 开放80端口sudo firewall-cmd --permanent --add-port=80/tcp
# 移除端口 firewall-cmd --permanent --remove-port=8080/tcp
#重启防火墙(修改配置后要重启防火墙) sudo firewall-cmd --reload
# 参数解释
#1、firwall-cmd：是Linux提供的操作firewall的一个工具；
#2、--permanent：表示设置为持久；
#3、--add-port：标识添加的端口；

#如果访问时出现无权限
#修改/home/pan/pan/mynginx/nginx/conf/nginx.conf 的 #user nobody; 改成user pan
```

**9.nginx配置**

```
#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;

events {
    worker_connections  1024;
}

http {
    # include  mime.types;识别css文件等
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    #tcp_nopush     on;
    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;
    # 第一个主机
    server {
      # 1.侦听80端口
      listen 80;
      # 服务名称用于配置域名
      server_name  localhost;
      charset     utf-8;
      client_max_body_size 1024M;
      location / {
        # 2. 默认主页目录在nginx安装目录的html子目录。
        root html;
        index index.html index.htm;
        # 3. 没有索引页时，罗列文件和子目录
        autoindex on;
        autoindex_exact_size on;
        autoindex_localtime on;
      }
      # 4.指定虚拟目录
      location /tshirt {
        alias D:\programs\Apache2\htdocs\tshirt;
        index index.html index.htm;
      }
    }
    # 第二个主机（测试反向代理）
    server {
        listen  81;
        server_name  www.dbjwb.com;
        location / {
            root html;
            index  index.html;
            # 设置反向代理
            proxy_set_header Host $host;
            proxy_pass http://spring_gateway/;
        }
    }

    # 必须配置在http里面
    # 反向代理上游服务器(spring网关)
    upstream spring_gateway {
        server 192.168.0.109:8080 ;
        #多节点，负载均衡
        #server 192.168.0.110:8080 ;
        #server 192.168.0.111:8080 ;
    }
}
```

windows 关闭nginx： taskkill /f /t /im nginx.exe

# nacos集群

下载nacos，解压修改集群配置文件cluster.conf添加集群地址

```
192.168.146.133:3333
192.168.146.133:4444
192.168.146.133:5555
```

修改nginx.conf添加代理

```shell
vim /home/pan/pan/mynginx/nginx/conf/nginx.conf
```

修改内容，如果改了端口需要在防火墙开放

    #gzip  on;
    ####### 新增内容开始 ######
    upstream cluster{
        server 192.168.146.133:3333;
        server 192.168.146.133:4444;
        server 192.168.146.133:5555;
    }
    ####### 新增内容结束 ######
    server {
        listen       1111;
        server_name  localhost;
        location / {
            ####### 修改内容开始 ######
            #root   html;
            #index  index.html index.htm;
            proxy_pass http://cluster;
            ####### 修改内容结束 ######
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }

访问测试地址http://192.168.146.133:1111/nacos

微服务应用nacos配置地址改为192.168.146.133:1111，启动服务，页面查看成功注册到nacos集群